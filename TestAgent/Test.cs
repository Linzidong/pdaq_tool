﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PDAQ4DriverAgent.PDAQ4Implementation;
using PDAQ4DriverInterface;
using System.Threading;
namespace TestAgent
{
    class Test
    { 
        
        void OnFrame(Frame frame)
        {
            System.Console.WriteLine("Got Frame");
        }
        private void PrintHardwareResource(HardwareResource hwresource)
        {

        System.Console.WriteLine(String.Format("Hardware Resource: {0:X},{1:X},{2:X}",
                        hwresource.VirtualPointer,
                        hwresource.PhysicalAddress,
                        "Bytes" + hwresource.Bytes));
        }
        private void PrintDebugInfo(PDAQ4DebugInfo debuginfo)
        {

            System.Console.WriteLine(string.Format("FIFO_Index = {0}", debuginfo.FIFO_Index));
            System.Console.WriteLine(string.Format("DMA_RFFrames_InBuffer = {0}", debuginfo.DMA_RFFrames_InBuffer));
            System.Console.WriteLine(string.Format("interrupt_counter_1 = {0}", debuginfo.interrupt_counter_1));
            System.Console.WriteLine(string.Format("interrupt_counter_2 = {0}", debuginfo.interrupt_counter_2));
        
        
        }
        public static void Main()
        {
            byte []abc= {1,2,3,4,5,6,7,8};
            UInt32[] def = { 1, 2 };
            Buffer.BlockCopy(abc,0,def,0,8);


            Test test = new Test();
            try
            {
                DriverAgent drvagent = new DriverAgent();
                IPDAQDevice device = drvagent.FindPDAQ4();
                System.Console.WriteLine("DeviceName" + device.DeviceName);
                System.Console.WriteLine("HardwareID" + device.HardwareID);
                System.Console.WriteLine("PID" + device.PID.ToString());
                System.Console.WriteLine("VID" + device.VID.ToString());

                drvagent.OpenPDAQ4();
                HardwareResource hwresource = drvagent.QueryPDAQ4Resource();
                test.PrintHardwareResource(hwresource);
                
                FifoInfo []fifos = drvagent.QueryFIFO();
                
                foreach (FifoInfo info in fifos)
                {
                    System.Console.Out.WriteLine("FIFO SIZE is " + info.FifoSize);
                    System.Console.Out.WriteLine("FrameType is  " + info.FrameType);
                    foreach (FifoElement element in info.fifo_element_list)
                    {
                        foreach (HardwareResource hrw in element.dma_descriptor_list)
                        {
                            test.PrintHardwareResource(hrw);
                        }
                    }
                }

                
                /*
                // Regiester operation test
                uint space_length = drvagent.ReadPDAQ4RegSpaceBytesLength();
                uint[] regs = drvagent.ReadPDAQ4RegSpace(0x100);
                foreach (uint reg in regs)
                {
                    System.Console.WriteLine(string.Format("{0:X4}",reg));

                }
                drvagent.WritePDAQ4Reg(64, 0xCC1122);
                uint value_after_write = drvagent.ReadRegisterValue(64);
                System.Console.WriteLine(string.Format("{0:X4}", value_after_write));
                uint[] regblock = new uint[100];
                for (uint i = 0; i < 100;i++ )
                {
                    regblock[i] = i;
                }
                drvagent.WritePDAQ4RegBlock(64, regblock);
                */
                // connect to the fifo
                FiFoConnection connection_rf, connection_rt;
                ChannelHeader rf_channelheader = new ChannelHeader();
                rf_channelheader.ChannelIndex = 0;
                rf_channelheader.ChannelType = ChannelType.RF_CHANNEL;
                rf_channelheader.FrameHdrSize = 96; // TODO - use type size
                rf_channelheader.FrameRate = 2;
                rf_channelheader.VectorHdrSize = 16;
                rf_channelheader.VectorsPerFrame = 256;
                rf_channelheader.SamplesPerVector = 1024;
                rf_channelheader.BitsPerSample = 16;
                rf_channelheader.SampleMask = 0x0fff;
                rf_channelheader.SwVersion = 0x0300;
                connection_rf = new Frame_Connection(rf_channelheader, fifos[0]);

                ChannelHeader rt_channelheader = new ChannelHeader();
                rt_channelheader.ChannelType = ChannelType.RT_CHANNEL;
                rt_channelheader.ChannelIndex = 1;
                rt_channelheader.FrameHdrSize = 96; // TODO - use type size
                rt_channelheader.FrameRate = 30;
                rt_channelheader.VectorHdrSize = 16;
                rt_channelheader.VectorsPerFrame = 256;
                rt_channelheader.SamplesPerVector = 256;
                rt_channelheader.BitsPerSample = 8;


                connection_rt = new Frame_Connection(rt_channelheader, fifos[1]);
                
                drvagent.PDAQ4ConnectFIFOChannel(connection_rf);
                drvagent.PDAQ4ConnectFIFOChannel(connection_rt);
                connection_rf.FrameReadyEvent += new DeviceGenericEvent<Frame>(test.OnFrame);
                connection_rt.FrameReadyEvent += new DeviceGenericEvent<Frame>(test.OnFrame);
                connection_rt.start();
                connection_rf.start();
                drvagent.StartDMA();
                Thread.Sleep(300000);
                drvagent.StopDMA();
                Thread.Sleep(1000);
                PDAQ4DebugInfo debuginfo = drvagent.PDAQ4QueryDebugInfo();
                test.PrintDebugInfo(debuginfo);
                System.Console.WriteLine(String.Format("Overflow = {0}", connection_rf.fifo.GetNumOverflow()));
                System.Console.WriteLine(String.Format("Overflow = {0}", connection_rt.fifo.GetNumOverflow()));
               
                
                connection_rt.stop();
                connection_rf.stop();


             
          }
      

            
            catch(Exception err)
            {

                System.Console.WriteLine(String.Format("{0}",err));
            }
            
            
            
        }
    }
}
