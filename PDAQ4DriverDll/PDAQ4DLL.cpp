#include "stdafx.h"
#define _DLLMODULENAME_

#include <memory.h>
using namespace std;
#include <string>
#include <iostream>
#include <sstream>
#include <stdint.h>
#include <winioctl.h>
#define INITGUID
//#include "../PDAQ4_driver/PDAQ4KMDFDriver/RegPDAQ4.h"
#include "PDAQ4DLL.h"
#include <iostream>
//#include "../PDAQ4_driver/PDAQ4KMDFDriver/PDAQ4DriverPublic.h"
#define DEVICE_PATH_MAX 1000
PDAQ4CARDResource gResourcePacket;
DrvQueryFiFoPacket	gFiFoPacket[2];


HANDLE gDeviceHandle = INVALID_HANDLE_VALUE;
CHAR gDevicePath[1000];

BOOL	gDeviceFound = false;

PBYTE getDeviceProperty(HDEVINFO infoSet,PSP_DEVINFO_DATA infoData,DWORD property)
{
	PBYTE propBuffer = NULL;
	ULONG resourceSize = 0;
	BOOL  success;

	// Retrieve the device property, dynamically allocating a buffer.
	SetupDiGetDeviceRegistryProperty(infoSet,
		infoData,
		property,
		NULL,
		NULL,
		0,
		&resourceSize);

	// The call should fail with an insufficient buffer
	if (GetLastError() == ERROR_INSUFFICIENT_BUFFER) {
		propBuffer = (PBYTE)malloc(resourceSize);
		if (propBuffer != NULL) {
			// Fetch the string into the allocated buffer
			success = SetupDiGetDeviceRegistryProperty(infoSet,
				infoData,
				property,
				NULL,
				(PBYTE)propBuffer,
				resourceSize,
				NULL);
			if (success == FALSE) {
				// Free the allocated buffer and set it to NULL
				free(propBuffer);
				propBuffer = NULL;
			}
		}

	}

	// Return the dynamically-allocated buffer, or NULL
	return(propBuffer);
}


PDAQ4_STATUS FindPDAQ4(PDAQ4CARD *card)
{
	UINT32 instanceCount;
	SP_DEVICE_INTERFACE_DATA interfaceData;
	SP_DEVINFO_DATA infoData;
	HDEVINFO infoSet;
	PDAQ4_STATUS status = PDAQ4_DLL_STATUS_SUCCESS;
	// Retrieve the device information for all PDAQ4 devices.
	infoSet = SetupDiGetClassDevs(&GUID_PDAQ4_INTERFACE,NULL,NULL,(DIGCF_DEVICEINTERFACE | DIGCF_PRESENT));

	// Initialize the device interface and info data structures.
	interfaceData.cbSize = sizeof(SP_DEVICE_INTERFACE_DATA);
	infoData.cbSize = sizeof(SP_DEVINFO_DATA);

	// Iterate through all the devices found in the system by the public driver GUID.
	// We just get the first device on bus, because normally there will not such case to have two cards on bus.
	instanceCount = 0;
	while (SetupDiEnumDeviceInterfaces(infoSet,
		NULL,
		&GUID_PDAQ4_INTERFACE,
		instanceCount,
		&interfaceData))
	{
		PSP_DEVICE_INTERFACE_DETAIL_DATA interfaceDetails;
		ULONG  resourceSize;
		BOOL   success;
		TCHAR *deviceName;
		TCHAR *hardwareId;

		// Get details about the device's interface.  Begin by dynamically determining the
		// size of the interface details structure to be returned. 
		SetupDiGetDeviceInterfaceDetail(infoSet,
			&interfaceData,
			NULL,
			0,
			&resourceSize,
			NULL);

		// see remark http://msdn.microsoft.com/en-us/library/windows/hardware/ff551120%28v=vs.85%29.aspx
		// The error must be ERROR_INSUFFICIENT_BUFFER, Before the last step is for getting the size of detail Interface Description

		interfaceDetails = (PSP_DEVICE_INTERFACE_DETAIL_DATA)malloc(resourceSize);

		// Repeat the call, with an allocated details structure.  Set the cbSize member
		// to the fixed size of the structure.
		if (interfaceDetails)
		{
			interfaceDetails->cbSize = sizeof(SP_DEVICE_INTERFACE_DETAIL_DATA);
			success = SetupDiGetDeviceInterfaceDetail(infoSet,
				&interfaceData,
				interfaceDetails,
				resourceSize,
				NULL,
				&infoData);
			strcpy_s(gDevicePath, sizeof(gDevicePath), interfaceDetails->DevicePath);
			free(interfaceDetails);
		}
		

		// Get properties about the device,remember to delete the memeory allocated inside the getDeviceProperty
		deviceName = (TCHAR*)getDeviceProperty(infoSet, &infoData, SPDRP_DEVICEDESC);
		hardwareId = (TCHAR*)getDeviceProperty(infoSet, &infoData, SPDRP_HARDWAREID);

		if (deviceName == NULL || hardwareId == NULL)
			return PDAQ4_DLL_EXCEPTION_IN_FIND_PDAQ4;

		// Parse the hardware ID string, obtaining the vendor and device IDs for use in 
		// creating a tag to search for an appropriate subclass.

		unsigned int  vendorId = 0;
		unsigned int  deviceId = 0;
		string idString(hardwareId);
		string descString(deviceName);
		size_t vendorPos = idString.find("VEN_");
		size_t devicePos = idString.find("DEV_");

		if ((vendorPos != string::npos) && (idString.length() >= (vendorPos + 8)) &&
			(devicePos != string::npos) && (idString.length() >= (devicePos + 8))) {
			istringstream vendorStream("0x" + idString.substr((vendorPos + 4), 4));
			vendorStream >> hex >> vendorId;

			istringstream deviceStream("0x" + idString.substr((devicePos + 4), 4));
			deviceStream >> hex >> deviceId;
		}



		// copy the PDAQ4 information to return it for user.
		card->PID = deviceId;
		card->VID = vendorId;
		strcpy_s(card->cardname, sizeof(card->cardname), deviceName);
		strcpy_s(card->hardwardid, sizeof(card->hardwardid), hardwareId);

		// Free the character string for the hardware ID and deviceName if not NULL
		free(hardwareId);
		free(deviceName);

		gDeviceFound = true;
		break;
	}
	// if no device found. 
	if (gDeviceFound == false)
		status = PDAQ4_DLL_STATUS_NO_PDAQ4_PRESENT;

	return(status);
}


PDAQ4_STATUS OpenPDAQ4()
{
	FILE *fp;
	fopen_s(&fp,"dll_log.txt", "a");
	// Create a file and get a handle to the device
	if (gDeviceHandle != INVALID_HANDLE_VALUE)
		return PDAQ4_DLL_STATUS_CANNOT_OPEN_AGAIN;
	if (gDeviceFound == false)
		return PDAQ4_DLL_STATUS_OPEN_BEFORE_FOUND;

	fprintf(fp, "gDevicePath = %s\n", gDevicePath);
	gDeviceHandle = CreateFile(gDevicePath, (GENERIC_READ | GENERIC_WRITE), (FILE_SHARE_READ | FILE_SHARE_WRITE),
		NULL,
		OPEN_EXISTING,
		0,
		NULL);

	
	
	
	if (gDeviceHandle == INVALID_HANDLE_VALUE) 
	{
		DWORD errorcode = GetLastError();
		LPVOID lpMsgBuf;
		FormatMessage(
			FORMAT_MESSAGE_ALLOCATE_BUFFER |
			FORMAT_MESSAGE_FROM_SYSTEM |
			FORMAT_MESSAGE_IGNORE_INSERTS,
			NULL,
			errorcode,
			MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), // Default language
			(LPTSTR)&lpMsgBuf,
			0,
			NULL
			);
		fprintf(fp, "Failed CreateFile GetLastErrorCode = %d\n", errorcode);
		fprintf(fp, "Failed CreateFile FailedReson = %s\n", lpMsgBuf);
		LocalFree(lpMsgBuf);
		fclose(fp);
		return PDAQ4_DLL_STATUS_CANNOT_OPEN_DEVICE;
	}
	if (PDAQ4Init() == PDAQ4_DLL_STATUS_SUCCESS && QueryPDAQ4Resource(&gResourcePacket) == PDAQ4_DLL_STATUS_SUCCESS  && QueryFIFO(gFiFoPacket) == PDAQ4_DLL_STATUS_SUCCESS)
		return PDAQ4_DLL_STATUS_SUCCESS;
	fclose(fp);
	return PDAQ4_DLL_STATUS_QUERY_RESOURCE_FAILED;
}
PDAQ4_STATUS ClosePDAQ4()
{
	if (gDeviceHandle == INVALID_HANDLE_VALUE)
		return PDAQ4_DLL_STATUS_CLOSE_BEOFRE_OPEN;
	if (CloseHandle(gDeviceHandle) == false)
		return PDAQ4_DLL_STATUS_CANNOT_CLOSE;
	gDeviceHandle = INVALID_HANDLE_VALUE;
	gDeviceFound = false;
	return PDAQ4_DLL_STATUS_SUCCESS;

}
PDAQ4_STATUS PDAQ4Init()
{
	DWORD return_length = 0;
	if (DeviceIoControl(gDeviceHandle, IOCTL_INIT, NULL, 0, NULL, 0, &return_length, NULL))
	{
		
		return PDAQ4_DLL_STATUS_SUCCESS;
	}
	return PDAQ4_DLL_DEVICE_IO_CTRL_ERROR;
}

DLLMODULENAME_LIB_API PDAQ4_STATUS PDAQ4DMAInit(PDrvInitRequest pInitRequest)
{
	DWORD return_length = 0;
	if (DeviceIoControl(gDeviceHandle, IOCTL_DMA_INIT, pInitRequest, sizeof(DrvInitRequest), NULL, 0, &return_length, NULL))
	{

		return PDAQ4_DLL_STATUS_SUCCESS;
	}
	return PDAQ4_DLL_DEVICE_IO_CTRL_ERROR;

}


PDAQ4_STATUS QueryFIFO(DrvQueryFiFoPacket *fifobuffer)
{
	
	PDrvQueryFifoPacket pqueryresoucepackage = (PDrvQueryFifoPacket)malloc(sizeof(DrvQueryFiFoPacket)* 2);
	DWORD return_bytes = 0;
	if (DeviceIoControl(gDeviceHandle, IOCTL_GET_DMABUFFER, NULL, 0, pqueryresoucepackage, sizeof(DrvQueryFiFoPacket)* 2, &return_bytes, NULL))
	{
		memcpy(fifobuffer, pqueryresoucepackage, sizeof(DrvQueryFiFoPacket)* 2);
		free(pqueryresoucepackage);
		return PDAQ4_DLL_STATUS_SUCCESS;
	}
	free(pqueryresoucepackage);
	return PDAQ4_DLL_DEVICE_IO_CTRL_ERROR;

}

PDAQ4_STATUS PDAQ4QueryDebugInfo(PDrvDebugInfo pRequest)
{

	DrvDebugInfo querypackage;
	DWORD return_bytes = 0;
	if (DeviceIoControl(gDeviceHandle, IOCTL_DIAG_QUERY_DEBUG_INFO, NULL, 0, &querypackage, sizeof(DrvDebugInfo), &return_bytes, NULL))
	{
		memcpy(pRequest, &querypackage, sizeof(DrvDebugInfo));
		//gResourcePacket = queryresoucepackage;
		return PDAQ4_DLL_STATUS_SUCCESS;
	}
	return PDAQ4_DLL_DEVICE_IO_CTRL_ERROR;
}




PDAQ4_STATUS PDAQ4ConnectFIFOChannel(PDrvConnectFifoInPacket pRequest, PDrvConnectFifoOutPacket pout)
{

	unsigned long actual_length_long = 0;
	BOOL result = DeviceIoControl(gDeviceHandle, IOCTL_CONNECT_FIFO, pRequest, sizeof(DrvConnectFifoInPacket), pout, sizeof(DrvConnectFifoOutPacket), &actual_length_long, NULL);
	
	if (result)
	{
		return PDAQ4_DLL_STATUS_SUCCESS;
	}
	else
		return PDAQ4_DLL_DEVICE_IO_CTRL_ERROR;

}
PDAQ4_STATUS PDAQ4DisConnectFIFOChannel(UINT32 channel_index)
{


	unsigned long actual_length_long = 0;
	BOOL result = DeviceIoControl(gDeviceHandle, IOCTL_DISCONNECT_FIFO, &channel_index, sizeof(UINT32), NULL, 0, &actual_length_long, NULL);

	if (result)
	{
		return PDAQ4_DLL_STATUS_SUCCESS;
	}
	else
		return PDAQ4_DLL_DEVICE_IO_CTRL_ERROR;

}

PDAQ4_STATUS QueryPDAQ4Resource(PDAQ4CARDResource *cardResource)
{
	UNREFERENCED_PARAMETER(cardResource);
	DrvQueryResourcePacket queryresoucepackage;
	DWORD return_bytes = 0;
	if (DeviceIoControl(gDeviceHandle, IOCTL_QUERY_RESOURCE, NULL, 0, &queryresoucepackage, sizeof(DrvQueryResourcePacket), &return_bytes, NULL))
	{
		cardResource->physical_regbase = queryresoucepackage.resource[0].physAddr;
		cardResource->virtual_regbase = queryresoucepackage.resource[0].pvUser;
		cardResource->reglength = queryresoucepackage.resource[0].length;
		//gResourcePacket = queryresoucepackage;
		return PDAQ4_DLL_STATUS_SUCCESS;
	}
	return PDAQ4_DLL_DEVICE_IO_CTRL_ERROR;
}



PDAQ4_STATUS ReadPDAQ4RegSpaceBytesLength_ByIO(unsigned int *length_in_bytes)
{
	UINT32 length = 0;
	DWORD return_bytes = 0;
	if (gDeviceHandle == INVALID_HANDLE_VALUE)
		return PDAQ4_DLL_STATUS_DEVICE_NOT_OPEN_BEFORE_USE;

	if (DeviceIoControl(gDeviceHandle, IOCTL_PDAQ4_READ_CONFIG_LENGTH, NULL, 0, &length, sizeof(UINT32), &return_bytes, NULL))
	{
		*length_in_bytes =  length;
		return PDAQ4_DLL_DEVICE_IO_CTRL_ERROR;
	}
	else
	{
		return PDAQ4_DLL_DEVICE_IO_CTRL_ERROR;
	}
		
}

PDAQ4_STATUS ReadPDAQ4RegSpaceBytesLength_By_LocalMap(unsigned int *length_in_bytes)
{
	
	if (gDeviceHandle == INVALID_HANDLE_VALUE)
		return PDAQ4_DLL_STATUS_DEVICE_NOT_OPEN_BEFORE_USE;
	*length_in_bytes = gResourcePacket.reglength;
	return PDAQ4_DLL_STATUS_SUCCESS;


}

PDAQ4_STATUS ReadPDAQ4RegSpace_ByIO(byte *regsapce, unsigned int expected_regspace_length_in_bytes, unsigned int *actual_regspace_length_in_bytes)
{
	//UINT32 * mem = (UINT32*)malloc(sizeof(UINT32)*regs_size);
	unsigned long actual_regspace_length_long = 0;
	BOOL result = DeviceIoControl(gDeviceHandle, IOCTL_PDAQ4_READ_CONFIG_REGS, NULL, 0, regsapce, expected_regspace_length_in_bytes, &actual_regspace_length_long, NULL);
	*actual_regspace_length_in_bytes = actual_regspace_length_long;
	if (result)
	{
		return PDAQ4_DLL_STATUS_SUCCESS;
	}
	else
		return PDAQ4_DLL_DEVICE_IO_CTRL_ERROR;
}


PDAQ4_STATUS ReadPDAQ4RegSpace_ByLocalMap(byte *regsapce, unsigned int expected_regspace_length_in_bytes, unsigned int *actual_regspace_length_in_bytes)
{
	if (expected_regspace_length_in_bytes > gResourcePacket.reglength)
		return PDAQ4_DLL_STATUS_SUCCESS;
	memcpy(regsapce, gResourcePacket.virtual_regbase, expected_regspace_length_in_bytes);
	
	*actual_regspace_length_in_bytes = gResourcePacket.reglength;
	return PDAQ4_DLL_STATUS_SUCCESS;
}



PDAQ4_STATUS WritePDAQ4Reg_ByIO(unsigned int reg_address_byte4_offset, unsigned int value)
{

	WriteToRegister registertowrite;
	registertowrite.offset = reg_address_byte4_offset;
	registertowrite.value = value;
	DWORD return_bytes = 0;
	BOOL result = DeviceIoControl(gDeviceHandle, IOCTL_PDAQ4_WRITE_CONFIG_REG, &registertowrite, sizeof(WriteToRegister), NULL, 0, &return_bytes, NULL);
	if (result)
		return PDAQ4_DLL_STATUS_SUCCESS;
	else
		return PDAQ4_DLL_DEVICE_IO_CTRL_ERROR;
}


PDAQ4_STATUS WritePDAQ4Reg_ByLocalMap(unsigned int reg_address_byte4_offset, unsigned int value)
{

	REG32 *reg = (REG32 *)gResourcePacket.virtual_regbase + reg_address_byte4_offset;
	*reg = value;
	return PDAQ4_DLL_STATUS_SUCCESS;

}



PDAQ4_STATUS WritePDAQ4RegBlock_ByIO(unsigned int StartBARAddress, unsigned int regspace_word[], unsigned int regspacelength_word)
{
	WriteToRegisterBlock *registertowriteBlock;
	unsigned int len = sizeof(WriteToRegisterBlock)+sizeof(unsigned int)*(regspacelength_word - 1);
	registertowriteBlock = (WriteToRegisterBlock *)malloc(len);
	registertowriteBlock->StartBARAddress = StartBARAddress;
	registertowriteBlock->RegisterNum = regspacelength_word;
	memcpy(registertowriteBlock->RegisterBlock, (PUCHAR)regspace_word, regspacelength_word*sizeof(unsigned int));

	DWORD return_bytes = 0;
	BOOL result = DeviceIoControl(gDeviceHandle, IOCTL_PDAQ4_WRITE_CONFIG_REG_BLOCK, registertowriteBlock, len, NULL, 0, &return_bytes, NULL);
	free(registertowriteBlock);
	if (result)
		return PDAQ4_DLL_STATUS_SUCCESS;
	else
		return PDAQ4_DLL_DEVICE_IO_CTRL_ERROR;
}


PDAQ4_STATUS WritePDAQ4RegBlock_ByLocalMap(unsigned int StartBARAddress, unsigned int regspace_word[], unsigned int regspacelength_word)
{

	REG32 *reg = (REG32 *)gResourcePacket.virtual_regbase+ StartBARAddress/sizeof(REG32);
	for (unsigned int  i = 0; i < regspacelength_word; i++)
	{
		*reg = regspace_word[i];
		Sleep(1);
		reg++;
	}
	//memcpy((PUCHAR)reg, regspace_word, regspacelength_word * sizeof(REG32));
	return PDAQ4_DLL_STATUS_SUCCESS;
}


PDAQ4_STATUS ReadPDAQ4RegSpaceBytesLength(unsigned int *length_in_bytes)
{
	return ReadPDAQ4RegSpaceBytesLength_By_LocalMap(length_in_bytes);

}

PDAQ4_STATUS ReadPDAQ4RegPtr(unsigned int *regptr, unsigned int *actual_regspace_length_in_bytes)
{
	*regptr = (unsigned int)gResourcePacket.virtual_regbase;
	*actual_regspace_length_in_bytes = gResourcePacket.reglength;
	return PDAQ4_DLL_STATUS_SUCCESS;
}
PDAQ4_STATUS ReadPDAQ4RegSpace(byte *regsapce, unsigned int expected_regspace_length_in_bytes, unsigned int *actual_regspace_length_in_bytes)
{
	return ReadPDAQ4RegSpace_ByLocalMap(regsapce, expected_regspace_length_in_bytes, actual_regspace_length_in_bytes);
}


PDAQ4_STATUS WritePDAQ4Reg(unsigned int reg_address_byte4_offset, unsigned int value)
{

	return WritePDAQ4Reg_ByLocalMap(reg_address_byte4_offset, value);
}

PDAQ4_STATUS WritePDAQ4RegBlock(unsigned int StartBARAddress, unsigned int regspace_word[], unsigned int regspacelength_word)
{
	return WritePDAQ4RegBlock_ByLocalMap(StartBARAddress, regspace_word, regspacelength_word);
}





PDAQ4_STATUS ProgramFPGA(char *bit_file)
{
	UNREFERENCED_PARAMETER(bit_file);
	return PDAQ4_DLL_STATUS_SUCCESS;
}



PDAQ4_STATUS StartMotor()
{
	union
	{
		Mot_RUN mot_run;
		UINT value;
	}data;
	data.value = 0;
	data.mot_run.Enable = 1;
	return WritePDAQ4Reg(OFFSET_REG_MOTRUN, (unsigned int)data.value);
}
PDAQ4_STATUS StopMotor()
{
	union
	{
		Mot_RUN mot_run;
		UINT value;
	}data;
	data.value = 0;
	data.mot_run.Enable = 0;
	return WritePDAQ4Reg(OFFSET_REG_MOTRUN, (unsigned int)data.value);
}