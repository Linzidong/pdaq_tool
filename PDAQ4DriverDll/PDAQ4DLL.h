#ifndef __PDAQ4_DLL
#define __PDAQ4_DLL
#include "../PDAQ4KMDFDriver/RegPDAQ4.h"
#include "../PDAQ4KMDFDriver/PDAQ4DriverPublic.h"
#ifdef _DLLMODULENAME_ 
#define DLLMODULENAME_LIB_API __declspec(dllexport) 
#else 
#define DLLMODULENAME_LIB_API __declspec(dllimport)



#endif

#define PDAQ_DLL_MAX_DEVICE_NAME 100

#define PDAQ4_DLL_STATUS_SUCCESS   0
#define PDAQ4_DLL_STATUS_NO_PDAQ4_PRESENT 1
#define PDAQ4_DLL_STATUS_CANNOT_OPEN_AGAIN 2
#define PDAQ4_DLL_STATUS_OPEN_BEFORE_FOUND 3
#define PDAQ4_DLL_STATUS_READ_FRAME_TIMEOUT 5
#define PDAQ4_DLL_STATUS_CANNOT_OPEN_DEVICE 6
#define PDAQ4_DLL_STATUS_CLOSE_BEOFRE_OPEN 7
#define PDAQ4_DLL_STATUS_CANNOT_CLOSE 8
#define PDAQ4_DLL_STATUS_DEVICE_NOT_OPEN_BEFORE_USE 9
#define PDAQ4_DLL_DEVICE_IO_CTRL_ERROR 10
#define PDAQ4_DLL_EXCEPTION_IN_FIND_PDAQ4 11
#define PDAQ4_DLL_STATUS_READ_ERROR 12
#define PDAQ4_DLL_STATUS_QUERY_RESOURCE_FAILED 13
#define VECTOR_PER_FRAME		256
#define RT_SAMPLE_PER_VECTOR	256
#define RF_SAMPLE_PER_VECTOR    1024

typedef unsigned int  PDAQ4_STATUS;
typedef unsigned char byte;

typedef struct __PDAQ4CARD
{
	char cardname[PDAQ_DLL_MAX_DEVICE_NAME];
	char hardwardid[PDAQ_DLL_MAX_DEVICE_NAME];
	unsigned long PID;
	unsigned long VID;
}PDAQ4CARD;

typedef struct __PDAQ4CARDResource
{
	LARGE_INTEGER physical_regbase;
	PUCHAR virtual_regbase;
	unsigned int reglength;
}PDAQ4CARDResource;


typedef struct __RFFrameHeader
{
	UINT32 sync_pattern;
	UINT32 frame_size;
	UINT32 ch_ind;
	UINT samples_per_vector;
	UINT PB_distance;
	UINT RF_Sample_Spacing_Low;
	UINT RF_Sample_Spacing_High;
	UINT RF_Disp_Method;
	UINT TCG3_0;
	UINT TCG7_4;
	UINT TCG11_8;
	UINT TCG15_12;
	UINT TCG16;
	UINT RF_CathetherMaskRatio;
	UINT RF_Reserved[10];

}RFFrameHeader;


typedef struct __RTFrameHeader
{
	UINT32 sync_pattern;
	UINT32 frame_size;
	UINT32 ch_ind;
	UINT samples_per_vector;
	UINT PB_distance;
	UINT RT_Sample_Spacing_Low;
	UINT RT_Sample_Spacing_High;
	UINT RT_Disp_Method;
	UINT TCG3_0;
	UINT TCG7_4;
	UINT TCG11_8;
	UINT TCG15_12;
	UINT TCG16;
	UINT RT_CathetherMaskRatio;
	UINT RT_Reserved[10];

}RTFrameHeader;

typedef struct  __VectorHeader
{
	UINT32 Angle : 16;
	UINT32 VecInd : 16;
	UINT32 Process;
	UINT32 CurrentTime_LOW;
	UINT32 CurrentTime_HIGH;

}VectorHeader;

typedef struct  __RFVector
{
	VectorHeader vectorheader;
	UINT16 Data[RF_SAMPLE_PER_VECTOR];
}RFVector;

typedef struct __RFFrame
{
	RFFrameHeader frameheader;
	RFVector rfvectors[VECTOR_PER_FRAME];
}
RFFrame;

typedef struct  __RTVector
{
	VectorHeader vectorheader;
	UCHAR Data[RT_SAMPLE_PER_VECTOR];
}RTVector;


typedef struct __RTDataFrame
{
	RTFrameHeader frameheader;
	RTVector rtvectors[VECTOR_PER_FRAME];
}
RTFrame;

typedef struct __Frame
{
	RFFrame rfframe;
	RTFrame ivusdataframe;
}Frame;

#ifdef __cplusplus
extern "C"{
#endif
	DLLMODULENAME_LIB_API PDAQ4_STATUS FindPDAQ4(PDAQ4CARD *card);
	DLLMODULENAME_LIB_API PDAQ4_STATUS OpenPDAQ4();
	DLLMODULENAME_LIB_API PDAQ4_STATUS ClosePDAQ4();
	DLLMODULENAME_LIB_API PDAQ4_STATUS QueryPDAQ4Resource(PDAQ4CARDResource *cardResource);
	DLLMODULENAME_LIB_API PDAQ4_STATUS QueryFIFO(DrvQueryFiFoPacket *fifobuffer);
	DLLMODULENAME_LIB_API PDAQ4_STATUS PDAQ4Init();
	DLLMODULENAME_LIB_API PDAQ4_STATUS PDAQ4DMAInit(PDrvInitRequest pInitRequest);
	DLLMODULENAME_LIB_API PDAQ4_STATUS ReadPDAQ4RegSpaceBytesLength(unsigned int *length_in_bytes);
	DLLMODULENAME_LIB_API PDAQ4_STATUS ReadPDAQ4RegSpace(byte *regsapce, unsigned int expected_regspace_length_in_bytes, unsigned int *actual_regspace_length_in_bytes);
	DLLMODULENAME_LIB_API PDAQ4_STATUS ReadPDAQ4RegPtr(unsigned int  *regptr, unsigned int *actual_regspace_length_in_bytes);
	DLLMODULENAME_LIB_API PDAQ4_STATUS WritePDAQ4Reg(unsigned int reg_address_byte4_offset, unsigned int value);
	DLLMODULENAME_LIB_API PDAQ4_STATUS WritePDAQ4RegBlock(unsigned int StartBARAddress, unsigned int regspace_word[], unsigned int regspacelength_word);
	DLLMODULENAME_LIB_API PDAQ4_STATUS PDAQ4QueryDebugInfo(PDrvDebugInfo pRequest);
	DLLMODULENAME_LIB_API PDAQ4_STATUS PDAQ4ConnectFIFOChannel(PDrvConnectFifoInPacket pRequest,PDrvConnectFifoOutPacket pout);
	DLLMODULENAME_LIB_API PDAQ4_STATUS PDAQ4DisConnectFIFOChannel(UINT channel_index);

	DLLMODULENAME_LIB_API PDAQ4_STATUS ProgramFPGA(char *bit_file);
	DLLMODULENAME_LIB_API PDAQ4_STATUS StartMotor();
	DLLMODULENAME_LIB_API PDAQ4_STATUS StopMotor();
#ifdef __cplusplus
}
#endif
#endif