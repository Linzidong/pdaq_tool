﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PDAQ4DriverAgent.PDAQ4Implementation;

namespace PDAQ_Test_Console
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            DriverAgent agent = new DriverAgent();
            PDAQDevice pdaqDevice = null;
            try
            {
                pdaqDevice = (PDAQDevice)agent.FindPDAQ4();
                Console.WriteLine("");
            }
            catch (Exception e)
            {
                Console.WriteLine("FindPDAQ4 Failed");
                Console.WriteLine("Message:");
                Console.WriteLine(e.Message);
                Console.WriteLine("StackTrace:");
                Console.WriteLine(e.StackTrace);
                Console.ReadLine();
                return;
                throw e;
            }

            Console.WriteLine("Found PDAQ,VID:{0},PID:{1},DeviceName:{2},HardwareID:{3}", pdaqDevice.VID.ToString(), pdaqDevice.PID.ToString(), pdaqDevice.DeviceName.ToString(), pdaqDevice.HardwareID.ToString());

            try
            {
                agent.OpenPDAQ4();
            }
            catch (Exception e)
            {
                Console.WriteLine("FindPDAQ4 Failed");
                Console.WriteLine("Message:");
                Console.WriteLine(e.Message);
                Console.WriteLine("StackTrace:");
                Console.WriteLine(e.StackTrace);
                Console.ReadLine();
                return;
                throw e;
            }

            //agent.PrintDbgInfo();

            Console.WriteLine("Press any button to activate ACQ");
            Console.ReadLine();

            try
            {
                agent.MotorStart();
                Console.WriteLine("Start ACQ Succeed");
                Console.ReadLine();
            }
            catch (Exception e)
            {
                Console.WriteLine("Start ACQ Failed");
                Console.WriteLine("Message:");
                Console.WriteLine(e.Message);
                Console.WriteLine("StackTrace:");
                Console.WriteLine(e.StackTrace);
                Console.ReadLine();
                return;
            }

            Console.WriteLine("Press any button to stop ACQ");
            Console.ReadLine();

            try
            {
                agent.MotorStop();
                Console.WriteLine("Stop ACQ Succeed");
                Console.ReadLine();
            }
            catch (Exception e)
            {
                Console.WriteLine("Stop ACQ Failed");
                Console.WriteLine("Message:");
                Console.WriteLine(e.Message);
                Console.WriteLine("StackTrace:");
                Console.WriteLine(e.StackTrace);
                Console.ReadLine();
                return;
            }
        }
    }
}