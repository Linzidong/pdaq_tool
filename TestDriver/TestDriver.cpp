// TestDriver.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <Windows.h>
#include <vector>
#include <iostream>
#include <process.h>
using namespace std;
#include <stdio.h>
#include "../PDAQ4Driver//PDAQ4DriverPublic.h"
#include "../PDAQ4Driver/RegPDAQ4.h"
#include "../PDAQ4DriverDll/PDAQ4DLL.h"
#pragma comment(lib, "../Win7Debug/PDAQ4DriverDll.lib")
#define TEST_READ_WRITE_REGISTER 0 
#define TEST_DMA_READ			 1
#define TEST_QUERY_RESOURCE		 0			
#define TEST_BLOCK_WRITE		 0	
HANDLE write_to_file_thread;
HANDLE write_to_file_event;
typedef struct __FrameHandlder
{
	volatile PUINT mot;
	HANDLE handle[2];
	DrvSharedFifo* pFifo_rf;
	DrvSharedFifo* pFifo_rt;
	DrvQueryFiFoPacket fifo_packets[2];

}FrameHandlder;
/*This thread should be very quick otherwise it will overflow*/



void GetRFFrame(UINT32 index, PDrvQueryFifoPacket fifo, RFFrame *out_frame)
{
	
	if (fifo->frame_type == RF_FRAME)
	{
	
		ULONG resource_num = fifo->packets[index].nSharedResource;
		PUCHAR mem = (PUCHAR)malloc(resource_num*fifo->packets[index].ResourceList[0].length);
		PUCHAR rf_mem = mem;
		for (ULONG j = 0; j < resource_num; j++)
		{
			memcpy(rf_mem, fifo->packets[index].ResourceList[j].pvUser, fifo->packets[index].ResourceList[j].length);
			rf_mem += fifo->packets[index].ResourceList[j].length;
		}
		out_frame = (RFFrame*)mem;
	}
}


void GetRTFrame(UINT32 index, PDrvQueryFifoPacket fifo, RTFrame **out_frame)
{

	if (fifo->frame_type == RT_FRAME)
	{

		ULONG resource_num = fifo->packets[index].nSharedResource;
		PUCHAR mem = (PUCHAR)malloc(resource_num*fifo->packets[index].ResourceList[0].length);
		PUCHAR rf_mem = mem;
		for (ULONG j = 0; j < resource_num; j++)
		{
			memcpy(rf_mem, fifo->packets[index].ResourceList[j].pvUser, fifo->packets[index].ResourceList[j].length);
			rf_mem += fifo->packets[index].ResourceList[j].length;
		}
		*out_frame = (RTFrame*)mem;

	}
}

unsigned __stdcall FrameHandler_Thread(void *p)
{

	FrameHandlder *handler = (FrameHandlder*)p;
	*(handler->mot) = 0x01;
	
	DWORD ret;
	DWORD interrupt_num = 0;
	static UINT32 index = 0;
	
	while (TRUE)
	{
		ret = WaitForMultipleObjects(2, handler->handle, TRUE, 500);
		if (ret == WAIT_TIMEOUT)
			break;
		index++;
		//cout  << " Frame Index=" << index << endl;
		(handler->pFifo_rf->nRead)++;
		(handler->pFifo_rt->nRead)++;
		//SetEvent(write_to_file_event);
		
	}
	cout  << " Frame Index=" << dec<<index << endl;
	return 0;
}


unsigned __stdcall Write_To_File(void *p)
{

	FrameHandlder *handler = (FrameHandlder*)p;
	FILE *f1 = fopen("RT_FRAMES.txt", "wt");
	while (TRUE)
	{
		DWORD ret = WaitForSingleObject(write_to_file_event, 500);
		if (ret == WAIT_TIMEOUT)
			break;
		RTFrame *prt = NULL;
		UINT32 index = (handler->pFifo_rt->nRead - 1) % handler->fifo_packets[RT_FRAME].nFifoElement;
		GetRTFrame(index, &handler->fifo_packets[RT_FRAME], &prt);
		
		//cout << " RT Frame Index=" <<dec<< handler->pFifo_rt->nRead - 1 <<"  "<<hex<<((*(ULONG*)prt))<< endl;

		
		free(prt);

	}
	fclose(f1);
	return 0;
}






int _tmain(int argc, _TCHAR* argv[])
{
	DrvConnectFifoInPacket connectpacket, connectpacket2;
	DrvConnectFifoOutPacket outconnectpacket, outconnectpacket2;
	ULONG size = sizeof(Frame);
	printf("Size Of Frame is %d\n", sizeof(Frame));
	PDAQ4CARD PdaqCard;
	UINT32 DAMDescriptorSize = sizeof(DMADescriptor);
	PUCHAR abc = (PUCHAR)malloc(0x100000);
	PUCHAR abc2 = (PUCHAR)malloc(0x100000);
	memcpy(abc, abc2, 0x100000);
	PDAQ4_STATUS  status = FindPDAQ4(&PdaqCard);
	if (status == PDAQ4_DLL_STATUS_NO_PDAQ4_PRESENT)
	{
		cout << "There is no PDAQ4 card present" <<endl;
		return status;
	}
	else
	{
		cout << "PDAQ4 Card Name is:" <<PdaqCard.cardname<<endl;
		cout << "PDAQ4 Card Hardware ID " << PdaqCard.hardwardid << endl;
		cout << "PDAQ4 Card VID " << PdaqCard.VID << endl;
		cout << "PDAQ4 Card DID " << PdaqCard.PID << endl;
	}
	status = OpenPDAQ4();
	if (status != PDAQ4_DLL_STATUS_SUCCESS)
	{
		cout << "Open the card Failed, the status is " << GetLastError() << endl;
		
		return 0;
	}
#if TEST_READ_WRITE_REGISTER
	unsigned int reg_space_length = 0;
	status = ReadPDAQ4RegSpaceBytesLength(&reg_space_length);
	if (status != PDAQ4_DLL_STATUS_SUCCESS)
	{
		cout << "Read Reg Space Failed, stauts is .. " << GetLastError() << endl;
		return 0;
	}
	else
	{
		cout << "The length for the reg space is  " << hex << reg_space_length << endl;
	}

	UINT32 offset_DAM_Descriptor = 0x01000 / sizeof(UINT32);

	//Write some random number to the DAMDescriptor registers and readout later, you can use rwevery.exe to 
	//verify that
	DMADescriptor descp_to_write_1, descp_to_write_2, descp_to_write_3;
	descp_to_write_1.msb = 0;
	descp_to_write_1.lsb = 0x100;
	descp_to_write_1.next_descriptor = 1;
	descp_to_write_1.transfer_size = 0x100000000;
	descp_to_write_1.end_of_chain = 0;
	descp_to_write_1.eot_interrupt = 0;
	descp_to_write_1.read_channel = 0;
	descp_to_write_2.msb = 0;
	descp_to_write_2.lsb = 0x100;
	descp_to_write_2.next_descriptor = 2;
	descp_to_write_2.transfer_size = 0x100000000;
	descp_to_write_2.end_of_chain = 0;
	descp_to_write_2.eot_interrupt = 0;
	descp_to_write_2.read_channel = 1;
	descp_to_write_3.msb = 0;
	descp_to_write_3.lsb = 0x100;
	descp_to_write_3.next_descriptor = 0;
	descp_to_write_3.transfer_size = 0x100000000;
	descp_to_write_3.end_of_chain = 1;
	descp_to_write_3.eot_interrupt = 0;
	descp_to_write_3.read_channel = 0;

	DMADescriptor descp_array[3] = { descp_to_write_1, descp_to_write_2, descp_to_write_3 };

	UINT32 * reg_value_to_write = (UINT32*)descp_array;

	for (int i = 0; i < sizeof(descp_array) / sizeof(UINT32); i++)
	{
		WritePDAQ4Reg(offset_DAM_Descriptor, *reg_value_to_write);
		offset_DAM_Descriptor++;
		reg_value_to_write++;
	}
	
	

	byte *reg_space = (byte*)malloc(reg_space_length);
	unsigned int actual_reg_space_length = 0;
	status = ReadPDAQ4RegSpace(reg_space, reg_space_length, &actual_reg_space_length);
	if (status != PDAQ4_DLL_STATUS_SUCCESS)
	{
		cout << "Read Regs Failed, stauts is .. " << GetLastError() << endl;
		return 0;
	}
	else
	{
		cout << "The read length for the reg space is  " << hex << actual_reg_space_length << endl;
		// 4byte for one register
		if (actual_reg_space_length % sizeof(UINT32) != 0)
		{
			cout << "The read length for the reg space cannot be divided event by 4  " << endl;
		}
		UINT32 * reg_space_uint32 = (UINT32*)reg_space;
		unsigned int num_of_reg = actual_reg_space_length / sizeof(UINT32);
		//Print Part of The value
		// Read The Scatter/Gatter descriptitor Table
		//0x01000-0x01FFF
		offset_DAM_Descriptor = 0x01000 / sizeof(UINT32);
		reg_space_uint32 += offset_DAM_Descriptor;
		UINT32 DAMDescriptorSize = sizeof(DMADescriptor);
		//Print out 3 Descriptors
		for (int i = 0; i < 3; i++)
		{
			DMADescriptor *desp = (DMADescriptor *)reg_space_uint32;
			cout << "Descriptor " << i << endl;
			cout << "     MSB " << hex <<desp->msb << endl;
			cout << "     LSB " << hex <<desp->lsb << endl;
			cout << "     Transfer Length " << desp->transfer_size << endl;
			cout << "     Next_descriptor is " << desp->next_descriptor << endl;
			cout << "     Read Channel is " << desp->read_channel << endl;
			cout << "     End Chain is " << desp->end_of_chain << endl;
			cout << "     EOT " << desp->eot_interrupt<< endl;
			reg_space_uint32+=4;
		}


	}
	free(reg_space);
#endif
#if TEST_DMA_READ_OLD

	status = StartMotor();
	printf("Size Of Frame is %d", sizeof(Frame));
	Frame *frame = (Frame*)malloc(sizeof(Frame));
	ZeroMemory(frame, sizeof(Frame));
	for (int i = 0; i <2; i++)
	{
		ZeroMemory(frame, sizeof(Frame));
		status = ReadFrame(frame, 100);
		printf("RF Frame Header %X\n", frame->rfframe.frameheader.sync_pattern);
		printf("RT Frame Header %X\n", frame->ivusdataframe.frameheader.sync_pattern);
		FILE *f1 = fopen("DATA.txt", "wt");
		
		for (int i = 0; i < sizeof(Frame) / sizeof(ULONG); i++)
		{
			ULONG * value = (ULONG *)(&frame->rfframe);
			value += i;
			char temp[10];
			sprintf(temp, "%08X\n", *value);
			fputs(temp, f1);

		}
		fclose(f1);

	}
	free(frame);
	status = StopMotor();
#endif
#if TEST_BLOCK_WRITE
	PDAQ4CARDResource resource;
	status = PDAQ4Init();
	QueryPDAQ4Resource(&resource);
	DrvQueryFiFoPacket packet[2];
	QueryFIFO(packet);
	unsigned int len;
	ReadPDAQ4RegSpaceBytesLength(&len);
	byte *bytes =(byte*) malloc(0x100);
	unsigned int regblock[64];
	for (int i = 0; i < 64; i++)
	{
		regblock[i] = i;
	}

	WritePDAQ4RegBlock(0x100,regblock,64);
#endif

	
	PDAQ4CARDResource resource;
	status = PDAQ4Init();
	if (status = 0)
	{
		printf("Init OK");
	}
	QueryPDAQ4Resource(&resource);

	DrvQueryFiFoPacket packet[2];
	QueryFIFO(packet);
	unsigned int len;
	ReadPDAQ4RegSpaceBytesLength(&len);
	//byte *bytes =(byte*) malloc(0x100000);
	//ReadPDAQ4RegSpace(bytes, 0x100000,&len);

	PUCHAR dma = resource.virtual_regbase + 0xD4;
	PUINT  dma_reg = (PUINT)dma;
	UINT start_dma = *dma_reg | 0x400;
	*dma_reg = start_dma;

	PUCHAR mot = resource.virtual_regbase + 0x4;
	volatile PUINT  mot2 = (PUINT)mot;
	unsigned int tid,tid2 = 0;

	HANDLE handle[2];

	handle[0] = CreateEvent(NULL, FALSE, FALSE, NULL);
	handle[1] = CreateEvent(NULL, FALSE, FALSE, NULL);

	connectpacket.frame_type = RF_FRAME;
	connectpacket.hEvent = handle[0];

	connectpacket2.frame_type = RT_FRAME;
	connectpacket2.hEvent = handle[1];
	PDAQ4ConnectFIFOChannel(&connectpacket, &outconnectpacket);
	PDAQ4ConnectFIFOChannel(&connectpacket2, &outconnectpacket2);
	FrameHandlder handler;
	handler.handle[0]= handle[0];
	handler.handle[1]= handle[1];
	handler.mot = mot2;
	handler.pFifo_rf = outconnectpacket.pFifo;
	handler.pFifo_rt = outconnectpacket2.pFifo;
	handler.fifo_packets[0] = packet[0];
	handler.fifo_packets[1] = packet[1];
	write_to_file_event = CreateEvent(NULL, FALSE, FALSE, NULL);
	HANDLE th = (HANDLE)_beginthreadex(NULL, 0, FrameHandler_Thread, &handler, 0, &tid);
	
	//HANDLE write_to_file_thread = (HANDLE)_beginthreadex(NULL, 0, Write_To_File, &handler, 0, &tid2);

	
	Sleep(1000);
	*mot2 = 0x0;
	
	printf("Size Of Frame is %d", sizeof(Frame));
	DrvDebugInfo debuginfo;
	WaitForSingleObject(th, 1000);
	PDAQ4QueryDebugInfo(&debuginfo);

	
	FILE *f1 = fopen("DATA2.txt", "wt");
	
	ULONG elementnum = packet[0].nFifoElement;
	ULONG elementnum2 = packet[1].nFifoElement;
	PUCHAR rf_mem, rt_mem;
	std::vector<RFFrame *> rframelist;
	std::vector<RTFrame *> rtramelist;
	for (ULONG i = 0; i < elementnum; i++)
	{
		ULONG resource_num = packet[0].packets[i].nSharedResource;
		rf_mem = (PUCHAR)malloc(0x100000 * resource_num);
		RFFrame *frame = (RFFrame*)rf_mem;
		rframelist.push_back(frame);
		for (ULONG j = 0; j < resource_num; j++)
		{
			memcpy(rf_mem, packet[0].packets[i].ResourceList[j].pvUser, packet[0].packets[i].ResourceList[j].length);
			rf_mem += packet[0].packets[i].ResourceList[j].length;
		}


	}

	for (ULONG i = 0; i < elementnum2; i++)
	{
		ULONG resource_num = packet[1].packets[i].nSharedResource;
		rt_mem = (PUCHAR)malloc(0x100000 * resource_num);
		RTFrame *frame = (RTFrame*)rt_mem;
		rtramelist.push_back(frame);
		for (ULONG j = 0; j < resource_num; j++)
		{
			memcpy(rt_mem, packet[1].packets[i].ResourceList[j].pvUser, packet[1].packets[i].ResourceList[j].length);
			rt_mem += packet[1].packets[i].ResourceList[j].length;
		}

	}

	std::vector<RFFrame *>::iterator itor = rframelist.begin();
	
	while (itor != rframelist.end())
	{
		RFFrame * frame = *itor;
		ULONG * value = (ULONG *)frame;
		for (int i = 0; i < sizeof(RFFrame) / sizeof(ULONG); i++)
		{
			char temp[10];
			sprintf(temp, "%08X\n", *value);
			fputs(temp, f1);
			value += 1;

		}
		

		itor++;
	}

	std::vector<RTFrame *>::iterator itor2 = rtramelist.begin();
	while (itor2 != rtramelist.end())
	{
		RTFrame * frame = *itor2;
		ULONG * value = (ULONG *)frame;
		for (int i = 0; i < sizeof(RFFrame) / sizeof(ULONG); i++)
		{
			char temp[10];
			sprintf(temp, "%08X\n", *value);
			fputs(temp, f1);
			value += 1;

		}


		itor2++;
	}
	
	fclose(f1);

	

	ClosePDAQ4();

	return 0;
}

