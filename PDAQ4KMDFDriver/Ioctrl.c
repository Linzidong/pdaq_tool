#include "Precomp.h"
#include "Trace.h"
#include "ioctrl.tmh"
#include <stdlib.h>

#define READ_REG_NUM (8)

#define Delay_One_MicroSecond (-10)
#define Delay_One_MilliSecond (Delay_One_MicroSecond * 1000)
void MySleep(LONG msec)
{
	LARGE_INTEGER li;
	li.QuadPart = Delay_One_MilliSecond;
	li.QuadPart *= msec;
	KeDelayExecutionThread(KernelMode, 0, &li);
}

//VOID PDAQ4_IOCTL_InitFPGA(WDFQUEUE Queue, WDFREQUEST Request, WDF_REQUEST_PARAMETERS* param) {
//	PDEVICE_EXTENSION devExt = NULL;
//	devExt = PDAQ4GetDeviceContext(WdfIoQueueGetDevice(Queue));
//
//	PUCHAR Addr_DSP = NULL;
//	PUCHAR Addr_ADC = NULL;
//	PUCHAR Addr_TXHold_RF = NULL;
//	PUCHAR Addr_TXHold_RT = NULL;
//	PUCHAR Addr_TXConrol = NULL;
//	PUCHAR Addr_MDU_Control = NULL;
//	PUCHAR Addr_RX_AFE = NULL;
//	PUCHAR Addr_RF_FRAMER = NULL;
//	PUCHAR Addr_RT_FRAMER = NULL;
//	PUCHAR Addr_PDAQ_I2C = NULL;
//
//	Addr_DSP = devExt->FpgaFunRegBase + OFFSET_DSP;
//	Addr_TXHold_RF = devExt->FpgaFunRegBase + OFFSET_RF_TXHOLDOFF;
//	Addr_TXHold_RT = devExt->FpgaFunRegBase + OFFSET_RT_TXHOLDOFF;
//	Addr_ADC = devExt->FpgaFunRegBase + OFFSET_ADC_RECEIVER;
//	Addr_MDU_Control = devExt->FpgaFunRegBase + OFFSET_MDU_CONTROL;
//	Addr_TXConrol = devExt->FpgaFunRegBase + OFFSET_TX_CONTROL;
//	Addr_RX_AFE = devExt->FpgaFunRegBase + OFFSET_RX_AFE;
//	Addr_RF_FRAMER = devExt->FpgaFunRegBase + OFFSET_RF_FRAMER;
//	Addr_RT_FRAMER = devExt->FpgaFunRegBase + OFFSET_RT_FRAMER;
//	Addr_PDAQ_I2C = devExt->FpgaFunRegBase + OFFSET_PDAQ_I2C;
//
//	// 1- ADC
//
//	// I2C
//			// Do a soft reset on the part to make sure that we don't have any leftover
//			// states from previous tests. (Ran into this a couple of times...)
//	WRITE_REGISTER_ULONG((PULONG)(Addr_PDAQ_I2C + (AD5593_CTL_CFG_WR | AD5593_REG_SOFT_RST)), 0x0DAC);
//	//Wait 0x1000000 TODO
//
//	MySleep(1);
//
//	// To configure IO0 as a DAC, write 0x0001 to the DAC register.
//	WRITE_REGISTER_ULONG((PULONG)(Addr_PDAQ_I2C + (AD5593_CTL_CFG_WR | AD5593_REG_DAC_PIN)), 0x0001);
//
//	//Enable the internal reference.
//	WRITE_REGISTER_ULONG((PULONG)(Addr_PDAQ_I2C + (AD5593_CTL_CFG_WR | AD5593_REG_PD_REF)), 0x0200);
//
//	// Set the voltage. 0 = 0V and 0x0FFF = 2.5V so r/1.44 = 0x0FFF/2.5
//	// = 4095/2.5, so r = 2359 = 0x0937
//	WRITE_REGISTER_ULONG((PULONG)(Addr_PDAQ_I2C + AD5593_CTL_DAC_WR), 0x0937);
//
//	// 2- Motor
//		//Control
//	WRITE_REGISTER_ULONG((PULONG)(Addr_MDU_Control + 0x04), 0x00);
//	//Watchdog Timeout
//	WRITE_REGISTER_ULONG((PULONG)(Addr_MDU_Control + 0x24), 1000);
//	//Watchdog Control
//	WRITE_REGISTER_ULONG((PULONG)(Addr_MDU_Control + 0x20), 0);
//
//	int rpm = 1500;
//	int speedTarget = (int)(200.0e6 * 60.0 / rpm / 200);
//	WRITE_REGISTER_ULONG((PULONG)(Addr_MDU_Control + 0x40), speedTarget);
//	WRITE_REGISTER_ULONG((PULONG)(Addr_MDU_Control + 0x44), (int)(speedTarget * 5.0 / 100.0));
//	WRITE_REGISTER_ULONG((PULONG)(Addr_MDU_Control + 0x50), (speedTarget > 0) ? 3000 : 0);
//	WRITE_REGISTER_ULONG((PULONG)(Addr_MDU_Control + 0x54), 100);
//	WRITE_REGISTER_ULONG((PULONG)(Addr_MDU_Control + 0x58), (speedTarget > 0) ? 200 : 0);
//
//	// 3- TX High Voltage and Waveform Geration
//
//		// Write a waveform to the RAM.
//		// Each pulse of 200 MHz. We want 40 MHz.
//		// 0,1,1,2,2,0... will approximate a 40 MHz chirp.
//		// Make sure the first byte is 0 so everything stays off...
//	char offset = 0;
//	WRITE_REGISTER_ULONG((PULONG)(Addr_TXConrol + OFFSET_TX_CONTROL_BURST_RAM), 0x00);
//	WRITE_REGISTER_ULONG((PULONG)(Addr_TXConrol + OFFSET_TX_CONTROL_BURST_RAM + 0x04), 0x01);
//	WRITE_REGISTER_ULONG((PULONG)(Addr_TXConrol + OFFSET_TX_CONTROL_BURST_RAM + 0x08), 0x01);
//	WRITE_REGISTER_ULONG((PULONG)(Addr_TXConrol + OFFSET_TX_CONTROL_BURST_RAM + 0x0c), 0x02);
//	WRITE_REGISTER_ULONG((PULONG)(Addr_TXConrol + OFFSET_TX_CONTROL_BURST_RAM + 0x10), 0x02);
//	WRITE_REGISTER_ULONG((PULONG)(Addr_TXConrol + OFFSET_TX_CONTROL_BURST_RAM + 0x14), 0x00);
//
//	/*WRITE_REGISTER_UCHAR((PUCHAR)(Addr_TXConrol + OFFSET_TX_CONTROL_BURST_RAM + offset++), 0);
//	WRITE_REGISTER_UCHAR((PUCHAR)(Addr_TXConrol + OFFSET_TX_CONTROL_BURST_RAM + offset++), 1);
//	WRITE_REGISTER_UCHAR((PUCHAR)(Addr_TXConrol + OFFSET_TX_CONTROL_BURST_RAM + offset++), 1);
//	WRITE_REGISTER_UCHAR((PUCHAR)(Addr_TXConrol + OFFSET_TX_CONTROL_BURST_RAM + offset++), 2);
//	WRITE_REGISTER_UCHAR((PUCHAR)(Addr_TXConrol + OFFSET_TX_CONTROL_BURST_RAM + offset++), 2);
//	WRITE_REGISTER_UCHAR((PUCHAR)(Addr_TXConrol + OFFSET_TX_CONTROL_BURST_RAM + offset++), 0);*/
//
//	// Make sure we have a long enough window to see our outputs.
//	//TX_ACTIVE_REG
//	WRITE_REGISTER_ULONG((PULONG)(Addr_TXConrol + 0xc), 6);
//	//TX_HOLDOFF_REG
//	WRITE_REGISTER_ULONG((PULONG)(Addr_TXConrol + 0x08), 45);
//
//	// 4- RX AFE (TGC)
//
//	int ramCount = 0;
//	int attenVector[] = {
//		0x00, // Baseline value. Don't change!
//		0x52,
//		0x87,
//		0x9C,
//		0xA7,
//		0xAD,
//		0xB3,
//		0xB7,
//		0xBA,
//		0xBC,
//		0xBE,
//		0xBE,
//		0xBE,
//		0xBE,
//		0xBE,
//		0xBE,
//		0xBE,
//		0xBE
//	};
//
//	int attenVectorSize = sizeof(attenVector) / sizeof(attenVector[0]);
//
//	static const int extrapCount = 8;
//	unsigned int ramVal = 0;
//	for (int c = 0; c < (attenVectorSize - 1); c++) {
//		for (int d = 0; d < extrapCount; d++) {
//			unsigned char vecVal = attenVector[c] + ((unsigned char)d) * (attenVector[c + 1] - attenVector[c]) / extrapCount;
//
//			// Figure out the associated voltage in millivolts.
//			// TODO: Upper limit it too high! This will cause non-linear behavior
//			// in some cases! This is being set as such to emulate V3.
//			static const int upperLim = (1024);
//			static const int lowerLimB = (2048 + 512);
//			static const int lowerLimC = (2048 + 512);
//			static const int dacOffset = (2047);//-64);
//			static const int dacFS = 7250;
//			int V = 1587 - vecVal * 34;
//			int VtgcB = V - upperLim;
//
//			// Make sure the voltage is not out of range.
//			if (VtgcB < -lowerLimB) VtgcB = -lowerLimB;
//			if (VtgcB > upperLim) VtgcB = upperLim;
//
//			// Figure out how much is left over for TGC C
//			int VtgcC = V - VtgcB;
//
//			// Make sure the voltage is not out of range.
//			if (VtgcC < -lowerLimC) VtgcC = -lowerLimC;
//			if (VtgcC > upperLim) VtgcC = upperLim;
//
//			// Convert the old vector value for the new DAC.
//			// The 2 LSBs are TGC 1 (B) and the MSBs are TGC 2 (C).
//			ramVal = (((VtgcC * 4000 / dacFS) + dacOffset) << 16) | ((VtgcB * 4000 / dacFS) + dacOffset);
//
//			// And load...
//			WRITE_REGISTER_ULONG((PULONG)(Addr_RX_AFE + 0x4000 + ramCount * 0x04), ramVal);
//			ramCount++;
//			//IOWR_TGC_RAM(PDAQ_RX_AFE_CONTROL_BASE, ramCount++, ramVal);
//		}
//	}
//
//	// Stretch out the last entry.
//	for (int c = 0; c < 2 * extrapCount; c++) {
//		WRITE_REGISTER_ULONG((PULONG)(Addr_RX_AFE + 0x4000 + ramCount * 0x04), ramVal);
//		ramCount++;
//		//IOWR_TGC_RAM(PDAQ_RX_AFE_CONTROL_BASE, ramCount++, ramVal);
//	}
//
//	// Write the RAM size.
//	WRITE_REGISTER_ULONG((PULONG)(Addr_RX_AFE + 0xC), ramCount - 1);
//
//	// Set a hold-off value.
//	WRITE_REGISTER_ULONG((PULONG)(Addr_RX_AFE + 0x8), 110);
//
//	// Set the control value.
//	// Bit 0 selects the HPF, 5 MHz is 0, 20 MHz is 1.
//	// Bit 1 selects the LPF, 45 MHz is 0, 88 MHz is 1.
//	// For now, set to 3.
//	WRITE_REGISTER_ULONG((PULONG)(Addr_RX_AFE + 0x04), 0x03);
//
//	// Turn on the high voltage.
//	// Now write the bit to turn on the supply.
//	WRITE_REGISTER_ULONG((PULONG)(Addr_TXConrol + 0x04), 0x01);
//
//	// 5- RF TX Holdoff
//	WRITE_REGISTER_ULONG((PULONG)(Addr_TXHold_RF + 0x04), 105);
//
//	// 6- RF Framer
//
//	// 7- RT TX Holdoff
//	WRITE_REGISTER_ULONG((PULONG)(Addr_TXHold_RT + 0x08), 0x280);
//	WRITE_REGISTER_ULONG((PULONG)(Addr_TXHold_RT + 0x04), 105);
//	// 8- RT Framer
//
//	// 9- DSP Block
//	WRITE_REGISTER_ULONG((PULONG)Addr_DSP, 0x01);
//	WRITE_REGISTER_ULONG((PULONG)(Addr_DSP + 0x04), 0x00);
//	WRITE_REGISTER_ULONG((PULONG)(Addr_DSP + 0x08), 0x00);
//	WRITE_REGISTER_ULONG((PULONG)(Addr_DSP + 0x0C), 0x00);
//	WRITE_REGISTER_ULONG((PULONG)(Addr_DSP + 0x400), 0x01);
//	WRITE_REGISTER_ULONG((PULONG)(Addr_DSP + 0x404), 0x00);
//	WRITE_REGISTER_ULONG((PULONG)(Addr_DSP + 0x408), 0x00);
//	WRITE_REGISTER_ULONG((PULONG)(Addr_DSP + 0x40C), 0x00);
//	WRITE_REGISTER_ULONG((PULONG)(Addr_DSP + 0x800), 0x00);
//	WRITE_REGISTER_ULONG((PULONG)(Addr_DSP + 0x804), 0x00);
//	WRITE_REGISTER_ULONG((PULONG)(Addr_DSP + 0x808), 0x00);
//	WRITE_REGISTER_ULONG((PULONG)(Addr_DSP + 0x80C), 0x00);
//	WRITE_REGISTER_ULONG((PULONG)(Addr_DSP + 0xC04), 0x04);
//	WRITE_REGISTER_ULONG((PULONG)(Addr_DSP + 0xC08), 0x80000);
//	WRITE_REGISTER_ULONG((PULONG)(Addr_DSP + 0xC0C), 0x00);
//	WRITE_REGISTER_ULONG((PULONG)(Addr_DSP + 0xC10), 0x00);
//
//	WRITE_REGISTER_ULONG((PULONG)(Addr_DSP + 0x100), 0x7FFF);
//	WRITE_REGISTER_ULONG((PULONG)(Addr_DSP + 0x104), 0x00);
//	WRITE_REGISTER_ULONG((PULONG)(Addr_DSP + 0x104), 0x00);
//	WRITE_REGISTER_ULONG((PULONG)(Addr_DSP + 0x10C), 0x00);
//	WRITE_REGISTER_ULONG((PULONG)(Addr_DSP + 0x110), 0x00);
//	WRITE_REGISTER_ULONG((PULONG)(Addr_DSP + 0x114), 0x00);
//	WRITE_REGISTER_ULONG((PULONG)(Addr_DSP + 0x118), 0x00);
//	WRITE_REGISTER_ULONG((PULONG)(Addr_DSP + 0x11C), 0x00);
//	WRITE_REGISTER_ULONG((PULONG)(Addr_DSP + 0x120), 0x00);
//	WRITE_REGISTER_ULONG((PULONG)(Addr_DSP + 0x124), 0x00);
//	WRITE_REGISTER_ULONG((PULONG)(Addr_DSP + 0x128), 0x00);
//	WRITE_REGISTER_ULONG((PULONG)(Addr_DSP + 0x12C), 0x00);
//	WRITE_REGISTER_ULONG((PULONG)(Addr_DSP + 0x130), 0x00);
//	WRITE_REGISTER_ULONG((PULONG)(Addr_DSP + 0x134), 0x00);
//	WRITE_REGISTER_ULONG((PULONG)(Addr_DSP + 0x138), 0x00);
//	WRITE_REGISTER_ULONG((PULONG)(Addr_DSP + 0x13C), 0x00);
//
//	WRITE_REGISTER_ULONG((PULONG)(Addr_DSP + 0x500), 0x07);
//	WRITE_REGISTER_ULONG((PULONG)(Addr_DSP + 0x504), 0xFFFE);
//	WRITE_REGISTER_ULONG((PULONG)(Addr_DSP + 0x508), 0x08);
//	WRITE_REGISTER_ULONG((PULONG)(Addr_DSP + 0x50C), 0x08);
//	WRITE_REGISTER_ULONG((PULONG)(Addr_DSP + 0x510), 0xFFE5);
//	WRITE_REGISTER_ULONG((PULONG)(Addr_DSP + 0x514), 0xFFE5);
//	WRITE_REGISTER_ULONG((PULONG)(Addr_DSP + 0x518), 0x02);
//	WRITE_REGISTER_ULONG((PULONG)(Addr_DSP + 0x51C), 0xFFDD);
//	WRITE_REGISTER_ULONG((PULONG)(Addr_DSP + 0x520), 0xFFE5);
//	WRITE_REGISTER_ULONG((PULONG)(Addr_DSP + 0x524), 0x4C);
//	WRITE_REGISTER_ULONG((PULONG)(Addr_DSP + 0x528), 0x3B);
//	WRITE_REGISTER_ULONG((PULONG)(Addr_DSP + 0x52C), 0x01);
//	WRITE_REGISTER_ULONG((PULONG)(Addr_DSP + 0x530), 0x6C);
//	WRITE_REGISTER_ULONG((PULONG)(Addr_DSP + 0x534), 0x41);
//	WRITE_REGISTER_ULONG((PULONG)(Addr_DSP + 0x538), 0xFF5A);
//	WRITE_REGISTER_ULONG((PULONG)(Addr_DSP + 0x53C), 0xFF9D);
//	WRITE_REGISTER_ULONG((PULONG)(Addr_DSP + 0x540), 0xFFEE);
//	WRITE_REGISTER_ULONG((PULONG)(Addr_DSP + 0x544), 0xFEF3);
//	WRITE_REGISTER_ULONG((PULONG)(Addr_DSP + 0x548), 0xFF7B);
//	WRITE_REGISTER_ULONG((PULONG)(Addr_DSP + 0x54C), 0x131);
//	WRITE_REGISTER_ULONG((PULONG)(Addr_DSP + 0x550), 0x82);
//	WRITE_REGISTER_ULONG((PULONG)(Addr_DSP + 0x554), 0x3E);
//	WRITE_REGISTER_ULONG((PULONG)(Addr_DSP + 0x558), 0x242);
//	WRITE_REGISTER_ULONG((PULONG)(Addr_DSP + 0x55C), 0xF6);
//	WRITE_REGISTER_ULONG((PULONG)(Addr_DSP + 0x560), 0xFE08);
//	WRITE_REGISTER_ULONG((PULONG)(Addr_DSP + 0x564), 0xFF8B);
//	WRITE_REGISTER_ULONG((PULONG)(Addr_DSP + 0x568), 0xFF5C);
//	WRITE_REGISTER_ULONG((PULONG)(Addr_DSP + 0x56C), 0xFB74);
//	WRITE_REGISTER_ULONG((PULONG)(Addr_DSP + 0x570), 0xFE59);
//	WRITE_REGISTER_ULONG((PULONG)(Addr_DSP + 0x574), 0x318);
//	WRITE_REGISTER_ULONG((PULONG)(Addr_DSP + 0x578), 0xFFEC);
//	WRITE_REGISTER_ULONG((PULONG)(Addr_DSP + 0x57C), 0x198);
//	WRITE_REGISTER_ULONG((PULONG)(Addr_DSP + 0x580), 0x990);
//	WRITE_REGISTER_ULONG((PULONG)(Addr_DSP + 0x584), 0x354);
//	WRITE_REGISTER_ULONG((PULONG)(Addr_DSP + 0x588), 0xFAAF);
//	WRITE_REGISTER_ULONG((PULONG)(Addr_DSP + 0x58C), 0x26B);
//	WRITE_REGISTER_ULONG((PULONG)(Addr_DSP + 0x590), 0xFAD6);
//	WRITE_REGISTER_ULONG((PULONG)(Addr_DSP + 0x594), 0xDFDD);
//	WRITE_REGISTER_ULONG((PULONG)(Addr_DSP + 0x598), 0xF1F9);
//	WRITE_REGISTER_ULONG((PULONG)(Addr_DSP + 0x59C), 0x2AB2);
//
//	WRITE_REGISTER_ULONG((PULONG)(Addr_DSP + 0x900), 0xFFFB);
//	WRITE_REGISTER_ULONG((PULONG)(Addr_DSP + 0xA00), 0xFFFB);
//	WRITE_REGISTER_ULONG((PULONG)(Addr_DSP + 0x904), 0xFFE4);
//	WRITE_REGISTER_ULONG((PULONG)(Addr_DSP + 0xA04), 0xFFE4);
//	WRITE_REGISTER_ULONG((PULONG)(Addr_DSP + 0x908), 0xFFE4);
//	WRITE_REGISTER_ULONG((PULONG)(Addr_DSP + 0xA08), 0xFFE4);
//	WRITE_REGISTER_ULONG((PULONG)(Addr_DSP + 0x90C), 0x0D);
//	WRITE_REGISTER_ULONG((PULONG)(Addr_DSP + 0xA0C), 0x0D);
//	WRITE_REGISTER_ULONG((PULONG)(Addr_DSP + 0x910), 0x58);
//	WRITE_REGISTER_ULONG((PULONG)(Addr_DSP + 0xA10), 0x58);
//	WRITE_REGISTER_ULONG((PULONG)(Addr_DSP + 0x914), 0x79);
//	WRITE_REGISTER_ULONG((PULONG)(Addr_DSP + 0xA14), 0x79);
//	WRITE_REGISTER_ULONG((PULONG)(Addr_DSP + 0x918), 0x19);
//	WRITE_REGISTER_ULONG((PULONG)(Addr_DSP + 0xA18), 0x19);
//	WRITE_REGISTER_ULONG((PULONG)(Addr_DSP + 0x91C), 0xFF47);
//	WRITE_REGISTER_ULONG((PULONG)(Addr_DSP + 0xA1C), 0xFF47);
//	WRITE_REGISTER_ULONG((PULONG)(Addr_DSP + 0x920), 0xFEB1);
//	WRITE_REGISTER_ULONG((PULONG)(Addr_DSP + 0xA20), 0xFEB1);
//	WRITE_REGISTER_ULONG((PULONG)(Addr_DSP + 0x924), 0xFF3D);
//	WRITE_REGISTER_ULONG((PULONG)(Addr_DSP + 0xA24), 0xFF3D);
//	WRITE_REGISTER_ULONG((PULONG)(Addr_DSP + 0x928), 0x010B);
//	WRITE_REGISTER_ULONG((PULONG)(Addr_DSP + 0xA28), 0x010B);
//	WRITE_REGISTER_ULONG((PULONG)(Addr_DSP + 0x92C), 0x02D5);
//	WRITE_REGISTER_ULONG((PULONG)(Addr_DSP + 0xA2C), 0x02D5);
//	WRITE_REGISTER_ULONG((PULONG)(Addr_DSP + 0x930), 0x028D);
//	WRITE_REGISTER_ULONG((PULONG)(Addr_DSP + 0xA30), 0x028D);
//	WRITE_REGISTER_ULONG((PULONG)(Addr_DSP + 0x934), 0xff3b);
//	WRITE_REGISTER_ULONG((PULONG)(Addr_DSP + 0xA34), 0xff3b);
//	WRITE_REGISTER_ULONG((PULONG)(Addr_DSP + 0x938), 0xfaa3);
//	WRITE_REGISTER_ULONG((PULONG)(Addr_DSP + 0xA38), 0xfaa3);
//	WRITE_REGISTER_ULONG((PULONG)(Addr_DSP + 0x93c), 0xf903);
//	WRITE_REGISTER_ULONG((PULONG)(Addr_DSP + 0xA3c), 0xf903);
//	WRITE_REGISTER_ULONG((PULONG)(Addr_DSP + 0x940), 0xfe60);
//	WRITE_REGISTER_ULONG((PULONG)(Addr_DSP + 0xA40), 0xfe60);
//	WRITE_REGISTER_ULONG((PULONG)(Addr_DSP + 0x944), 0x0b13);
//	WRITE_REGISTER_ULONG((PULONG)(Addr_DSP + 0xA44), 0x0b13);
//	WRITE_REGISTER_ULONG((PULONG)(Addr_DSP + 0x948), 0x1a69);
//	WRITE_REGISTER_ULONG((PULONG)(Addr_DSP + 0xA48), 0x1a69);
//	WRITE_REGISTER_ULONG((PULONG)(Addr_DSP + 0x94c), 0x24e2);
//	WRITE_REGISTER_ULONG((PULONG)(Addr_DSP + 0xA4c), 0x24e2);
//
//	TraceEvents(TRACE_LEVEL_INFORMATION, TRACE_DEVICE, "Program FPGA successfully");
//
//	WdfRequestCompleteWithInformation(Request, STATUS_SUCCESS, 0);
//}

VOID PDAQ4_IOCTL_WriteRegisterValue(WDFQUEUE Queue, WDFREQUEST Request, WDF_REQUEST_PARAMETERS* param) {
	UNREFERENCED_PARAMETER(param);
	TraceEvents(TRACE_LEVEL_INFORMATION, TRACE_DEVICE, "Enter PDAQ4_IOCTL_WriteRegisterValue");
	NTSTATUS                status = STATUS_SUCCESS;
	PVOID                   DataBufferIn;
	size_t                  BufferLengthIn;

	PDEVICE_EXTENSION devExt = NULL;
	devExt = PDAQ4GetDeviceContext(WdfIoQueueGetDevice(Queue));

	status = WdfRequestRetrieveInputBuffer(Request, 4, &DataBufferIn, &BufferLengthIn);

	ULONG(*params)[3];

	params = (ULONG(*)[3])DataBufferIn;
	ULONG type = *((PULONG)DataBufferIn); //(*params)[0];
	ULONG offset = *((PULONG)DataBufferIn + 1); //(*params)[1];
	ULONG value = *((PULONG)DataBufferIn + 2);//(*params)[2];

	PUCHAR base = NULL;
	KdPrintEx((DPFLTR_IHVDRIVER_ID, DPFLTR_ERROR_LEVEL, "Input type = %d, offset = %d, value = %d\n", type, offset, value));
	TraceEvents(TRACE_LEVEL_INFORMATION, TRACE_DEVICE, "Input type = %d, offset = %d, value = %d\n", type, offset, value);
	/*WdfRequestCompleteWithInformation(Request, STATUS_SUCCESS, 0);

	return;*/
	//if (type == 0) {
	//	//base = devExt->FpgaDmaRegBase;
	//	base = devExt->FpgaFunRegBase + OFFSET_RF_FRAMER;
	//}
	//else if (type == 1) {
	//	base = devExt->FpgaFunRegBase + OFFSET_RT_FRAMER;
	//}

	if (type == 0) {
		//base = devExt->FpgaDmaRegBase;
		base = devExt->FpgaFunRegBase + OFFSET_RF_FRAMER;
	}
	else if (type == 1) {
		base = devExt->FpgaFunRegBase + OFFSET_RT_FRAMER;
	}
	else if (type == 3) {
		base = 0x50;
	}
	//Bar0
	else if (type == 4) {
		base = devExt->FpgaDmaRegBase;
	}
	//Bar2
	else if (type == 5) {
		base = devExt->FpgaFunRegBase;
	}
	//MDU
	else if (type == 6) {
		base = devExt->FpgaFunRegBase + OFFSET_MDU_CONTROL;
	}
	//TX Control
	else if (type == 7) {
		base = devExt->FpgaFunRegBase + OFFSET_TX_CONTROL;
	}
	//RX AFE
	else if (type == 8) {
		base = devExt->FpgaFunRegBase + OFFSET_RX_AFE;
	}
	//DSP
	else if (type == 9) {
		base = devExt->FpgaFunRegBase + OFFSET_DSP;
	}
	//ADC
	else if (type == 10) {
		base = devExt->FpgaFunRegBase + OFFSET_ADC_RECEIVER;
	}
	//I2C
	else if (type == 11) {
		base = devExt->FpgaFunRegBase + OFFSET_PDAQ_I2C;
	}
	//RF_TXHOLD
	else if (type == 12) {
		base = devExt->FpgaFunRegBase + OFFSET_RF_TXHOLDOFF;
	}
	//RT_TXHOLD
	else if (type == 13) {
		base = devExt->FpgaFunRegBase + OFFSET_RT_TXHOLDOFF;
	}
	//ADC_SPI
	else if (type == 14) {
		base = devExt->FpgaFunRegBase + OFFSET_ADC_SPI;
	}
	PULONG address = (PULONG)(base + offset);

	WRITE_REGISTER_ULONG(address, value);

	TraceEvents(TRACE_LEVEL_INFORMATION, TRACE_DEVICE, "Write Register succeed");
	TraceEvents(TRACE_LEVEL_INFORMATION, TRACE_DEVICE, "base = %x,Address = %x, type = %d\n", (ULONG)devExt->FpgaFunRegBase, (ULONG)base, type);

	WdfRequestCompleteWithInformation(Request, STATUS_SUCCESS, 0);
}

VOID PDAQ4_IOCTL_ReadRegisterValue(WDFQUEUE Queue, WDFREQUEST Request, WDF_REQUEST_PARAMETERS* param) {
	UNREFERENCED_PARAMETER(param);
	TraceEvents(TRACE_LEVEL_INFORMATION, TRACE_DEVICE, "Enter PDAQ4_IOCTL_ReadRegisterValue");

	NTSTATUS                status = STATUS_SUCCESS;
	PVOID                   DataBufferOut;
	PVOID                   DataBufferIn;
	size_t                  BufferLengthOut;
	size_t                  BufferLengthIn;
	PDEVICE_EXTENSION devExt = NULL;
	devExt = PDAQ4GetDeviceContext(WdfIoQueueGetDevice(Queue));

	status = WdfRequestRetrieveInputBuffer(Request, 4, &DataBufferIn, &BufferLengthIn);
	ULONG(*params)[3];

	params = (ULONG(*)[3])DataBufferIn;
	ULONG type = *((PULONG)DataBufferIn); //(*params)[0];
	ULONG offset = *((PULONG)DataBufferIn + 1); //(*params)[1];
	ULONG number = *((PULONG)DataBufferIn + 2);//(*params)[2];

	PUCHAR base = NULL;
	KdPrintEx((DPFLTR_IHVDRIVER_ID, DPFLTR_ERROR_LEVEL, "Input type = %d, offset = %d, number = %d\n", type, offset, number));

	TraceEvents(TRACE_LEVEL_INFORMATION, TRACE_DEVICE, "Input type = %d, offset = %d, number = %d\n", type, offset, number);

	if (type == 0) {
		//base = devExt->FpgaDmaRegBase;
		base = devExt->FpgaFunRegBase + OFFSET_RF_FRAMER;
	}
	else if (type == 1) {
		base = devExt->FpgaFunRegBase + OFFSET_RT_FRAMER;
	}
	else if (type == 3) {
		base = 0x50;
	}//Bar0
	else if (type == 4) {
		base = devExt->FpgaDmaRegBase;
	}//bar2
	else if (type == 5) {
		base = devExt->FpgaFunRegBase;
	}
	//MDU
	else if (type == 6) {
		base = devExt->FpgaFunRegBase + OFFSET_MDU_CONTROL;
	}
	//TX Control
	else if (type == 7) {
		base = devExt->FpgaFunRegBase + OFFSET_TX_CONTROL;
	}
	//RX AFE
	else if (type == 8) {
		base = devExt->FpgaFunRegBase + OFFSET_RX_AFE;
	}
	//DSP
	else if (type == 9) {
		base = devExt->FpgaFunRegBase + OFFSET_DSP;
	}
	//ADC
	else if (type == 10) {
		base = devExt->FpgaFunRegBase + OFFSET_ADC_RECEIVER;
	}
	//I2C
	else if (type == 11) {
		base = devExt->FpgaFunRegBase + OFFSET_PDAQ_I2C;
	}
	//RF_TXHOLD
	else if (type == 12) {
		base = devExt->FpgaFunRegBase + OFFSET_RF_TXHOLDOFF;
	}
	//RT_TXHOLD
	else if (type == 13) {
		base = devExt->FpgaFunRegBase + OFFSET_RT_TXHOLDOFF;
	}
	//ADC_SPI
	else if (type == 14) {
		base = devExt->FpgaFunRegBase + OFFSET_ADC_SPI;
	}

	//strcpy(DataBufferIn, &params);

	status = WdfRequestRetrieveOutputBuffer(Request, number * sizeof(RegisterPacket), &DataBufferOut, &BufferLengthOut);

	//PRegisterPacket pReg = (PRegisterPacket)DataBufferOut;
	//ULONG(*output)[READ_REG_NUM];

	PRegisterPacket buffer = (PRegisterPacket)DataBufferOut;
	//PULONG buffer = (PULONG)DataBufferOut;
	PULONG address = (PULONG)(base + offset);

	//output = (ULONG(*)[READ_REG_NUM])DataBufferOut;
	for (ULONG i = 0; i < number; i++)
	{
		buffer->Address = address;
		buffer->Value = READ_REGISTER_ULONG((PULONG)(address));
		//PUCHAR address = base + offset + (i * 4);
	  //*buffer = READ_REGISTER_ULONG((PULONG)(address));

		//*output[i] = registerValue;

		//pReg++;
		KdPrintEx((DPFLTR_IHVDRIVER_ID, DPFLTR_ERROR_LEVEL, "output Address = %x, Value = %d\n", (ULONG)(buffer->Address), buffer->Value));
		TraceEvents(TRACE_LEVEL_INFORMATION, TRACE_DEVICE, "output Address = %x, Value = %d\n", (ULONG)(buffer->Address), buffer->Value);
		buffer++;
		address++;
	}

	//PULONG pValue = (PULONG)DataBufferOut;

	//*pValue = registerValue;

	////DrvQueryResourcePacket* pQuery = (DrvQueryResourcePacket*)DataBuffer;

	//if (pValue == NULL)
	//{
	//	WdfRequestComplete(Request, STATUS_INVALID_PARAMETER);
	//	return;
	//}
	TraceEvents(TRACE_LEVEL_INFORMATION, TRACE_DEVICE, "base = %x,Address = %x, type = %d\n", (ULONG)devExt->FpgaFunRegBase, (ULONG)base, type);

	TraceEvents(TRACE_LEVEL_INFORMATION, TRACE_DEVICE, "Exit PDAQ4_IOCTL_ReadRegisterValue");

	WdfRequestCompleteWithInformation(Request, STATUS_SUCCESS, number * sizeof(RegisterPacket));
}

//VOID PDAQ4_IOCTL_ReadRegisterValue(WDFQUEUE Queue, WDFREQUEST Request, WDF_REQUEST_PARAMETERS* param) {
//	UNREFERENCED_PARAMETER(param);
//	TraceEvents(TRACE_LEVEL_INFORMATION, TRACE_DEVICE, "Enter PDAQ4_IOCTL_ReadRegisterValue");
//
//	NTSTATUS                status = STATUS_SUCCESS;
//	PVOID                   DataBufferOut;
//	PVOID                   DataBufferIn;
//	size_t                  BufferLengthOut;
//	size_t                  BufferLengthIn;
//	PDEVICE_EXTENSION devExt = NULL;
//	devExt = PDAQ4GetDeviceContext(WdfIoQueueGetDevice(Queue));
//
//	status = WdfRequestRetrieveInputBuffer(Request, 4, &DataBufferIn, &BufferLengthIn);
//	ULONG (*params)[3];
//
//	params = (ULONG (*) [3])DataBufferIn;
//	ULONG type = (*params)[0];
//	ULONG offset = (*params)[1];
//	ULONG number = (*params)[2];
//
//
//
//	PUCHAR base = NULL;
//
//	TraceEvents(TRACE_LEVEL_INFORMATION, TRACE_DEVICE, "Input type = %d, offset = %d, number = %d\n",type,offset,number);
//	if (type == 0) {
//		base = devExt->FpgaDmaRegBase;
//	}
//	else if (type == 1) {
//		base = devExt->FpgaFunRegBase;
//	}
//	//strcpy(DataBufferIn, &params);
//
//	status = WdfRequestRetrieveOutputBuffer(Request, number * sizeof(ULONG), &DataBufferOut, &BufferLengthOut);
//
//	PULONG pBuffer = (PULONG)DataBufferOut;
//
//	READ_REGISTER_BUFFER_ULONG((PULONG)(base + offset), pBuffer, number);
//
//	/*for (ULONG i = 0; i < number; i++)
//	{
//		PUCHAR address = base + (offset + (i * 4));
//		ULONG registerValue = READ_REGISTER_ULONG((PULONG)(address));
//
//		pReg->Address = address;
//		pReg->Value = registerValue;
//
//		pReg++;
//		TraceEvents(TRACE_LEVEL_INFORMATION, TRACE_DEVICE, "output Address = %x, Value = %d\n", (ULONG)address, registerValue);
//		TraceEvents(TRACE_LEVEL_INFORMATION, TRACE_DEVICE, "pReg output Address = %x, Value = %d\n", (ULONG)(pReg -> Address), pReg->Value);
//	}*/
//
//	//PULONG pValue = (PULONG)DataBufferOut;
//
//	//*pValue = registerValue;
//
//	////DrvQueryResourcePacket* pQuery = (DrvQueryResourcePacket*)DataBuffer;
//
//	//if (pValue == NULL)
//	//{
//	//	WdfRequestComplete(Request, STATUS_INVALID_PARAMETER);
//	//	return;
//	//}
//
//
//
//	TraceEvents(TRACE_LEVEL_INFORMATION, TRACE_DEVICE, "Exit PDAQ4_IOCTL_ReadRegisterValue");
//
//
//	WdfRequestCompleteWithInformation(Request, STATUS_SUCCESS, 4);
//}

VOID
PDAQ4_IOCTL_QueryResource(WDFQUEUE Queue, WDFREQUEST Request, WDF_REQUEST_PARAMETERS* param)
{
	UNREFERENCED_PARAMETER(param);
	NTSTATUS                status = STATUS_SUCCESS;
	PVOID                   DataBuffer;
	size_t                  BufferLength;
	PDEVICE_EXTENSION devExt = NULL;
	devExt = PDAQ4GetDeviceContext(WdfIoQueueGetDevice(Queue));

	status = WdfRequestRetrieveOutputBuffer(Request, sizeof(DrvQueryResourcePacket), &DataBuffer, &BufferLength);
	DrvQueryResourcePacket* pQuery = (DrvQueryResourcePacket*)DataBuffer;

	if (pQuery == NULL)
	{
		WdfRequestComplete(Request, STATUS_INVALID_PARAMETER);
		return;
	}

	MapSharedMemory(&devExt->FPGARegisterFunc);

	TraceEvents(TRACE_LEVEL_INFORMATION, TRACE_DEVICE, "FPGARegisterFunc -> physAddr = %x", devExt->FPGARegisterFunc.physAddr.QuadPart);
	TraceEvents(TRACE_LEVEL_INFORMATION, TRACE_DEVICE, "FPGARegisterFunc -> pvUser = %x", devExt->FPGARegisterFunc.pvUser);
	TraceEvents(TRACE_LEVEL_INFORMATION, TRACE_DEVICE, "FPGARegisterFunc -> physAddr = %x", devExt->FPGARegisterFunc.length);

	memcpy(&pQuery->resource[0], &devExt->FPGARegisterFunc, sizeof(DrvSharedResource));

	WdfRequestCompleteWithInformation(Request, STATUS_SUCCESS, sizeof(DrvQueryResourcePacket));
}

VOID
PDAQ4_IOCTL_Init(WDFQUEUE Queue, WDFREQUEST Request, WDF_REQUEST_PARAMETERS* param)
{
	UNREFERENCED_PARAMETER(param);
	NTSTATUS                status = STATUS_SUCCESS;
	PDEVICE_EXTENSION devExt = NULL;
	devExt = PDAQ4GetDeviceContext(WdfIoQueueGetDevice(Queue));

	//UnmapSharedMemory(&devExt->FPGARegister);
	UnmapSharedMemory((PDrvSharedResource)devExt->PSharedFifo[0]);
	UnmapSharedMemory((PDrvSharedResource)devExt->PSharedFifo[1]);
	//UnmapSharedMemory((PDrvSharedResource)devExt->PSharedFifo[2]);

	for (ULONG i = 0; i < devExt->Shared_Resource_Num; i++)
	{
		UnmapSharedMemory(&devExt->SharedResourceList[i]);
	}
	for (ULONG i = 0; i < devExt->Diag_Shared_Resource_Num; i++)
	{
		UnmapSharedMemory(&devExt->Diag_SharedResourceList[i]);
	}

	// Rest the PDAQ4
	//RESET_PDAQ(devExt->ConfigSpace);

	//while (*((PREG32)&(devExt->ConfigSpace->rSoftReset))!=0xE0000000)
	{
		KeStallExecutionProcessor(500000);
	}
	RtlZeroMemory(&devExt->debug_info, sizeof(DrvDebugInfo));
	WdfRequestComplete(Request, status);
}

VOID PDAQ4_IOCTL_GetDBGInfo(WDFQUEUE Queue, WDFREQUEST Request, WDF_REQUEST_PARAMETERS* param) {
	PDEVICE_EXTENSION devExt = NULL;
	devExt = PDAQ4GetDeviceContext(WdfIoQueueGetDevice(Queue));
	TraceEvents(TRACE_LEVEL_ERROR, TRACE_DEVICE, "RegsBase = %p\n", (devExt->RegsBase));
	TraceEvents(TRACE_LEVEL_ERROR, TRACE_DEVICE, "FpgaFunRegBase = %p\n", (devExt->FpgaFunRegBase));
}

VOID PDAQ4_IOCTL_QueryDebugInfo(WDFQUEUE Queue, WDFREQUEST Request, WDF_REQUEST_PARAMETERS* param)
{
	UNREFERENCED_PARAMETER(param);
	NTSTATUS                status = STATUS_SUCCESS;
	PVOID                   DataBuffer;
	size_t                  BufferLength;
	PDEVICE_EXTENSION devExt = NULL;
	devExt = PDAQ4GetDeviceContext(WdfIoQueueGetDevice(Queue));

	status = WdfRequestRetrieveOutputBuffer(Request, sizeof(DrvDebugInfo), &DataBuffer, &BufferLength);

	PDrvDebugInfo pQuery = (PDrvDebugInfo)DataBuffer;

	if (pQuery == NULL)
	{
		WdfRequestComplete(Request, STATUS_INVALID_PARAMETER);
		return;
	}
	RtlCopyMemory(pQuery, &devExt->debug_info, sizeof(DrvDebugInfo));
	WdfRequestCompleteWithInformation(Request, STATUS_SUCCESS, BufferLength);
}

VOID PDAQ4_IOCTL_InitializeDMA(WDFQUEUE Queue, WDFREQUEST Request, WDF_REQUEST_PARAMETERS* param)
{
	NTSTATUS    status = STATUS_SUCCESS;
	PDEVICE_EXTENSION DevExt = NULL;
	DevExt = PDAQ4GetDeviceContext(WdfIoQueueGetDevice(Queue));

	PVOID                   DataBuffer;
	size_t                  BufferLength;

	status = WdfRequestRetrieveInputBuffer(Request, sizeof(DrvInitRequest), &DataBuffer, &BufferLength);
	if (!NT_SUCCESS(status))
	{
		WdfRequestComplete(Request, status);
		return;
	}

	PDrvInitRequest p_initrequest = (PDrvInitRequest)DataBuffer;
	//Set_FIFO_Parameters(DevExt, FRAME_SIZE(VECTOR_NUM_PER_FRAME_256, SAMPLE_PER_VECTOR_1024, RF_SAMPLE_BYTES), FRAME_SIZE(VECTOR_NUM_PER_FRAME_256, SAMPLE_PER_VECTOR_256, RT_SAMPLE_BYTES));
	status = PDAQ4InitWriteDMA(DevExt, p_initrequest->n_rfframesize, p_initrequest->n_rtframesize);
	//get raw rf frame sizef from rfframesize
	ULONG rawrfsize = 0;
	if (p_initrequest->n_rfframesize == 528480)
	{
		rawrfsize = 524288;
	}
	else if (p_initrequest->n_rfframesize == 1052768)
	{
		rawrfsize = 1048576;
	}
	else if (p_initrequest->n_rfframesize == 1056864)
	{
		rawrfsize = 1048576;
	}
	else if (p_initrequest->n_rfframesize == 2105440)
	{
		rawrfsize = 2097152;
	}
	//status = PDAQ4InitReadDMA(DevExt, rawrfsize);
	WdfRequestComplete(Request, status);
}

VOID
PDAQ4_IOCTL_QueryFIFO(WDFQUEUE Queue, WDFREQUEST Request, WDF_REQUEST_PARAMETERS* param)
{
	UNREFERENCED_PARAMETER(param);
	NTSTATUS                status = STATUS_SUCCESS;
	PVOID                   DataBuffer;
	size_t                  BufferLength;
	PDEVICE_EXTENSION devExt = NULL;
	TraceEvents(TRACE_LEVEL_INFORMATION, TRACE_DEVICE, "QueryFIFO Enter");
	devExt = PDAQ4GetDeviceContext(WdfIoQueueGetDevice(Queue));

	status = WdfRequestRetrieveOutputBuffer(Request, sizeof(DrvQueryFiFoPacket), &DataBuffer, &BufferLength);

	if (BufferLength != sizeof(DrvQueryFiFoPacket) * FRAME_FIFO_NUM)
	{
		TraceEvents(TRACE_LEVEL_INFORMATION, TRACE_DEVICE, "QueryFIFO sizeof(DrvSharedResource) = %d", sizeof(DrvSharedResource));
		TraceEvents(TRACE_LEVEL_INFORMATION, TRACE_DEVICE, "QueryFIFO BufferLength = %d", BufferLength);
		TraceEvents(TRACE_LEVEL_INFORMATION, TRACE_DEVICE, "QueryFIFO sizeof(DrvQueryFiFoPacket) = %d", sizeof(DrvQueryFiFoPacket));
		TraceEvents(TRACE_LEVEL_INFORMATION, TRACE_DEVICE, "QueryFIFO BufferLength require = %d", sizeof(DrvQueryFiFoPacket) * FRAME_FIFO_NUM);
		WdfRequestComplete(Request, STATUS_INVALID_PARAMETER);
		return;
	}

	KdPrintEx((DPFLTR_IHVDRIVER_ID, DPFLTR_ERROR_LEVEL, "QueryFIFO BufferLength = %d\n ", BufferLength));
	DrvQueryFiFoPacket* pQuery = (DrvQueryFiFoPacket*)DataBuffer;

	if (pQuery == NULL)
	{
		TraceEvents(TRACE_LEVEL_INFORMATION, TRACE_DEVICE, "pQuery == null");
		WdfRequestComplete(Request, STATUS_INVALID_PARAMETER);
		return;
	}

	for (ULONG i = 0; i < devExt->Shared_Resource_Num; i++)
	{
		MapSharedMemory(&devExt->SharedResourceList[i]);
	}

	for (ULONG i = 0; i < devExt->Diag_Shared_Resource_Num; i++)
	{
		MapSharedMemory(&devExt->Diag_SharedResourceList[i]);
	}

	pQuery->nFifoElement = devExt->Frame_FIFO_size;
	pQuery->frame_type = RF_FRAME;
	for (ULONG j = 0; j < devExt->Frame_FIFO_size; j++)
	{
		pQuery->packets[j].nSharedResource = devExt->PSharedFifo[RF_FRAME]->FifoElementList[j].nSharedResource;
		for (ULONG i = 0; i < pQuery->packets[j].nSharedResource; i++)
		{
			pQuery->packets[j].ResourceList[i].physAddr = devExt->PSharedFifo[RF_FRAME]->FifoElementList[j].pResourceList[i]->physAddr;
			pQuery->packets[j].ResourceList[i].pvUser = devExt->PSharedFifo[RF_FRAME]->FifoElementList[j].pResourceList[i]->pvUser;
			pQuery->packets[j].ResourceList[i].length = devExt->PSharedFifo[RF_FRAME]->FifoElementList[j].pResourceList[i]->length;
		}
	}
	pQuery++;
	pQuery->frame_type = RT_FRAME;
	pQuery->nFifoElement = devExt->Frame_FIFO_size;
	for (ULONG j = 0; j < devExt->Frame_FIFO_size; j++)
	{
		pQuery->packets[j].nSharedResource = devExt->PSharedFifo[RT_FRAME]->FifoElementList[j].nSharedResource;
		for (ULONG i = 0; i < pQuery->packets[j].nSharedResource; i++)
		{
			pQuery->packets[j].ResourceList[i].physAddr = devExt->PSharedFifo[RT_FRAME]->FifoElementList[j].pResourceList[i]->physAddr;
			pQuery->packets[j].ResourceList[i].pvUser = devExt->PSharedFifo[RT_FRAME]->FifoElementList[j].pResourceList[i]->pvUser;
			pQuery->packets[j].ResourceList[i].length = devExt->PSharedFifo[RT_FRAME]->FifoElementList[j].pResourceList[i]->length;
		}
	}

	pQuery++;
	pQuery->frame_type = RF_DIAG_RAW_FRAME;
	pQuery->nFifoElement = devExt->Diag_RawFrame_FIFO_size;
	for (ULONG j = 0; j < devExt->Diag_RawFrame_FIFO_size; j++)
	{
		pQuery->packets[j].nSharedResource = devExt->PSharedFifo[RF_DIAG_RAW_FRAME]->FifoElementList[j].nSharedResource;
		for (ULONG i = 0; i < pQuery->packets[j].nSharedResource; i++)
		{
			pQuery->packets[j].ResourceList[i].physAddr = devExt->PSharedFifo[RF_DIAG_RAW_FRAME]->FifoElementList[j].pResourceList[i]->physAddr;
			pQuery->packets[j].ResourceList[i].pvUser = devExt->PSharedFifo[RF_DIAG_RAW_FRAME]->FifoElementList[j].pResourceList[i]->pvUser;
			pQuery->packets[j].ResourceList[i].length = devExt->PSharedFifo[RF_DIAG_RAW_FRAME]->FifoElementList[j].pResourceList[i]->length;
		}
	}
	WdfRequestCompleteWithInformation(Request, STATUS_SUCCESS, BufferLength);
}

VOID
PDAQ4_IOCTL_WriteConfigREG(WDFQUEUE Queue, WDFREQUEST Request, WDF_REQUEST_PARAMETERS* param)
{
	UNREFERENCED_PARAMETER(param);
	NTSTATUS                status = STATUS_SUCCESS;
	PVOID                   DataBuffer;
	size_t                  BufferLength;
	PDEVICE_EXTENSION devExt = NULL;
	devExt = PDAQ4GetDeviceContext(WdfIoQueueGetDevice(Queue));

	status = WdfRequestRetrieveInputBuffer(Request, sizeof(WriteToRegister), &DataBuffer, &BufferLength);
	if (!NT_SUCCESS(status)) {
		KdPrintEx((DPFLTR_IHVDRIVER_ID, DPFLTR_ERROR_LEVEL, "PDAQ4WriteConfigREG WdfRequestRetrieveInputBuffer failed: !STATUS! = 0x%08X\n ", status));
		WdfRequestComplete(Request, status);
		return;
	}
	WriteToRegister* reg = (WriteToRegister*)DataBuffer;
	UINT32* RegsBase = (UINT32*)devExt->RegsBase;

	if (reg->offset * 4 > devExt->RegsLength)
	{
		ASSERTMSG(FALSE, "Invalid Register offset value, outside of range\n");
		WdfRequestComplete(Request, STATUS_INVALID_PARAMETER);
		return;
	}

	// Write the config Register
	KdPrintEx((DPFLTR_IHVDRIVER_ID, DPFLTR_INFO_LEVEL, "PDAQ4WriteConfigREG write address:%x value:%x\n ", RegsBase + reg->offset, reg->value));
	*(RegsBase + reg->offset) = reg->value;

	WdfRequestComplete(Request, status);
}

VOID
PDAQ4_IOCTL_WriteConfigREGBlock(WDFQUEUE Queue, WDFREQUEST Request, WDF_REQUEST_PARAMETERS* param)
{
	UNREFERENCED_PARAMETER(param);
	NTSTATUS                status = STATUS_SUCCESS;
	PVOID                   DataBuffer;
	size_t                  BufferLength;
	PDEVICE_EXTENSION devExt = NULL;
	//size_t HeaderLen = (sizeof(WriteToRegisterBlock)-sizeof(UINT32));
	devExt = PDAQ4GetDeviceContext(WdfIoQueueGetDevice(Queue));

	status = WdfRequestRetrieveInputBuffer(Request, sizeof(WriteToRegisterBlock), &DataBuffer, &BufferLength);
	if (!NT_SUCCESS(status)) {
		KdPrintEx((DPFLTR_IHVDRIVER_ID, DPFLTR_ERROR_LEVEL, "PDAQ4WriteConfigREG WdfRequestRetrieveInputBuffer failed: !STATUS! = 0x%08X\n ", status));
		WdfRequestComplete(Request, status);
		return;
	}

	WriteToRegisterBlock* regblock = (WriteToRegisterBlock*)DataBuffer;

	if (regblock->StartBARAddress + regblock->RegisterNum * sizeof(UINT32) > devExt->RegsLength)
	{
		KdPrintEx((DPFLTR_IHVDRIVER_ID, DPFLTR_ERROR_LEVEL, " The Block is invalid, the start address is %08X,the length is %08X, exceeds the BAR region!", regblock->StartBARAddress, BufferLength - 1));
		WdfRequestComplete(Request, STATUS_UNSUCCESSFUL);
		return;
	}

	PUCHAR datablock = (PUCHAR)regblock->RegisterBlock;
	RtlCopyMemory(devExt->RegsBase + regblock->StartBARAddress, datablock, regblock->RegisterNum * sizeof(UINT32));

	// Write the config Register
	KdPrintEx((DPFLTR_IHVDRIVER_ID, DPFLTR_INFO_LEVEL, "PDAQ BLOCK WRITE FINISHED "));

	WdfRequestComplete(Request, STATUS_SUCCESS);
}

VOID
PDAQ4_IOCTL_ReadConfigREGs(WDFQUEUE Queue, WDFREQUEST Request, WDF_REQUEST_PARAMETERS* param)
{
	NTSTATUS                status = STATUS_SUCCESS;
	PVOID                   DataBuffer;
	size_t                  BufferLength;
	PDEVICE_EXTENSION devExt = NULL;
	UINT32* REG_PTR;
	UNREFERENCED_PARAMETER(param);
	KdPrintEx((DPFLTR_IHVDRIVER_ID, DPFLTR_INFO_LEVEL, " PDAQ4ReadConfigREGs Entered\n"));

	devExt = PDAQ4GetDeviceContext(WdfIoQueueGetDevice(Queue));
	status = WdfRequestRetrieveOutputBuffer(Request, devExt->RegsLength, &DataBuffer, &BufferLength);
	if (!NT_SUCCESS(status)) {
		KdPrintEx((DPFLTR_IHVDRIVER_ID, DPFLTR_ERROR_LEVEL, " PDAQ4ReadConfigREGs WdfRequestRetrieveInputBuffer failed:  !STATUS! = 0x%08X\n", status));
		WdfRequestComplete(Request, status);
		return;
	}

	RtlMoveMemory(DataBuffer, devExt->RegsBase, devExt->RegsLength);
	REG_PTR = (UINT32*)DataBuffer;

	KdPrintEx((DPFLTR_IHVDRIVER_ID, DPFLTR_INFO_LEVEL, " PDAQ4ReadConfigREGs, The first 2 UINT32 are %x,%x\n", *REG_PTR, *(REG_PTR + 1)));

	WdfRequestCompleteWithInformation(Request, status, devExt->RegsLength);
}

VOID
PDAQ4_IOCTL_ReadConfigLength(WDFQUEUE Queue, WDFREQUEST Request, WDF_REQUEST_PARAMETERS* param)
{
	UNREFERENCED_PARAMETER(param);
	NTSTATUS                status = STATUS_SUCCESS;
	PVOID                   DataBuffer;
	size_t                  BufferLength;
	PDEVICE_EXTENSION devExt = NULL;
	devExt = PDAQ4GetDeviceContext(WdfIoQueueGetDevice(Queue));
	status = WdfRequestRetrieveOutputBuffer(Request, 4, &DataBuffer, &BufferLength);

	if (!NT_SUCCESS(status)) {
		KdPrintEx((DPFLTR_IHVDRIVER_ID, DPFLTR_ERROR_LEVEL, " PDAQ4ReadConfigLength WdfRequestRetrieveInputBuffer failed:  !STATUS! = 0x%08X\n", status));
		WdfRequestComplete(Request, status);
		return;
	}

	UINT32* config_length = (UINT32*)DataBuffer;
	*config_length = devExt->RegsLength;
	KdPrintEx((DPFLTR_IHVDRIVER_ID, DPFLTR_INFO_LEVEL, " PDAQ4ReadConfigLength Length=%x\n", *config_length));
	WdfRequestSetInformation(Request, sizeof(UINT32));
	WdfRequestComplete(Request, status);
}

// START DMA
void PDAQ4_IOCTL_StartACQ(WDFQUEUE Queue, WDFREQUEST Request, WDF_REQUEST_PARAMETERS* param)
{
	KdPrintEx((DPFLTR_IHVDRIVER_ID, DPFLTR_ERROR_LEVEL, "PDAQ4_IOCTL_StartACQ Enter\n"));
	TraceEvents(TRACE_LEVEL_INFORMATION, TRACE_DEVICE, "PDAQ4_IOCTL_StartACQ Enter");
	PDEVICE_EXTENSION devExt = NULL;
	NTSTATUS                status = STATUS_SUCCESS;
	devExt = PDAQ4GetDeviceContext(WdfIoQueueGetDevice(Queue));
	//PPDAQ4_REG pConfig = devExt->ConfigSpace;
	////START_DMA(pConfig);
	//RtlZeroMemory(&devExt->debug_info, sizeof(DrvDebugInfo));
	//FPGA_MOTOR_RUN(pConfig, 1);

	Framer_Config_Regs_PTR pRegRF = (Framer_Config_Regs_PTR)(devExt->FpgaFunRegBase + OFFSET_RF_FRAMER);
	Framer_Config_Regs_PTR pRegRT = (Framer_Config_Regs_PTR)(devExt->FpgaFunRegBase + OFFSET_RT_FRAMER);

	WRITE_REGISTER_ULONG((PULONG)(&pRegRF->Control), (ENABLE_FRAME_DDR_WRITE | ENABLE_FRAME_MSI_WRITE));
	WRITE_REGISTER_ULONG((PULONG)(&pRegRT->Control), (ENABLE_FRAME_DDR_WRITE | ENABLE_FRAME_MSI_WRITE));
	KdPrintEx((DPFLTR_IHVDRIVER_ID, DPFLTR_ERROR_LEVEL, "PDAQ4_IOCTL_StartACQ Finish\n"));
	/*WRITE_REGISTER_ULONG((PULONG)(&pRegRF->Control), (ENABLE_FRAME_DDR_WRITE | ENABLE_FRAME_MSI_WRITE));
	ULONG rfStatus = READ_REGISTER_ULONG((PULONG) & (pRegRF->Control));
	KdPrintEx((DPFLTR_IHVDRIVER_ID, DPFLTR_ERROR_LEVEL, "Enable RF\n"));
	KdPrintEx((DPFLTR_IHVDRIVER_ID, DPFLTR_ERROR_LEVEL, "RFStatus: %u\n", rfStatus));

	WRITE_REGISTER_ULONG((PULONG)(&pRegRT->Control), (ENABLE_FRAME_DDR_WRITE | ENABLE_FRAME_MSI_WRITE));
	ULONG rtStatus = READ_REGISTER_ULONG((PULONG) & (pRegRT->Control));
	KdPrintEx((DPFLTR_IHVDRIVER_ID, DPFLTR_ERROR_LEVEL, "Enable RT\n"));
	KdPrintEx((DPFLTR_IHVDRIVER_ID, DPFLTR_ERROR_LEVEL, "RTStatus: %u\n", rtStatus));*/

	WdfRequestComplete(Request, status);

	TraceEvents(TRACE_LEVEL_INFORMATION, TRACE_DEVICE, "PDAQ4_IOCTL_StartACQ End");
}

//STOP DMA

void PDAQ4_IOCTL_StopACQ(WDFQUEUE Queue, WDFREQUEST Request, WDF_REQUEST_PARAMETERS* param)
{
	TraceEvents(TRACE_LEVEL_INFORMATION, TRACE_DEVICE, "%!FUNC! Enter");
	KdPrintEx((DPFLTR_IHVDRIVER_ID, DPFLTR_ERROR_LEVEL, "PDAQ4_IOCTL_StopACQ Enter\n"));

	PDEVICE_EXTENSION devExt = NULL;
	NTSTATUS                status = STATUS_SUCCESS;
	devExt = PDAQ4GetDeviceContext(WdfIoQueueGetDevice(Queue));
	//PPDAQ4_REG pConfig = devExt->ConfigSpace;
	////START_DMA(pConfig);
	//RtlZeroMemory(&devExt->debug_info, sizeof(DrvDebugInfo));
	//FPGA_MOTOR_RUN(pConfig, 1);
	Framer_Config_Regs_PTR pRegRF = (Framer_Config_Regs_PTR)(devExt->FpgaFunRegBase + OFFSET_RF_FRAMER);
	Framer_Config_Regs_PTR pRegRT = (Framer_Config_Regs_PTR)(devExt->FpgaFunRegBase + OFFSET_RT_FRAMER);

	WRITE_REGISTER_ULONG((PULONG)(&pRegRF->Control), 0);
	WRITE_REGISTER_ULONG((PULONG)(&pRegRT->Control), 0);
	KdPrintEx((DPFLTR_IHVDRIVER_ID, DPFLTR_ERROR_LEVEL, "PDAQ4_IOCTL_StopACQ Finish\n"));

	/*PDEVICE_EXTENSION devExt = NULL;
	NTSTATUS                status = STATUS_SUCCESS;
	devExt = PDAQ4GetDeviceContext(WdfIoQueueGetDevice(Queue));
	PPDAQ4_REG pConfig = devExt->ConfigSpace;
	FPGA_MOTOR_RUN(pConfig, 0);*/
	TraceEvents(TRACE_LEVEL_INFORMATION, TRACE_DEVICE, "%!FUNC! End");

	WdfRequestComplete(Request, status);
}

////////////////////////////////////////////////////////////////////////////////////////
// Supporting routine for IOCTL_CONNECT_CHANNEL
////////////////////////////////////////////////////////////////////////////////////////
VOID PDAQ4_IOCTL_ConnectChannel(WDFQUEUE Queue, WDFREQUEST Request, WDF_REQUEST_PARAMETERS* param)
{
	TraceEvents(TRACE_LEVEL_INFORMATION, TRACE_DEVICE, "%!FUNC! Enter");

	PDEVICE_EXTENSION devExt = NULL;
	PVOID                   InDataBuffer, OutDataBuffer;
	size_t                  InBufferLength, OutBufferLength;
	NTSTATUS                status = STATUS_SUCCESS;
	devExt = PDAQ4GetDeviceContext(WdfIoQueueGetDevice(Queue));

	status = WdfRequestRetrieveInputBuffer(Request, sizeof(DrvConnectFifoInPacket), &InDataBuffer, &InBufferLength);
	if (!NT_SUCCESS(status)) {
		KdPrintEx((DPFLTR_IHVDRIVER_ID, DPFLTR_ERROR_LEVEL, " PDAQ4_IOCTL_ConnectChannel WdfRequestRetrieveInputBuffer failed:  !STATUS! = 0x%08X\n", status));
		WdfRequestComplete(Request, status);
		return;
	}

	TraceEvents(TRACE_LEVEL_INFORMATION, TRACE_DEVICE, "DrvConnectFifoOutPacket Size = %d\n", sizeof(DrvConnectFifoOutPacket));
	status = WdfRequestRetrieveOutputBuffer(Request, sizeof(DrvConnectFifoOutPacket), &OutDataBuffer, &OutBufferLength);
	if (!NT_SUCCESS(status)) {
		KdPrintEx((DPFLTR_IHVDRIVER_ID, DPFLTR_ERROR_LEVEL, " PDAQ4_IOCTL_ConnectChannel WdfRequestRetrieveOutputBuffer failed:  !STATUS! = 0x%08X\n", status));
		WdfRequestComplete(Request, status);
		return;
	}

	DrvConnectFifoInPacket* pIn = (DrvConnectFifoInPacket*)InDataBuffer;
	DrvConnectFifoOutPacket* pOut = (DrvConnectFifoOutPacket*)OutDataBuffer;

	ULONG index = pIn->frame_type;

	if (index <= RF_DIAG_RAW_FRAME)
	{
		if (devExt->PSharedFifo[index]->pEvent != NULL)
		{
			ObDereferenceObject(devExt->PSharedFifo[index]->pEvent);
			devExt->PSharedFifo[index]->pEvent = NULL;
		}
		status = ObReferenceObjectByHandle(pIn->hEvent,
			OBJECT_TYPE_ALL_ACCESS,
			*ExEventObjectType,
			KernelMode,
			(PVOID*)&devExt->PSharedFifo[index]->pEvent,
			NULL
		);
		NTSTATUS mapStat = MapSharedMemory((PDrvSharedResource)devExt->PSharedFifo[index]);

		TraceEvents(TRACE_LEVEL_INFORMATION, TRACE_DEVICE, "MapSharedMemory result = %!STATUS! End", mapStat);
		pOut->pFifo = (PDrvSharedFifo)devExt->PSharedFifo[index]->shareheader.pvUser;
	}
	else
	{
		status = STATUS_INVALID_PARAMETER;
	}
	WdfRequestCompleteWithInformation(Request, status, OutBufferLength);
}

////////////////////////////////////////////////////////////////////////////////////////
// Supporting routine for IOCTL_DISCONNECT_CHANNEL
////////////////////////////////////////////////////////////////////////////////////////
VOID PDAQ4_IOCTL_DisConnectChannel(WDFQUEUE Queue, WDFREQUEST Request, WDF_REQUEST_PARAMETERS* param)
{
	PDEVICE_EXTENSION devExt = NULL;
	NTSTATUS status = STATUS_SUCCESS;
	PVOID                   DataBuffer;
	size_t                  BufferLength;
	devExt = PDAQ4GetDeviceContext(WdfIoQueueGetDevice(Queue));

	status = WdfRequestRetrieveInputBuffer(Request, sizeof(UINT32), &DataBuffer, &BufferLength);

	PUINT32 pindex = (UINT32*)DataBuffer;
	if (*pindex <= RF_DIAG_RAW_FRAME)
	{
		if (devExt->PSharedFifo[*pindex]->pEvent != NULL)
		{
			ObDereferenceObject(devExt->PSharedFifo[*pindex]->pEvent);
			devExt->PSharedFifo[*pindex]->pEvent = NULL;
		}
	}
	else
	{
		status = STATUS_INVALID_PARAMETER;
	}
	WdfRequestComplete(Request, status);
}