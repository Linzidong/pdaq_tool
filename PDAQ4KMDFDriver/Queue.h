/*++

Module Name:

	queue.h

Abstract:

	This file contains the queue definitions.

Environment:

	Kernel-mode Driver Framework

--*/

#pragma once

#include "wdf.h"

//
// This is the context that can be placed per queue
// and would contain per queue information.
//
typedef struct _QUEUE_CONTEXT {
	ULONG PrivateDeviceData;  // just a placeholder
	WDFDMATRANSACTION       ReadDmaTransaction;
	WDFDMATRANSACTION       WriteDmaTransaction;
} QUEUE_CONTEXT, * PQUEUE_CONTEXT;

WDF_DECLARE_CONTEXT_TYPE_WITH_NAME(QUEUE_CONTEXT, QueueGetContext)

NTSTATUS
Pdaq4DriverQueueInitialize(
	_In_ WDFDEVICE Device
);

VOID
InvertedNotifyRFframe(
	_In_ PDEVICE_CONTEXT devContext
);

VOID
InvertedNotifyRTframe(
	_In_ PDEVICE_CONTEXT devContext
);

//
// Events from the IoQueue object
//
EVT_WDF_IO_QUEUE_IO_DEVICE_CONTROL      OnIoDeviceControl;
EVT_WDF_IO_QUEUE_IO_STOP                OnIoStop;
EVT_WDF_IO_QUEUE_IO_READ                OnIoRead;
EVT_WDF_IO_QUEUE_IO_WRITE               OnIoWrite;
EVT_WDF_IO_QUEUE_IO_CANCELED_ON_QUEUE   OnIoCanceledOnQueue;

EXTERN_C_END
