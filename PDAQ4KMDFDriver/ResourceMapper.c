
#include "Precomp.h"







////////////////////////////////////////////////////////////////////////////////////////
// Map a physically contiguous memory block to application
////////////////////////////////////////////////////////////////////////////////////////
NTSTATUS  MapSharedMemory(IN OUT DrvSharedResource *pSm)   
{
  NTSTATUS status = STATUS_INSUFFICIENT_RESOURCES;
  PVOID pv = NULL;
  MEMORY_CACHING_TYPE cType;
  PMDL Mdl;

  if (NULL == pSm->pvUser)
  {
    if (NULL == pSm->pMdl)
    {
      Mdl = IoAllocateMdl(pSm->pvKernel, pSm->length, FALSE, FALSE, NULL);     

      if ( (Mdl->MdlFlags & (MDL_PAGES_LOCKED			|
        MDL_SOURCE_IS_NONPAGED_POOL	|
        MDL_MAPPED_TO_SYSTEM_VA		|
        MDL_PARTIAL) ) == 0)
        MmBuildMdlForNonPagedPool(Mdl); 

      try 
      {
         MmProbeAndLockPages(Mdl, KernelMode, IoModifyAccess);
      }
      except (STATUS_ACCESS_VIOLATION)
      {
        IoFreeMdl(Mdl);
        return status;
      }
    }
    else
      Mdl = pSm->pMdl;

    cType = MmNonCached;
    try 
    {

      pv = (PVOID)(((ULONG)PAGE_ALIGN(MmMapLockedPagesSpecifyCache(Mdl, UserMode, cType, NULL, FALSE, HighPagePriority ))) + MmGetMdlByteOffset(Mdl));
    }
    except(EXCEPTION_EXECUTE_HANDLER)
    {
      status = GetExceptionCode();
      MmUnlockPages(Mdl);
      IoFreeMdl(Mdl);
      pSm->pMdl = NULL;
    }

    if (NULL != pv)
    {
      pSm->pvUser = pv;
      pSm->pMdl = Mdl;
      status = STATUS_SUCCESS;
    }
  }
  return status;
}


////////////////////////////////////////////////////////////////////////////////////////
// Unmap from application a physically contiguous memory
////////////////////////////////////////////////////////////////////////////////////////
NTSTATUS UnmapSharedMemory(IN OUT DrvSharedResource *pSm)  
{
  if ((NULL != pSm->pMdl) && (NULL != pSm->pvUser))
  {
    MmUnmapLockedPages(pSm->pvUser, pSm->pMdl); 
    pSm->pvUser = NULL;
    MmUnlockPages(pSm->pMdl);
    IoFreeMdl(pSm->pMdl);
    pSm->pMdl = NULL;
  }
  return STATUS_SUCCESS; 
}




