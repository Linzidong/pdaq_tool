#ifndef __REGPDAQ4__
#define __REGPDAQ4__

typedef volatile unsigned char    REG8, * PREG8;
typedef volatile unsigned short   REG16, * PREG16;
typedef volatile unsigned long	  REG32, * PREG32;
typedef volatile unsigned __int64 REG64, * PREG64;

#define OFFSET_REG_MOTRUN 1
typedef struct __Mot_Run
{
	REG32 Enable : 1;
	REG32 Rsvd : 31;
}
Mot_RUN;

typedef struct __RT_frm_cnt
{
	REG32 RT_frm_cnt : 6;
	REG32 Rsvd : 26;
}RT_frm_cnt;

typedef struct __RF_frm_cnt
{
	REG32 RF_frm_cnt : 6;
	REG32 Rsvd : 26;
}RF_frm_cnt;

typedef struct __SoftRest
{
	REG32 Soft_rst : 1;
	REG32 Rsvd : 29;
	REG32 Soft_rst_en : 1;
	REG32 Mem_Cal_Comp : 1;
}SoftReset;

typedef struct __IntEnable
{
	REG32 host_wrt_ena : 1; // write to host
	REG32 host_rd_ena : 1;  // read from host
	REG32 Rsvd : 30;
}IntEnable;

#define ENABLE_DMA_WRITE_INT(ppdaq4_reg)  ((PPDAQ4_REG)ppdaq4_reg)->rIntEnable.host_wrt_ena = 1
#define DISABLE_DMA_WRITE_INT(ppdaq4_reg) ((PPDAQ4_REG)ppdaq4_reg)->rIntEnable.host_wrt_ena = 0
#define ENABLE_DMA_READ_INT(ppdaq4_reg)  ((PPDAQ4_REG)ppdaq4_reg)->rIntEnable.host_rd_ena = 1
#define DISABLE_DMA_READ_INT(ppdaq4_reg) ((PPDAQ4_REG)ppdaq4_reg)->rIntEnable.host_rd_ena = 0

#define RESET_PDAQ(ppdaq4_reg) ((PPDAQ4_REG)ppdaq4_reg)->rSoftReset.Soft_rst= 0x0000001

typedef struct __IntStatus
{
	REG32 Write_Frame_End_int : 1;
	REG32 Read_Frame_End_int : 1;
	REG32 Rsvd : 30;
}IntStatus;

#define CLEAR_WRITE_FRAME_END_INT(ppdaq4_reg) ((PPDAQ4_REG)ppdaq4_reg)->rIntStatus.Write_Frame_End_int = 1
#define CLEAR_READ_FRAME_END_INT(ppdaq4_reg) ((PPDAQ4_REG)ppdaq4_reg)->rIntStatus.Read_Frame_End_int = 1

// Define the Register of PDAQ4
typedef struct __PDAQ4_REG
{
	REG32 rVER;
	Mot_RUN rMot_RUN;
	RT_frm_cnt rRT_frm_cnt;
	RF_frm_cnt rRF_frm_cnt;
	REG32	rWatchDog_En;
	SoftReset rSoftReset;
	IntEnable rIntEnable;
	IntStatus rIntStatus;
}PDAQ4_REG, * PPDAQ4_REG;

#define FPGA_MOTOR_RUN(pdaq4reg,run) ((PPDAQ4_REG)pdaq4reg)->rMot_RUN.Enable=run

typedef enum __ReadChannel
{
	RF_CHANNEL,
	RT_CHANNEL
}ReadChannel;

typedef struct __DMADescriptor
{
	REG32 msb;
	REG32 lsb;
	REG32 transfer_size;
	REG32 next_descriptor : 10;
	REG32 end_of_chain : 1;
	REG32 eot_interrupt : 1;
	REG32 read_channel : 4;
	REG32 reserved3 : 16;
}DMADescriptor, * PDAMDescripotr;

typedef struct __Dma_ctl1_reg
{
	REG32 Rsvd : 10;
	REG32 DMAEnabled : 1;
	REG32 Rsvd2 : 21;
}Dma_ctl1_reg, * PDma_ctl1_reg;

//#define OFFSET_TRANSFTER_CONTROL 0x000C0
#define OFFSET_TRANSFTER_CONTROL 0x0118
//#define OFFSET_DMA_WRITE_DESCRIPTOR	 0x01000
#define OFFSET_DMA_WRITE_DESCRIPTOR	 0x40012000
//#define OFFSET_DMA_READ_DESCRIPTOR	 0x08000
#define OFFSET_DMA_READ_DESCRIPTOR	 0x40010000
typedef struct __PDAQ4_TransferControl
{
	REG32 rDmawas_reg;
	REG32 rDmaras_l_reg;
	REG32 rDmaras_u_reg;
	REG32 rDmarad_reg;
	REG32 rDmarxs_reg;
	Dma_ctl1_reg rDma_ctl1_reg;
	REG32 dmacst_reg;
	REG32 rRd_cnt;
	REG32 rDma_wr_count;
	REG32 rDma_rd_count;
}PDAQ4_TransferControl, * PPDAQ4_TransferControl;

#define START_DMA(pdaq4reg) ((PDma_ctl1_reg)((PUCHAR)pdaq4reg+OFFSET_DMA_CTL1_REG))->DMAEnabled=1

#define START_READ_DMA(pdaq4reg) ((PDma_ctl1_reg)((PUCHAR)pdaq4reg+OFFSET_DMA_CTL2_REG))->DMAEnabled=1

#define GET_TRANSFER_CONTROL(pdaq4reg) ((PPDAQ4_TransferControl)((PUCHAR)pdaq4reg+OFFSET_TRANSFTER_CONTROL))

//typedef struct {
//	unsigned int	SIZE : 18;	// unit: DWORDs
//	unsigned int	ID : 7;
//	unsigned int	Reserved : 7;
//} DMA_Descriptor_Control, * DMA_Descriptor_Control_PTR;

//typedef struct {
//	ULONG	RD_LOW_SRC_ADDR;
//	ULONG	RD_HIGH_SRC_ADDR;
//	ULONG	RD_CTRL_LOW_DEST_ADDR;
//	ULONG	RD_CTRL_HIGH_DEST_ADDR;
//	DMA_Descriptor_Control	CONTROL;
//	ULONG	Reserved[3];
//} Read_Descriptor_Format, * Read_Descriptor_Format_PTR;
//
//typedef struct {
//	ULONG	WR_LOW_SRC_ADDR;
//	ULONG	WR_HIGH_SRC_ADDR;
//	ULONG	WR_CTRL_LOW_DEST_ADDR;
//	ULONG	WR_CTRL_HIGH_DEST_ADDR;
//	DMA_Descriptor_Control	CONTROL;
//	ULONG	Reserved[3];
//} Write_Descriptor_Format, * Write_Descriptor_Format_PTR;

#endif