/*++

Copyright (c) Microsoft Corporation.  All rights reserved.

THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR
PURPOSE.

Module Name:

Init.c

Abstract:

Contains most of initialization functions

Environment:

Kernel mode

--*/

#include "precomp.h"
#ifdef ALLOC_PRAGMA
#pragma alloc_text (PAGE, PDAQ4CreateDMAReadCommonBuffer)
#pragma alloc_text (PAGE, PDAQ4CreateDMAWriteCommonBuffer)
#endif

const ULONG FPGA_StartAddressOfRFframe[RF_FPGA_RING_BUFFER_ENTRIES] =
{
	0x1000000,
	0x1081060,
	0x11020C0,
	0x1183120,
	0x1204180,
	0x12851E0,
	0x1306240,
	0x13872A0,
	0x1408300,
	0x1489360,
	0x150A3C0,
	0x158B420,
	0x160C480,
	0x168D4E0,
	0x170E540,
	0x178F5A0
};

//const ULONG FPGA_StartAddressOfRTframe[RT_FPGA_RING_BUFFER_ENTRIES] =
//{
//	0x2000000,
//	0x2021060,
//	0x20420C0,
//	0x2063120,
//	0x2084180,
//	0x20A51E0,
//	0x20C6240,
//	0x20E72A0,
//	0x2108300,
//	0x2129360,
//	0x214A3C0,
//	0x216B420,
//	0x218C480,
//	0x21AD4E0,
//	0x21CE540,
//	0x21EF5A0,
//	0x2210600,
//	0x2231660,
//	0x22526C0,
//	0x2273720,
//	0x2294780,
//	0x22B57E0,
//	0x22D6840,
//	0x22F78A0,
//	0x2318900,
//	0x2339960,
//	0x235A9C0,
//	0x237BA20,
//	0x239CA80,
//	0x23BDAE0,
//	0x23DEB40,
//	0x23FFBA0
//};

const ULONG FPGA_StartAddressOfRTframe[RT_FPGA_RING_BUFFER_ENTRIES] =
{
	0x2000000,
	0x2011060,
	0x20220C0,
	0x2033120,
	0x2044180,
	0x20551E0,
	0x2066240,
	0x20772A0,
	0x2088300,
	0x2099360,
	0x20AA3C0,
	0x20BB420,
	0x20CC480,
	0x20DD4E0,
	0x20EE540,
	0x20FF5A0,
	0x2110600,
	0x2121660,
	0x21326C0,
	0x2143720,
	0x2154780,
	0x21657E0,
	0x2176840,
	0x21878A0,
	0x2198900,
	0x21A9960,
	0x21BA9C0,
	0x21CBA20,
	0x21DCA80,
	0x21EDAE0,
	0x21FEB40,
	0x220FBA0
};

NTSTATUS PDAQ4ProgramFPGA(PDEVICE_EXTENSION DevExt, PCWSTR pwsPath)
{
	NTSTATUS status = STATUS_SUCCESS;
	HANDLE hImage = OpenImageFile(pwsPath);
	ULONG dwLen = 0;
	PREG32  pjBin, pword = NULL;

	if (hImage != NULL)
	{
		dwLen = GetImageFileLength(hImage);
		pjBin = (PREG32)ExAllocatePoolWithTag(NonPagedPool, dwLen, 0xFFF1);
		pword = pjBin;
		status = ReadImageFile(hImage, (PVOID)pjBin, dwLen);
		CloseImageFile(hImage);

		KeStallExecutionProcessor(10);

		UINT32 wordIndex;
		UINT32 numWords = (dwLen / sizeof(REG32));

		for (wordIndex = 0; wordIndex < numWords; wordIndex++) {
			// Write to the base address; the entire region is mapped for reconfiguration data.
			WRITE_REGISTER_ULONG((PULONG)DevExt->RegsBase, *(pword++));
		}
		ExFreePool((PUCHAR)pjBin);

		KeStallExecutionProcessor(10);

		// Init DMA Descriptor
	}

	return status;
}

void  PDAQ4SetFIFOParameters(IN PDEVICE_EXTENSION DevExt, ULONG RFFrameSize, ULONG RTFrameSize)
{
	KdPrintEx((DPFLTR_IHVDRIVER_ID, DPFLTR_INFO_LEVEL, "PDAQ4SetFIFOParameters Entered\n"));
	// Set the current RFFrameSize and RTFrameSize,
	DevExt->RFFrameSize = RFFrameSize;
	DevExt->RTFrameSize = RTFrameSize;

	// Size of DMA descriptor hold, must be 128 byte align
	DevExt->RFAllocFrameSize = ROUND_TO_DESCRIPTOR_MIN(DevExt->RFFrameSize);
	DevExt->RTAllocFrameSize = ROUND_TO_DESCRIPTOR_MIN(DevExt->RTFrameSize);

	// How many descriptor for RF Frame and RT Frame
	DevExt->SharedResource_Num_Per_RF = 1;//ROUND_TO_DESCRIPTOR_MAX(DevExt->RFAllocFrameSize) / DevExt->SharedResourceSize;
	DevExt->SharedResource_Num_Per_RT = 1;//ROUND_TO_DESCRIPTOR_MAX(DevExt->RTAllocFrameSize) / DevExt->SharedResourceSize;

		// Auto decide the max frame fifo size
	DevExt->Frame_FIFO_size = MAX_NUM_OF_SG_DESCRIPTOR / (DevExt->SharedResource_Num_Per_RF + DevExt->SharedResource_Num_Per_RT);

	//
	KdPrintEx((DPFLTR_IHVDRIVER_ID, DPFLTR_INFO_LEVEL, "RT Frame Size is  %x\n", DevExt->RTFrameSize));
	KdPrintEx((DPFLTR_IHVDRIVER_ID, DPFLTR_INFO_LEVEL, "RF Frame Size is  %x\n", DevExt->RFFrameSize));
	KdPrintEx((DPFLTR_IHVDRIVER_ID, DPFLTR_INFO_LEVEL, "RT Frame Allocated Size is (128 bytes algin) %x\n", DevExt->RTAllocFrameSize));
	KdPrintEx((DPFLTR_IHVDRIVER_ID, DPFLTR_INFO_LEVEL, "RF Frame Allocated Size is (128 bytes algin) %x\n", DevExt->RFAllocFrameSize));

	KdPrintEx((DPFLTR_IHVDRIVER_ID, DPFLTR_INFO_LEVEL, "Desriptor Number per RT Frame = %x\n", DevExt->SharedResource_Num_Per_RT));
	KdPrintEx((DPFLTR_IHVDRIVER_ID, DPFLTR_INFO_LEVEL, "Desriptor Number per RF Frame = %x\n", DevExt->SharedResource_Num_Per_RF));

	KdPrintEx((DPFLTR_IHVDRIVER_ID, DPFLTR_INFO_LEVEL, "FIFO SIZE = %x\n", DevExt->Frame_FIFO_size));
}

void  PDAQ4SetDiagFIFOParameters(IN PDEVICE_EXTENSION DevExt, ULONG rawRFFrameSize)
{
	KdPrintEx((DPFLTR_IHVDRIVER_ID, DPFLTR_INFO_LEVEL, "PDAQ4SetDiagFIFOParameters Entered\n"));
	// Set the current RFFrameSize and RTFrameSize,
	DevExt->Diag_RawRFFrameSize = rawRFFrameSize;

	// Size of DMA descriptor hold, must be 128 byte align
	DevExt->Diag_RawRFAllocFrameSize = ROUND_TO_DESCRIPTOR_MIN(DevExt->Diag_RawRFFrameSize);

	// How many descriptor for RF RAW Frame
	DevExt->Diag_SharedResource_Num_Per_RAWRF = ROUND_TO_DESCRIPTOR_MAX(DevExt->Diag_RawRFAllocFrameSize) / DevExt->Diag_SharedResourceSize;

	// Auto decide the max frame fifo size
	DevExt->Diag_RawFrame_FIFO_size = MAX_NUM_OF_SG_DESCRIPTOR / (DevExt->Diag_SharedResource_Num_Per_RAWRF);

	// Print for debug
	KdPrintEx((DPFLTR_IHVDRIVER_ID, DPFLTR_INFO_LEVEL, "RAW RF Frame Size is  %x\n", DevExt->Diag_RawRFFrameSize));
	KdPrintEx((DPFLTR_IHVDRIVER_ID, DPFLTR_INFO_LEVEL, "RAW RF Frame Allocated Size is (128 bytes algin) %x\n", DevExt->Diag_RawRFAllocFrameSize));
	KdPrintEx((DPFLTR_IHVDRIVER_ID, DPFLTR_INFO_LEVEL, "Desriptor Number per RAW RF Frame = %x\n", DevExt->Diag_SharedResource_Num_Per_RAWRF));
	KdPrintEx((DPFLTR_IHVDRIVER_ID, DPFLTR_INFO_LEVEL, "RAW RF FIFO SIZE = %x\n", DevExt->Diag_RawFrame_FIFO_size));
}

// Allocate Common Buffer 256*1M for DMA to write
NTSTATUS PDAQ4CreateDMAWriteCommonBuffer(IN PDEVICE_EXTENSION DevExt)
{
	NTSTATUS    status;
	PAGED_CODE();

	KdPrintEx((DPFLTR_IHVDRIVER_ID, DPFLTR_ERROR_LEVEL, "PDAQ4CreateDMAWriteCommonBuffer Entered\n"));
	DevExt->Shared_Resource_Num = MAX_NUM_OF_SG_DESCRIPTOR;
	DevExt->SharedResourceSize = MAX_TRANSFER_SINGLE_DESCRIPTOR;
	DevExt->WriteMaximumTransferLength = MAX_TRANSFER_SINGLE_DESCRIPTOR;

	//CommonBuffer is used for DMA write, 1M alignment physical address
	WdfDeviceSetAlignmentRequirement(DevExt->Device, DMA_DESTINATION_ADDRESS_ALIGNMENT - 1);
	KdPrintEx((DPFLTR_IHVDRIVER_ID, DPFLTR_ERROR_LEVEL, "The MAX TRANSFER LENGTH OF DEVICE IS %d\n", MAX_TRANSFER_SINGLE_DESCRIPTOR));
	//
	// Create a new DMA Enabler instance.
	//
	{
		WDF_DMA_ENABLER_CONFIG   dmaConfig;
		size_t system_dma_max_length;
		WDF_DMA_ENABLER_CONFIG_INIT(&dmaConfig,
			WdfDmaProfileScatterGather,
			//WdfDmaProfilePacket,
			DevExt->WriteMaximumTransferLength);

		//KdPrintEx((DPFLTR_IHVDRIVER_ID, DPFLTR_INFO_LEVEL, " The DMA Profile is WdfDmaProfileScatterGather64\n"));

		status = WdfDmaEnablerCreate(DevExt->Device,
			&dmaConfig,
			WDF_NO_OBJECT_ATTRIBUTES,
			&DevExt->WriteDmaEnabler);

		system_dma_max_length = WdfDmaEnablerGetFragmentLength(DevExt->WriteDmaEnabler, WdfDmaDirectionReadFromDevice);
		KdPrintEx((DPFLTR_IHVDRIVER_ID, DPFLTR_ERROR_LEVEL, "The MAX DRIVER DMA TRANSFER LENGTH IS %d\n", system_dma_max_length));

		if (!NT_SUCCESS(status)) {
			KdPrintEx((DPFLTR_IHVDRIVER_ID, DPFLTR_ERROR_LEVEL, "WdfDmaEnablerCreate failed: STATUS %d\n", status));
			return status;
		}
		// how many scatter-gatter list the hardware support.
		//WdfDmaEnablerSetMaximumScatterGatherElements(DevExt->ReadDmaEnabler, MAX_NUM_OF_SG_DESCRIPTOR);
	}

	// Create Common Buffer for FIFO Control

	//ZIDONG
	DevExt->DescriptorTableCommonBufferSize = sizeof(ReadWrite_DMA_Status_and_Descriptor_Table);
	status = WdfCommonBufferCreate(DevExt->WriteDmaEnabler,
		DevExt->DescriptorTableCommonBufferSize,
		WDF_NO_OBJECT_ATTRIBUTES,
		&DevExt->DescriptorTableCommonBuffer);
	RtlZeroMemory(WdfCommonBufferGetAlignedVirtualAddress(DevExt->DescriptorTableCommonBuffer), WdfCommonBufferGetLength(DevExt->DescriptorTableCommonBuffer));
	DevExt->DescriptorTableCommonBufferBaseVa = WdfCommonBufferGetAlignedVirtualAddress(DevExt->DescriptorTableCommonBuffer);
	DevExt->DescriptorTableCommonBufferBasePA = WdfCommonBufferGetAlignedLogicalAddress(DevExt->DescriptorTableCommonBuffer);
	DevExt->WriteDescriptorTableCommonBufferBasePA.QuadPart = DevExt->DescriptorTableCommonBufferBasePA.QuadPart + sizeof(Read_DMA_Status_and_Descriptor_Table);

	//获取Write Controller
	PWrite_DMA_Descriptor_Controller_Registers   pWriteCtr;
	pWriteCtr = (PWrite_DMA_Descriptor_Controller_Registers)DevExt->FpgaDmaDescClrRegWrite;

	//把 host的 Write Descriptor Table 起始的物理地址写给 Write Contoller
	WRITE_REGISTER_ULONG((PULONG)(&pWriteCtr->RC_Write_Status_and_Descriptor_Base_High),
		DevExt->WriteDescriptorTableCommonBufferBasePA.HighPart);
	WRITE_REGISTER_ULONG((PULONG)(&pWriteCtr->RC_Write_Status_and_Descriptor_Base_Low),
		DevExt->WriteDescriptorTableCommonBufferBasePA.LowPart);

	if (0xFF == READ_REGISTER_ULONG((PULONG)&pWriteCtr->WR_DMA_LAST_PTR))
	{
		//把 FPGA的 Write Descriptor Table 起始的物理地址写给 Write Contoller
		WRITE_REGISTER_ULONG((PULONG)(&pWriteCtr->EP_Write_Descriptor_FIFO_Base_High), 0);
		WRITE_REGISTER_ULONG((PULONG)(&pWriteCtr->EP_Write_Descriptor_FIFO_Base_Low), 0x40010000);
	}

	//ZIDONG

	for (ULONG i = 0; i < 2; i++)
	{
		WDFCOMMONBUFFER commonbuffer;
		status = WdfCommonBufferCreate(DevExt->WriteDmaEnabler, sizeof(DrvSharedFifo), WDF_NO_OBJECT_ATTRIBUTES, &commonbuffer);
		if (!NT_SUCCESS(status)) {
			KdPrintEx((DPFLTR_IHVDRIVER_ID, DPFLTR_ERROR_LEVEL, "WdfCommonBufferCreate (RF) failed %d\n", status));
			return status;
		}
		RtlZeroMemory(WdfCommonBufferGetAlignedVirtualAddress(commonbuffer), WdfCommonBufferGetLength(commonbuffer));
		//Store in the SharedResourceList
		DevExt->PSharedFifo[i] = (PDrvSharedFifo)WdfCommonBufferGetAlignedVirtualAddress(commonbuffer);
		DevExt->PSharedFifo[i]->shareheader.commonbuffer = commonbuffer;
		DevExt->PSharedFifo[i]->shareheader.pvKernel = WdfCommonBufferGetAlignedVirtualAddress(commonbuffer);
		DevExt->PSharedFifo[i]->shareheader.physAddr = WdfCommonBufferGetAlignedLogicalAddress(commonbuffer);
		DevExt->PSharedFifo[i]->shareheader.length = sizeof(DrvSharedFifo);
		// Initialize the FIFO content
		//RtlZeroMemory(DevExt->PSharedFifo[i]->shareheader.pvKernel, DevExt->PSharedFifo[i]->shareheader.length);
	}

	KdPrintEx((DPFLTR_IHVDRIVER_ID, DPFLTR_ERROR_LEVEL, "Create 256 1M CommonBuffer for DMA Write, the physical and virtual address are  like below\n"));
	for (ULONG i = 0; i < DevExt->Shared_Resource_Num; i++)
	{
		_Analysis_assume_(DevExt->SharedResourceSize > 0);
		status = WdfCommonBufferCreate(DevExt->WriteDmaEnabler, DevExt->SharedResourceSize, WDF_NO_OBJECT_ATTRIBUTES, &DevExt->SharedResourceList[i].commonbuffer);

		if (!NT_SUCCESS(status)) {
			KdPrintEx((DPFLTR_IHVDRIVER_ID, DPFLTR_ERROR_LEVEL, "WdfCommonBufferCreate DMA Write failed %d\n", status));
			return status;
		}
		//Store in the SharedResourceList
		DevExt->SharedResourceList[i].pvKernel = WdfCommonBufferGetAlignedVirtualAddress(DevExt->SharedResourceList[i].commonbuffer);
		DevExt->SharedResourceList[i].physAddr = WdfCommonBufferGetAlignedLogicalAddress(DevExt->SharedResourceList[i].commonbuffer);
		DevExt->SharedResourceList[i].length = DevExt->SharedResourceSize;
		// Initialize the memory to zero
		RtlZeroMemory(DevExt->SharedResourceList[i].pvKernel, DevExt->SharedResourceSize);

		//KdPrintEx((DPFLTR_IHVDRIVER_ID, DPFLTR_ERROR_LEVEL, "CommonBuffer index=%d 0x%p  (#0x%I64X), length %X\n", i, DevExt->SharedResourceList[i].pvKernel, DevExt->SharedResourceList[i].physAddr.QuadPart, WdfCommonBufferGetLength(DevExt->SharedResourceList[i].commonbuffer)));
	}
	return status;
}

// Allocate Common Buffer 256 * 1M for read from host via DMA for diagnostic purpose
NTSTATUS PDAQ4CreateDMAReadCommonBuffer(IN PDEVICE_EXTENSION DevExt)
{
	NTSTATUS    status;
	PAGED_CODE();

	KdPrintEx((DPFLTR_IHVDRIVER_ID, DPFLTR_ERROR_LEVEL, "PDAQ4CreateDMAReadCommonBuffer Entered\n"));
	DevExt->Diag_Shared_Resource_Num = MAX_NUM_OF_SG_DESCRIPTOR;
	DevExt->Diag_SharedResourceSize = MAX_TRANSFER_SINGLE_DESCRIPTOR;
	DevExt->ReadMaximumTransferLength = MAX_TRANSFER_SINGLE_DESCRIPTOR;

	//CommonBuffer is used for DMA write, 1M alignment physical address
	WdfDeviceSetAlignmentRequirement(DevExt->Device, DMA_DESTINATION_ADDRESS_ALIGNMENT - 1);
	KdPrintEx((DPFLTR_IHVDRIVER_ID, DPFLTR_ERROR_LEVEL, "The MAX TRANSFER LENGTH OF DEVICE IS %d\n", MAX_TRANSFER_SINGLE_DESCRIPTOR));
	//
	// Create a new DMA Enabler instance.
	//
	{
		WDF_DMA_ENABLER_CONFIG   dmaConfig;
		size_t system_dma_max_length;
		WDF_DMA_ENABLER_CONFIG_INIT(&dmaConfig,
			WdfDmaProfileScatterGather,
			//WdfDmaProfilePacket,
			DevExt->ReadMaximumTransferLength);

		//KdPrintEx((DPFLTR_IHVDRIVER_ID, DPFLTR_INFO_LEVEL, " The DMA Profile is WdfDmaProfileScatterGather64\n"));

		status = WdfDmaEnablerCreate(DevExt->Device,
			&dmaConfig,
			WDF_NO_OBJECT_ATTRIBUTES,
			&DevExt->ReadDmaEnabler);

		system_dma_max_length = WdfDmaEnablerGetFragmentLength(DevExt->ReadDmaEnabler, WdfDmaDirectionWriteToDevice);
		KdPrintEx((DPFLTR_IHVDRIVER_ID, DPFLTR_ERROR_LEVEL, "The MAX DRIVER DMA TRANSFER LENGTH IS %d\n", system_dma_max_length));

		if (!NT_SUCCESS(status)) {
			KdPrintEx((DPFLTR_IHVDRIVER_ID, DPFLTR_ERROR_LEVEL, "WdfDmaEnablerCreate failed: STATUS %d\n", status));
			return status;
		}
		// how many scatter-gatter list the hardware support.
		//WdfDmaEnablerSetMaximumScatterGatherElements(DevExt->ReadDmaEnabler, MAX_NUM_OF_SG_DESCRIPTOR);
	}

	// Create Common Buffer for FIFO Control

	WDFCOMMONBUFFER commonbuffer;
	status = WdfCommonBufferCreate(DevExt->ReadDmaEnabler, sizeof(DrvSharedFifo), WDF_NO_OBJECT_ATTRIBUTES, &commonbuffer);
	if (!NT_SUCCESS(status)) {
		KdPrintEx((DPFLTR_IHVDRIVER_ID, DPFLTR_INFO_LEVEL, "WdfCommonBufferCreate DMA Read failed %d\n", status));
		return status;
	}
	RtlZeroMemory(WdfCommonBufferGetAlignedVirtualAddress(commonbuffer), WdfCommonBufferGetLength(commonbuffer));
	//Store in the SharedResourceList
	DevExt->PSharedFifo[RF_DIAG_RAW_FRAME] = (PDrvSharedFifo)WdfCommonBufferGetAlignedVirtualAddress(commonbuffer);
	DevExt->PSharedFifo[RF_DIAG_RAW_FRAME]->shareheader.commonbuffer = commonbuffer;
	DevExt->PSharedFifo[RF_DIAG_RAW_FRAME]->shareheader.pvKernel = WdfCommonBufferGetAlignedVirtualAddress(commonbuffer);
	DevExt->PSharedFifo[RF_DIAG_RAW_FRAME]->shareheader.physAddr = WdfCommonBufferGetAlignedLogicalAddress(commonbuffer);
	DevExt->PSharedFifo[RF_DIAG_RAW_FRAME]->shareheader.length = sizeof(DrvSharedFifo);
	// Initialize the FIFO content
	//RtlZeroMemory(DevExt->PSharedFifo[i]->shareheader.pvKernel, DevExt->PSharedFifo[i]->shareheader.length);

	KdPrintEx((DPFLTR_IHVDRIVER_ID, DPFLTR_ERROR_LEVEL, "Create 256 1M CommonBuffer for DMA Read, the physical and virtual address are like below\n"));
	for (ULONG i = 0; i < DevExt->Diag_Shared_Resource_Num; i++)
	{
		_Analysis_assume_(DevExt->Diag_SharedResourceSize > 0);
		status = WdfCommonBufferCreate(DevExt->ReadDmaEnabler, DevExt->Diag_SharedResourceSize, WDF_NO_OBJECT_ATTRIBUTES, &DevExt->Diag_SharedResourceList[i].commonbuffer);

		if (!NT_SUCCESS(status)) {
			KdPrintEx((DPFLTR_IHVDRIVER_ID, DPFLTR_INFO_LEVEL, "WdfCommonBufferCreate DMA Read failed %d\n", status));
			return status;
		}
		//Store in the SharedResourceList
		DevExt->Diag_SharedResourceList[i].pvKernel = WdfCommonBufferGetAlignedVirtualAddress(DevExt->Diag_SharedResourceList[i].commonbuffer);
		DevExt->Diag_SharedResourceList[i].physAddr = WdfCommonBufferGetAlignedLogicalAddress(DevExt->Diag_SharedResourceList[i].commonbuffer);
		DevExt->Diag_SharedResourceList[i].length = DevExt->Diag_SharedResourceSize;
		// Initialize the memory to zero
		RtlZeroMemory(DevExt->Diag_SharedResourceList[i].pvKernel, DevExt->Diag_SharedResourceSize);

		KdPrintEx((DPFLTR_IHVDRIVER_ID, DPFLTR_ERROR_LEVEL, "CommonBuffer index=%d 0x%p  (#0x%I64X), length %X\n", i, DevExt->Diag_SharedResourceList[i].pvKernel, DevExt->Diag_SharedResourceList[i].physAddr.QuadPart, WdfCommonBufferGetLength(DevExt->Diag_SharedResourceList[i].commonbuffer)));
	}
	return status;
}

// This functioin used to estalbish the relation between FIFO element and descriptors
NTSTATUS PDAQ4InitFIFO(IN PDEVICE_EXTENSION DevExt)
{
	KdPrintEx((DPFLTR_IHVDRIVER_ID, DPFLTR_INFO_LEVEL, "PDAQ4InitFIFO Entered\n"));

	FIFO_CONFIG rf_fifo_config;
	FIFO_CONFIG rt_fifo_config;

	rf_fifo_config.nItems = DevExt->Frame_FIFO_size;
	rf_fifo_config.nAllocSize = DevExt->RFAllocFrameSize;
	rf_fifo_config.nItemSize = DevExt->RFFrameSize;
	rf_fifo_config.nRead = rf_fifo_config.nWrite = 0;

	rt_fifo_config.nItems = DevExt->Frame_FIFO_size;
	rt_fifo_config.nAllocSize = DevExt->RTAllocFrameSize;
	rt_fifo_config.nItemSize = DevExt->RTFrameSize;
	rt_fifo_config.nRead = rt_fifo_config.nWrite = 0;

	DrvSharedResource* pSharedResource = DevExt->SharedResourceList;

	//For every fifo element
	for (ULONG i = 0; i < DevExt->Frame_FIFO_size; i++)
	{
		ULONG rf_frame_remainding = DevExt->RFAllocFrameSize;
		ULONG rt_frame_remainding = DevExt->RTAllocFrameSize;
		DevExt->PSharedFifo[RF_FRAME]->FifoElementList[i].nSharedResource = DevExt->SharedResource_Num_Per_RF;

		for (ULONG j = 0; j < DevExt->SharedResource_Num_Per_RF; j++)
		{
			DevExt->PSharedFifo[RF_FRAME]->FifoElementList[i].pResourceList[j] = pSharedResource;
			if (j == DevExt->SharedResource_Num_Per_RF - 1)
			{
				pSharedResource->length = rf_frame_remainding;
				rf_frame_remainding = 0;
			}
			else
			{
				rf_frame_remainding -= MAX_TRANSFER_SINGLE_DESCRIPTOR;
			}
			pSharedResource++;
		}

		DevExt->PSharedFifo[RT_FRAME]->FifoElementList[i].nSharedResource = DevExt->SharedResource_Num_Per_RT;

		for (ULONG j = 0; j < DevExt->SharedResource_Num_Per_RT; j++)
		{
			DevExt->PSharedFifo[RT_FRAME]->FifoElementList[i].pResourceList[j] = pSharedResource;
			if (j == DevExt->SharedResource_Num_Per_RT - 1)
			{
				pSharedResource->length = rt_frame_remainding;
				rt_frame_remainding = 0;
			}
			else
			{
				rt_frame_remainding -= MAX_TRANSFER_SINGLE_DESCRIPTOR;
			}
			pSharedResource++;
		}

		DevExt->DMA_WriteItems[i].rf_fifo_element = &(DevExt->PSharedFifo[RF_FRAME]->FifoElementList[i]);
		DevExt->DMA_WriteItems[i].rt_fifo_element = &(DevExt->PSharedFifo[RT_FRAME]->FifoElementList[i]);

		ExInterlockedInsertTailList(&DevExt->DmaWriteList.queue_list, (PLIST_ENTRY)&DevExt->DMA_WriteItems[i], &DevExt->DmaWriteList.queueLock);
	}

	FIFOInit(DevExt->PSharedFifo[RF_FRAME], &rf_fifo_config);
	FIFOInit(DevExt->PSharedFifo[RT_FRAME], &rt_fifo_config);

	return STATUS_SUCCESS;
}

// This functioin used to estalbish the relation between FIFO element and descriptors
NTSTATUS PDAQ4InitDiagFIFO(IN PDEVICE_EXTENSION DevExt)
{
	KdPrintEx((DPFLTR_IHVDRIVER_ID, DPFLTR_INFO_LEVEL, "PDAQ4InitDiagFIFO Entered\n"));

	FIFO_CONFIG rawrf_fifo_config;

	rawrf_fifo_config.nItems = DevExt->Diag_RawFrame_FIFO_size;
	rawrf_fifo_config.nAllocSize = DevExt->Diag_RawRFAllocFrameSize;
	rawrf_fifo_config.nItemSize = DevExt->Diag_RawRFFrameSize;
	// When Starts, there will be allready 256 raw frames in the queue.
	rawrf_fifo_config.nRead = 0;
	rawrf_fifo_config.nWrite = rawrf_fifo_config.nItems;
	DrvSharedResource* pSharedResource = DevExt->Diag_SharedResourceList;

	//For every fifo element
	for (ULONG i = 0; i < DevExt->Diag_RawFrame_FIFO_size; i++)
	{
		ULONG rf_frame_remainding = DevExt->Diag_RawRFAllocFrameSize;
		DevExt->PSharedFifo[RF_DIAG_RAW_FRAME]->FifoElementList[i].nSharedResource = DevExt->Diag_SharedResource_Num_Per_RAWRF;

		for (ULONG j = 0; j < DevExt->Diag_SharedResource_Num_Per_RAWRF; j++)
		{
			DevExt->PSharedFifo[RF_DIAG_RAW_FRAME]->FifoElementList[i].pResourceList[j] = pSharedResource;
			if (j == DevExt->Diag_SharedResource_Num_Per_RAWRF - 1)
			{
				pSharedResource->length = rf_frame_remainding;
				rf_frame_remainding = 0;
			}
			else
			{
				rf_frame_remainding -= MAX_TRANSFER_SINGLE_DESCRIPTOR;
			}
			pSharedResource++;
		}

		DevExt->DMA_ReadItems[i].rf_fifo_element = &(DevExt->PSharedFifo[RF_DIAG_RAW_FRAME]->FifoElementList[i]);

		ExInterlockedInsertTailList(&DevExt->DmaReadList.queue_list, (PLIST_ENTRY)&DevExt->DMA_ReadItems[i], &DevExt->DmaReadList.queueLock);
	}

	FIFOInit(DevExt->PSharedFifo[RF_DIAG_RAW_FRAME], &rawrf_fifo_config);

	return STATUS_SUCCESS;
}

// Initialize the descriptor table
NTSTATUS PDAQ4InitWriteDMADescriptorTable(IN PDEVICE_EXTENSION DevExt)
{
	KdPrintEx((DPFLTR_IHVDRIVER_ID, DPFLTR_INFO_LEVEL, "PDAQ4InitWriteDMADescriptorTable Entered\n"));

	//ZIDONG
	ReadWrite_DMA_Status_and_Descriptor_Table_PTR   pDesc;
	PWrite_DMA_Descriptor_Controller_Registers   pwReq;

	pDesc = (ReadWrite_DMA_Status_and_Descriptor_Table_PTR)(DevExt->DescriptorTableCommonBufferBaseVa);

	ULONG dma_descriptor_index = 0;

	ULONG rtIndex = 0;

	ULONG rfIndex = 0;

	for (ULONG i = 0; i < DevExt->Frame_FIFO_size; i++)
	{
		ULONG rt_resource_num = DevExt->PSharedFifo[RT_FRAME]->FifoElementList[i].nSharedResource;
		ULONG rt_remainding = DevExt->RTAllocFrameSize;
		ULONG rf_resource_num = DevExt->PSharedFifo[RF_FRAME]->FifoElementList[i].nSharedResource;
		ULONG rf_remainding = DevExt->RFAllocFrameSize;

		for (ULONG j = 0; j < rt_resource_num; j++)
		{
			if (dma_descriptor_index > MAX_DMA_DESCRIPTOR_ENTRIES - 1) {
				break;
			}

			pDesc->Write.Descriptor[dma_descriptor_index].WR_HIGH_SRC_ADDR = 0;
			pDesc->Write.Descriptor[dma_descriptor_index].WR_LOW_SRC_ADDR = FPGA_StartAddressOfRTframe[rtIndex];
			pDesc->Write.Descriptor[dma_descriptor_index].WR_CTRL_LOW_DEST_ADDR = DevExt->PSharedFifo[RT_FRAME]->FifoElementList[i].pResourceList[j]->physAddr.LowPart;
			pDesc->Write.Descriptor[dma_descriptor_index].WR_CTRL_HIGH_DEST_ADDR = DevExt->PSharedFifo[RT_FRAME]->FifoElementList[i].pResourceList[j]->physAddr.HighPart;

			if (rt_remainding <= DevExt->SharedResourceSize)
			{
				pDesc->Write.Descriptor[dma_descriptor_index].CONTROL.SIZE = rt_remainding / 4;

				rt_remainding = 0;
			}

			else
			{
				pDesc->Write.Descriptor[dma_descriptor_index].CONTROL.SIZE = DevExt->SharedResourceSize / 4;
				rt_remainding -= MAX_TRANSFER_SINGLE_DESCRIPTOR;
			}

			pDesc->Write.Descriptor[dma_descriptor_index].CONTROL.ID = dma_descriptor_index;

			KdPrintEx((DPFLTR_IHVDRIVER_ID, DPFLTR_INFO_LEVEL, "SG Element %d, Address HighPart%8X,LowPart %8X,Length=%X\n", 0, pDesc->Write.Descriptor[dma_descriptor_index].WR_CTRL_HIGH_DEST_ADDR, pDesc->Write.Descriptor[dma_descriptor_index].WR_CTRL_LOW_DEST_ADDR, pDesc->Write.Descriptor[dma_descriptor_index].CONTROL.SIZE));

			if (rtIndex == RT_FPGA_RING_BUFFER_ENTRIES - 1) {
				rtIndex = 0;
			}
			else
			{
				rtIndex++;
			}
			dma_descriptor_index++;
		}

		for (ULONG j = 0; j < rf_resource_num; j++)
		{
			if (dma_descriptor_index > MAX_DMA_DESCRIPTOR_ENTRIES - 1) {
				break;
			}

			pDesc->Write.Descriptor[dma_descriptor_index].WR_HIGH_SRC_ADDR = 0;
			pDesc->Write.Descriptor[dma_descriptor_index].WR_LOW_SRC_ADDR = FPGA_StartAddressOfRFframe[rfIndex];
			pDesc->Write.Descriptor[dma_descriptor_index].WR_CTRL_LOW_DEST_ADDR = DevExt->PSharedFifo[RF_FRAME]->FifoElementList[i].pResourceList[j]->physAddr.LowPart;
			pDesc->Write.Descriptor[dma_descriptor_index].WR_CTRL_HIGH_DEST_ADDR = DevExt->PSharedFifo[RF_FRAME]->FifoElementList[i].pResourceList[j]->physAddr.HighPart;

			if (rf_remainding <= DevExt->SharedResourceSize)
			{
				pDesc->Write.Descriptor[dma_descriptor_index].CONTROL.SIZE = rf_remainding / 4;
				rf_remainding = 0;
			}

			else
			{
				pDesc->Write.Descriptor[dma_descriptor_index].CONTROL.SIZE = DevExt->SharedResourceSize / 4;
				rf_remainding -= DevExt->SharedResourceSize;
			}

			pDesc->Write.Descriptor[dma_descriptor_index].CONTROL.ID = dma_descriptor_index;

			if (rfIndex == RF_FPGA_RING_BUFFER_ENTRIES - 1) {
				rfIndex = 0;
			}
			else
			{
				rfIndex++;
			}
			dma_descriptor_index++;
		}
	}

	return STATUS_SUCCESS;
}

// Initialize the descriptor table
NTSTATUS PDAQ4InitReadDMADescriptorTable(IN PDEVICE_EXTENSION DevExt)
{
	KdPrintEx((DPFLTR_IHVDRIVER_ID, DPFLTR_INFO_LEVEL, "PDAQ4InitReadDMADescriptorTable Entered\n"));
	PDAMDescripotr			 dmadescriptor;
	dmadescriptor = (PDAMDescripotr)DevExt->RegReadDMADescriptorBase;
	ULONG dma_descriptor_index = 1;
	for (ULONG i = 0; i < DevExt->Diag_RawFrame_FIFO_size; i++)
	{
		ULONG rf_resource_num = DevExt->PSharedFifo[RF_DIAG_RAW_FRAME]->FifoElementList[i].nSharedResource;
		ULONG rf_remainding = DevExt->Diag_RawRFAllocFrameSize;
		for (ULONG j = 0; j < rf_resource_num; j++)
		{
			dmadescriptor->lsb = DevExt->PSharedFifo[RF_DIAG_RAW_FRAME]->FifoElementList[i].pResourceList[j]->physAddr.LowPart;
			dmadescriptor->msb = DevExt->PSharedFifo[RF_DIAG_RAW_FRAME]->FifoElementList[i].pResourceList[j]->physAddr.HighPart;

			if (rf_remainding <= DevExt->Diag_SharedResourceSize)
			{
				dmadescriptor->transfer_size = rf_remainding;
				rf_remainding = 0;
			}

			else
			{
				dmadescriptor->transfer_size = DevExt->Diag_SharedResourceSize;
				rf_remainding -= MAX_TRANSFER_SINGLE_DESCRIPTOR;
			}
			dmadescriptor->eot_interrupt = FALSE;
			dmadescriptor->end_of_chain = FALSE;
			dmadescriptor->next_descriptor = dma_descriptor_index;
			if (dma_descriptor_index == MAX_NUM_OF_SG_DESCRIPTOR)
			{
				dmadescriptor->next_descriptor = 0;
			}
			if (j == rf_resource_num - 1)
			{
				dmadescriptor->eot_interrupt = TRUE;
			}

			KdPrintEx((DPFLTR_IHVDRIVER_ID, DPFLTR_INFO_LEVEL, "SG Element %d, Address HighPart%8X,LowPart %8X,Length=%X\n", i, dmadescriptor->msb, dmadescriptor->lsb, dmadescriptor->transfer_size));

			dmadescriptor++;
			dma_descriptor_index++;
		}
	}

	return STATUS_SUCCESS;
}

NTSTATUS PDAQ4InitWriteDMA(IN PDEVICE_EXTENSION DevExt, ULONG n_rfframesize, ULONG n_rtframesize)
{
	NTSTATUS    status = STATUS_SUCCESS;
	//DISABLE_DMA_WRITE_INT(DevExt->ConfigSpace);

	//Set_FIFO_Parameters(DevExt, FRAME_SIZE(VECTOR_NUM_PER_FRAME_256, SAMPLE_PER_VECTOR_1024, RF_SAMPLE_BYTES), FRAME_SIZE(VECTOR_NUM_PER_FRAME_256, SAMPLE_PER_VECTOR_256, RT_SAMPLE_BYTES));

	if (n_rfframesize > 0 && n_rtframesize > 0)
	{
		KdPrintEx((DPFLTR_IHVDRIVER_ID, DPFLTR_ERROR_LEVEL, "Run Method: PDAQ4SetFIFOParameters\n"));

		PDAQ4SetFIFOParameters(DevExt, n_rfframesize, n_rtframesize);

		// Check that the memory is enough , MUST PASS not posssilbe to FAIL
		if (DevExt->Frame_FIFO_size * (DevExt->RFAllocFrameSize + DevExt->RTAllocFrameSize) <= DevExt->Shared_Resource_Num * DevExt->SharedResourceSize)
		{
			// Initialize the DMA LIST
			// This LIST work like producer and consumer,
			InitializeListHead(&DevExt->DmaWriteList.queue_list);
			InitializeListHead(&DevExt->DmaWriteList.done_list);
			KeInitializeSpinLock(&DevExt->DmaWriteList.queueLock);
			KeInitializeSpinLock(&DevExt->DmaWriteList.doneLock);
			//Init FIFO, one element of FIFO may hold several descriptor (1M)

			KdPrintEx((DPFLTR_IHVDRIVER_ID, DPFLTR_ERROR_LEVEL, "Run Method: PDAQ4InitFIFO\n"));

			PDAQ4InitFIFO(DevExt);

			//Init the DAM Table according to FIFO
			KdPrintEx((DPFLTR_IHVDRIVER_ID, DPFLTR_ERROR_LEVEL, "Run Method: PDAQ4InitWriteDMADescriptorTable\n"));

			PDAQ4InitWriteDMADescriptorTable(DevExt);
		}
		else
		{
			status = STATUS_INSUFFICIENT_RESOURCES;
		}
	}
	else
	{
		status = STATUS_DATA_ERROR;
	}

	//ENABLE_DMA_WRITE_INT(DevExt->ConfigSpace);
	return status;
}

NTSTATUS PDAQ4InitReadDMA(IN PDEVICE_EXTENSION DevExt, ULONG n_rfrawframesize)
{
	// n_rtrawframesize is not used, only for future extension
	NTSTATUS    status = STATUS_SUCCESS;
	//DISABLE_DMA_READ_INT(DevExt->ConfigSpace);

	//Set_FIFO_Parameters(DevExt, FRAME_SIZE(VECTOR_NUM_PER_FRAME_256, SAMPLE_PER_VECTOR_1024, RF_SAMPLE_BYTES), FRAME_SIZE(VECTOR_NUM_PER_FRAME_256, SAMPLE_PER_VECTOR_256, RT_SAMPLE_BYTES));

	if (n_rfrawframesize > 0)
	{
		PDAQ4SetDiagFIFOParameters(DevExt, n_rfrawframesize);

		// Check that the memory is enough , MUST PASS not posssilbe to FAIL
		if (DevExt->Diag_RawFrame_FIFO_size * (DevExt->Diag_RawRFAllocFrameSize) <= DevExt->Diag_Shared_Resource_Num * DevExt->SharedResourceSize)
		{
			// Initialize the DMA LIST
			// This LIST work like producer and consumer,
			InitializeListHead(&DevExt->DmaReadList.queue_list);
			InitializeListHead(&DevExt->DmaReadList.done_list);
			KeInitializeSpinLock(&DevExt->DmaReadList.queueLock);
			KeInitializeSpinLock(&DevExt->DmaReadList.doneLock);
			//Init FIFO, one element of FIFO may hold several descriptor (1M)
			PDAQ4InitDiagFIFO(DevExt);

			//Init the DAM Table according to FIFO
			PDAQ4InitReadDMADescriptorTable(DevExt);
		}
		else
		{
			status = STATUS_INSUFFICIENT_RESOURCES;
		}
	}
	else
	{
		status = STATUS_DATA_ERROR;
	}

	//ENABLE_DMA_READ_INT(DevExt->ConfigSpace);
	return status;
}

NTSTATUS
PDAQ4InitRead(
	IN PDEVICE_EXTENSION DevExt
)
/*++
Routine Description:

Initialize read data structures

Arguments:

DevExt     Pointer to Device Extension

Return Value:

--*/
{
	UNREFERENCED_PARAMETER(DevExt);
	KdPrintEx((DPFLTR_IHVDRIVER_ID, DPFLTR_INFO_LEVEL, "PDAQInitRead"));

	return STATUS_SUCCESS;
}

VOID
PDAQ4Shutdown(
	IN PDEVICE_EXTENSION DevExt
)
/*++

Routine Description:

Reset the device to put the device in a known initial state.
This is called from D0Exit when the device is torn down or
when the system is shutdown. Note that Wdf has already
called out EvtDisable callback to disable the interrupt.

Arguments:

DevExt -  Pointer to our adapter

Return Value:

None

--*/
{
	UNREFERENCED_PARAMETER(DevExt);
}

VOID
PDAQHardwareReset(
	IN PDEVICE_EXTENSION DevExt
)
/*++
Routine Description:

Called by D0Exit when the device is being disabled or when the system is shutdown to
put the device in a known initial state.

Arguments:

DevExt     Pointer to Device Extension

Return Value:

--*/
{
	LARGE_INTEGER delay;
	UNREFERENCED_PARAMETER(DevExt);

	//
	// Wait 100 msec.
	//
	delay.QuadPart = WDF_REL_TIMEOUT_IN_MS(100);

	KeDelayExecutionThread(KernelMode, TRUE, &delay);
}