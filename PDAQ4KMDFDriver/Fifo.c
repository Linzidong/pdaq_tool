#include "Precomp.h"
BOOLEAN FIFOInit(DrvSharedFifo *fifo, PFIFO_CONFIG config)
{
	if (fifo == NULL)
		return FALSE;
	fifo->nAllocSize = config->nAllocSize;
	fifo->nItems = config->nItems;
	fifo->nItemSize = config->nItemSize;
	//fifo->nWrite = fifo->nOverflow = fifo->nRead = 0;
	fifo->nWrite = config->nWrite;
	fifo->nRead = config->nRead;
	fifo->pEvent = NULL;
	return TRUE;
}

BOOLEAN IsOverFlow(DrvSharedFifo *fifo)
{
	return fifo->nOverflow > 0;
}

BOOLEAN FifoUpdateOnWritten(DrvSharedFifo* pFifo)
{
	if (pFifo->nWrite < (pFifo->nRead + pFifo->nItems))
	{
		pFifo->nWrite++;
		if (pFifo->pEvent)
			KeSetEvent(pFifo->pEvent, IO_NO_INCREMENT, FALSE);
		return TRUE;
	}
	pFifo->nOverflow++;
	return FALSE;
}

BOOLEAN FifoUpdateOnRead(DrvSharedFifo* pFifo)
{
	if (pFifo->nRead < (pFifo->nWrite ))
	{
		pFifo->nRead++;
		if (pFifo->pEvent)
			KeSetEvent(pFifo->pEvent, IO_NO_INCREMENT, FALSE);
		return TRUE;
	}
	pFifo->nOverflow++;
	return FALSE;
}