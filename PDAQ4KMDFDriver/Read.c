#include "Precomp.h"
#define DMA_PLAN1 0
#define DMA_PLAN2 0
#define DMA_PLAN3 0
// Event handler for Read IRPs
VOID PDAQ4EvtIoRead(WDFQUEUE Queue, WDFREQUEST Request, size_t Length)
{
	NTSTATUS                status = STATUS_UNSUCCESSFUL;
	//PDEVICE_EXTENSION       devExt;

	/*
	KdPrintEx((DPFLTR_IHVDRIVER_ID, DPFLTR_INFO_LEVEL, " PDAQ4Read Request"));


	//
	// Get the DevExt from the Queue handle
	//
	devExt = PDAQ4GetDeviceContext(WdfIoQueueGetDevice(Queue));
	devExt->ReadInterrupt = FALSE;

	do {

		if (Length < MAX_RF_FRAME_SIZE + MAX_RT_FRAME_SIZE)  {
			status = STATUS_INVALID_BUFFER_SIZE;
			break;
		}

		//
		// Initialize this new DmaTransaction.
		//
		status = WdfDmaTransactionInitializeUsingRequest(
			devExt->ReadDmaTransaction,
			Request,
			PDAQ4ProgramReadDma,
			WdfDmaDirectionReadFromDevice);

		if (!NT_SUCCESS(status)) {
			KdPrintEx((DPFLTR_IHVDRIVER_ID, DPFLTR_ERROR_LEVEL, "WdfDmaTransactionInitializeUsingRequest failed: %X\n", status));

			break;
		}

#if 0 // FYI
		//
		// Modify the MaximumLength for this DmaTransaction only.
		//
		// Note: The new length must be less than or equal to that set when
		//       the DmaEnabler was created.
		//
		{
			ULONG length = devExt->MaximumTransferLength / 2;

			//TraceEvents(TRACE_LEVEL_INFORMATION, DBG_READ,
			//            "Setting a new MaxLen %d\n", length);

			WdfDmaTransactionSetMaximumLength(devExt->ReadDmaTransaction,
				length);
		}
#endif

		//
		// Execute this DmaTransaction.
		//
		status = WdfDmaTransactionExecute(devExt->ReadDmaTransaction,
			WDF_NO_CONTEXT);

		if (!NT_SUCCESS(status)) {
			//
			// Couldn't execute this DmaTransaction, so fail Request.
			//
			KdPrintEx((DPFLTR_IHVDRIVER_ID, DPFLTR_ERROR_LEVEL, "WdfDmaTransactionExecute failed: %d\n", status));
			break;
		}

		//
		// Indicate that Dma transaction has been started successfully.
		// The request will be complete by the Dpc routine when the DMA
		// transaction completes.
		//
		status = STATUS_SUCCESS;

	} while (0);

	//
	// If there are errors, then clean up and complete the Request.
	//
	if (!NT_SUCCESS(status)) {
		WdfDmaTransactionRelease(devExt->ReadDmaTransaction);
		WdfRequestComplete(Request, status);
	}
	KdPrintEx((DPFLTR_IHVDRIVER_ID, DPFLTR_INFO_LEVEL, "<-- PDAQ4Read: status %x\n!", status));


	return;
	*/
	status = STATUS_NOT_IMPLEMENTED;
	WdfRequestComplete(Request, status);
}

_Use_decl_annotations_
BOOLEAN
PDAQ4ProgramReadDma(
IN  WDFDMATRANSACTION       Transaction,
IN  WDFDEVICE               Device,
IN  WDFCONTEXT              Context,
IN  WDF_DMA_DIRECTION       Direction,
IN  PSCATTER_GATHER_LIST    SgList
)
/*++

Routine Description:

The framework calls a driver's EvtProgramDma event callback function
when the driver calls WdfDmaTransactionExecute and the system has
enough map registers to do the transfer. The callback function must
program the hardware to start the transfer. A single transaction
initiated by calling WdfDmaTransactionExecute may result in multiple
calls to this function if the buffer is too large and there aren't
enough map registers to do the whole transfer.


Arguments:

Return Value:

--*/
{
	PDEVICE_EXTENSION        devExt;
	//size_t                   offset;
	//PDAMDescripotr			 dmadescriptor;
	//BOOLEAN                  errors;
	//NTSTATUS status = 0;
	UNREFERENCED_PARAMETER(devExt);
	UNREFERENCED_PARAMETER(Context);
	UNREFERENCED_PARAMETER(Direction);

	/*
	KdPrintEx((DPFLTR_IHVDRIVER_ID, DPFLTR_INFO_LEVEL, "PDAQProgramReadDma\n"));


	//
	// Initialize locals
	//
	devExt = PDAQ4GetDeviceContext(Device);
	errors = FALSE;

	//
	// Get the number of bytes as the offset to the beginning of this
	// Dma operations transfer location in the buffer.
	//
	offset = WdfDmaTransactionGetBytesTransferred(Transaction);

	//
	// Setup the pointer to the next DMA_TRANSFER_ELEMENT
	// for both virtual and physical address references.
	//
	dmadescriptor = (PDAMDescripotr)devExt->RegWriteDMADescriptorBase;

	// Translate the System's SCATTER_GATHER_LIST elements
	// into the device's DMA_TRANSFER_ELEMENT elements.
	//
	KdPrintEx((DPFLTR_IHVDRIVER_ID, DPFLTR_INFO_LEVEL, "SG Element Number %d\n", SgList->NumberOfElements));

	// Just Got One RF Frame and RT Frame Here.
	// Plan 1, The physical data is handled by WDF
	// Plan 2, The physical data is common buffer.
	//LONG RF_FRAME_SIZE = MAX_RF_FRAME_SIZE;
	//LONG RT_FRAME_SIZE = MAX_RT_FRAME_SIZE;


#if DMA_PLAN3
	for (i = 0; i < SgList->NumberOfElements; i++) {
		ULONG lsb = SgList->Elements[i].Address.LowPart;
		ULONG msb = SgList->Elements[i].Address.HighPart;
		ULONG transfer_size = SgList->Elements[i].Length;
		KdPrintEx((DPFLTR_IHVDRIVER_ID, DPFLTR_INFO_LEVEL, "SG Element %d, Address HighPart%8X,LowPart %8X,Length = %X\n", i, msb,lsb, transfer_size));
	}
#endif
#if DMA_PLAN1
	for (i = 0; i < SgList->NumberOfElements; i++) {

		//
		// Construct this DTE.
		//
		// NOTE: The LocalAddress is the offset into the SRAM from
		//       where this Read will start.
		//

		dmadescriptor->lsb = SgList->Elements[i].Address.LowPart;
		dmadescriptor->msb = SgList->Elements[i].Address.HighPart;

		
		
		dmadescriptor->transfer_size = SgList->Elements[i].Length;
		dmadescriptor->end_of_chain = 0;
		dmadescriptor->next_descriptor = i + 1;
		dmadescriptor->eot_interrupt = FALSE;
		dmadescriptor->read_channel = RF_CHANNEL;
		KdPrintEx((DPFLTR_IHVDRIVER_ID, DPFLTR_INFO_LEVEL, "SG Element %d, Address HighPart%8X,LowPart %8X,Length = %X\n", i, dmadescriptor->msb, dmadescriptor->lsb, dmadescriptor->transfer_size));

		RF_FRAME_SIZE = RF_FRAME_SIZE - dmadescriptor->transfer_size;
		dmadescriptor++;

		if (RF_FRAME_SIZE<=0)
			break;
	}

	i += 1;
	for (; i < SgList->NumberOfElements; i++) {

		//
		// Construct this DTE.
		//
		// NOTE: The LocalAddress is the offset into the SRAM from
		//       where this Read will start.
		//

		dmadescriptor->lsb = SgList->Elements[i].Address.LowPart;
		dmadescriptor->msb = SgList->Elements[i].Address.HighPart;
		dmadescriptor->transfer_size = SgList->Elements[i].Length;
		dmadescriptor->end_of_chain = FALSE;
		dmadescriptor->next_descriptor = i + 1;
		dmadescriptor->read_channel = RT_CHANNEL;
		KdPrintEx((DPFLTR_IHVDRIVER_ID, DPFLTR_INFO_LEVEL, "SG Element %d, Address HighPart%8X,LowPart %8X,Length = %X\n", i, dmadescriptor->msb, dmadescriptor->lsb, dmadescriptor->transfer_size));

		

		RT_FRAME_SIZE = RT_FRAME_SIZE - dmadescriptor->transfer_size;
		if (RT_FRAME_SIZE <= 0)
		{
			dmadescriptor->next_descriptor = 0;
			dmadescriptor->end_of_chain = TRUE;
			dmadescriptor->eot_interrupt = TRUE;
			break;
		}
		dmadescriptor++;
	}
#endif

#if DMA_PLAN2
	RtlZeroMemory(devExt->ReadCommonBufferBaseRT, devExt->ReadCommonBufferSizeRT);
	RtlZeroMemory(devExt->ReadCommonBufferBaseRF, devExt->ReadCommonBufferSizeRF);
	PHYSICAL_ADDRESS commonbufferLA = devExt->ReadCommonBufferBaseLARF;
	dmadescriptor->lsb = commonbufferLA.LowPart;
	dmadescriptor->msb = commonbufferLA.HighPart;

	KdPrintEx((DPFLTR_IHVDRIVER_ID, DPFLTR_INFO_LEVEL, "SG Element %d, Address HighPart%8X,LowPart %8X,Length=%X\n", 0, dmadescriptor->msb, dmadescriptor->lsb, ROUND_TO_DESCRIPTOR_SIZE(MAX_RF_FRAME_SIZE)));
	
	dmadescriptor->transfer_size = ROUND_TO_DESCRIPTOR_SIZE(MAX_RF_FRAME_SIZE);
	dmadescriptor->end_of_chain = FALSE;
	dmadescriptor->next_descriptor = 1;
	dmadescriptor->read_channel = RF_CHANNEL;
	dmadescriptor->eot_interrupt = FALSE;

	dmadescriptor++;

	commonbufferLA = devExt->ReadCommonBufferBaseLART;
	dmadescriptor->lsb = commonbufferLA.LowPart;
	dmadescriptor->msb = commonbufferLA.HighPart;

	KdPrintEx((DPFLTR_IHVDRIVER_ID, DPFLTR_INFO_LEVEL, "SG Element %d, Address HighPart%8X,LowPart %8X,Length=%X\n", 1, dmadescriptor->msb, dmadescriptor->lsb, ROUND_TO_DESCRIPTOR_SIZE(MAX_RT_FRAME_SIZE)));

	dmadescriptor->transfer_size = ROUND_TO_DESCRIPTOR_SIZE(MAX_RT_FRAME_SIZE);
	dmadescriptor->end_of_chain =TRUE ;
	dmadescriptor->next_descriptor = 0;
	dmadescriptor->read_channel = RT_CHANNEL;
	dmadescriptor->eot_interrupt = TRUE;

#endif
	//
	// Start the DMA operation.
	// Acquire this device's InterruptSpinLock.
	//
	WdfInterruptAcquireLock(devExt->Interrupt);

	union {
		Dma_ctl1_reg  bits;
		ULONG     ulong;
	} dmaMode;

	PPDAQ4_TransferControl transfercontrol = (PPDAQ4_TransferControl)devExt->RegTransferControlBase;

	
	{
		PULONG DMA_CTRL_ADDRESS = (PULONG)&(transfercontrol->rDma_ctl1_reg);
		dmaMode.ulong = READ_REGISTER_ULONG(DMA_CTRL_ADDRESS);
		KdPrintEx((DPFLTR_IHVDRIVER_ID, DPFLTR_INFO_LEVEL, "READ DMA STATUS REGISTER %X\n", dmaMode.ulong));
		dmaMode.bits.DMAEnabled = TRUE;

		*DMA_CTRL_ADDRESS = dmaMode.ulong;
		KdPrintEx((DPFLTR_IHVDRIVER_ID, DPFLTR_INFO_LEVEL, "Write DMA START %16X\n", DMA_CTRL_ADDRESS));
		//WRITE_REGISTER_ULONG(address_to_write, dmaMode.ulong);
	}


	//
	// Release our interrupt spinlock
	//
	WdfInterruptReleaseLock(devExt->Interrupt);

	//
	// NOTE: This shows how to process errors which occur in the
	//       PFN_WDF_PROGRAM_DMA function in general.
	//       Basically the DmaTransaction must be deleted and
	//       the Request must be completed.
	//
	if (errors) {
		

		//
		// Must abort the transaction before deleting.
		//
		(VOID)WdfDmaTransactionDmaCompletedFinal(Transaction, 0, &status);
		ASSERT(NT_SUCCESS(status));

		PDAQ4ReadRequestComplete(Transaction, STATUS_INVALID_DEVICE_STATE);
		KdPrintEx((DPFLTR_IHVDRIVER_ID, DPFLTR_ERROR_LEVEL, "PDAQ4ProgramReadDma FAILED \n"));
		return FALSE;
	}

	if (!devExt->ReadInterrupt)
	{
		// Follow code is for the hardware without interrupt implementation
		LARGE_INTEGER delay;
		delay.QuadPart = WDF_REL_TIMEOUT_IN_MS(1);
		KeDelayExecutionThread(KernelMode, TRUE, &delay);
		ULONG length = WdfDmaTransactionGetCurrentDmaTransferLength(Transaction);
		KdPrintEx((DPFLTR_IHVDRIVER_ID, DPFLTR_ERROR_LEVEL, "WdfDmaTransactionGetCurrentDmaTransferLength %d \n",length));
		WdfDmaTransactionDmaCompleted(Transaction, &status);

		UCHAR *RFdata = (UCHAR*)devExt->RF_FIFO_CommonBufferBase + sizeof(DrvSharedFifo);
		UCHAR *RTdata = (UCHAR*)devExt->RF_FIFO_CommonBufferBase + sizeof(DrvSharedFifo);

		WDFREQUEST   Request = WdfDmaTransactionGetRequest(Transaction);
		UCHAR *DataBuffer;
		ULONG expectedsize = ROUND_TO_DESCRIPTOR_SIZE(MAX_RF_FRAME_SIZE) + ROUND_TO_DESCRIPTOR_SIZE(MAX_RT_FRAME_SIZE);
		ULONG BufferLength;
		status = WdfRequestRetrieveOutputBuffer(Request, expectedsize, &DataBuffer, &BufferLength);
		if (!NT_SUCCESS(status) || expectedsize != BufferLength) {
			KdPrintEx((DPFLTR_IHVDRIVER_ID, DPFLTR_ERROR_LEVEL, "REQUEST DATA LENGTH IS  %X \n", BufferLength));
			return FALSE;
		}
		RtlCopyMemory(DataBuffer, RFdata, ROUND_TO_DESCRIPTOR_SIZE(MAX_RF_FRAME_SIZE));
		RtlCopyMemory(DataBuffer + ROUND_TO_DESCRIPTOR_SIZE(MAX_RF_FRAME_SIZE), RTdata, ROUND_TO_DESCRIPTOR_SIZE(MAX_RT_FRAME_SIZE));
		KdPrintEx((DPFLTR_IHVDRIVER_ID, DPFLTR_ERROR_LEVEL, "FIRST REGISTER VALUE %X \n", *RFdata));

		
		PDAQ4ReadRequestComplete(Transaction, status);
		ASSERT(NT_SUCCESS(status));
	}



	KdPrintEx((DPFLTR_IHVDRIVER_ID, DPFLTR_INFO_LEVEL, "PDAQ4ProgramReadDma FINISHED \n"));
	*/
	return TRUE;
}

VOID
PDAQ4ReadRequestComplete(
IN WDFDMATRANSACTION  DmaTransaction,
IN NTSTATUS           Status
)
/*++

Routine Description:

Arguments:

Return Value:

--*/
{
	/*
	WDFREQUEST         request;
	size_t             bytesTransferred;

	//
	// Get the associated request from the transaction.
	//
	request = WdfDmaTransactionGetRequest(DmaTransaction);

	ASSERT(request);

	//
	// Get the final bytes transferred count.
	//
	bytesTransferred = WdfDmaTransactionGetBytesTransferred(DmaTransaction);

	KdPrintEx((DPFLTR_IHVDRIVER_ID, DPFLTR_INFO_LEVEL, "DMA Transcation Transferred %d\n", bytesTransferred));

	WdfDmaTransactionRelease(DmaTransaction);

	//
	// Complete this Request.
	//
	WdfRequestCompleteWithInformation(request, Status, bytesTransferred);
	*/

}

