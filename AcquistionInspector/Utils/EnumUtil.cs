﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace AcquistionInspector.Utils
{
    public static class EnumUtil
    {
        public static bool GetEnumReadOnly(this Enum val)
        {
            Type type = val.GetType();
            FieldInfo fd = type.GetField(val.ToString());
            if (fd == null)
                return true;
            object[] attrs = fd.GetCustomAttributes(typeof(ReadOnlyAttribute), false);
            bool readOnly = false;
            foreach (ReadOnlyAttribute attr in attrs)
            {
                readOnly = attr.ReadOnly;
            }
            return readOnly;
        }

        public static bool GetEnumIsRegion(this Enum val)
        {
            Type type = val.GetType();
            FieldInfo fd = type.GetField(val.ToString());
            if (fd == null)
                return true;
            object[] attrs = fd.GetCustomAttributes(typeof(IsRegionAttribute), false);
            bool readOnly = false;
            foreach (IsRegionAttribute attr in attrs)
            {
                readOnly = attr.IsRegion;
            }
            return readOnly;
        }

        public static UInt32 GetEnumLastEntry(this Enum val)
        {
            Type type = val.GetType();
            FieldInfo fd = type.GetField(val.ToString());
            if (fd == null)
                return 0;
            object[] attrs = fd.GetCustomAttributes(typeof(LastEntryAttribute), false);
            UInt32 lastEntry = 0;
            foreach (LastEntryAttribute attr in attrs)
            {
                lastEntry = attr.LastEntry;
            }
            return lastEntry;
        }

        public static List<UInt32> GetEnumDefaultValues(this Enum val)
        {
            Type type = val.GetType();
            FieldInfo fd = type.GetField(val.ToString());
            if (fd == null)
                return null;
            object[] attrs = fd.GetCustomAttributes(typeof(DefaultValueAttribute), false);
            List<UInt32> defaultValues = null;
            foreach (DefaultValueAttribute attr in attrs)
            {
                defaultValues = attr.DefaultValues;
            }
            return defaultValues;
        }

        public static string GetEnumDescription(this Enum val)
        {
            Type type = val.GetType();
            FieldInfo fd = type.GetField(val.ToString());
            if (fd == null)
                return "";
            object[] attrs = fd.GetCustomAttributes(typeof(DescriptionAttribute), false);
            string description = string.Empty;
            foreach (DescriptionAttribute attr in attrs)
            {
                description = attr.Description;
            }
            return description;
        }

        public static Type GetEnumMatchType(this Enum val)
        {
            Type type = val.GetType();
            FieldInfo fd = type.GetField(val.ToString());
            if (fd == null)
                return null;
            object[] attrs = fd.GetCustomAttributes(typeof(MatchTypeAttribute), false);
            Type matchType = null;
            foreach (MatchTypeAttribute attr in attrs)
            {
                matchType = attr.MatchType;
            }
            return matchType;
        }
    }
}