﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using PDAQ4DriverAgent.PDAQ4Implementation;
using PDAQ4DriverInterface;
using System.IO;
using System.Collections;
using AcquistionInspector.Models;
using System.Windows;

namespace AcquistionInspector
{
    public delegate void OnFrame(Frame frame);

    public delegate void OnRTFrameSizeChanged(int new_RTVectorPerFrame, int new_RFSamplesPerVector);

    public delegate void OnRFFrameSizeChanged(int new_RFVectorPerFrame, int new_RFSamplesPerVector);

    public delegate void OnViewStyleChanged(bool circleview);

    public delegate void OnChannelConnected(DMAInfo dmainfo, FifoInfo info);

    public class PDAQ4Controller
    {
        // max valid register number
        public const int MAX_REG_SPACE_SIZE = 0x100000;

        public const int BYTE_PER_PAGE = 256;
        public const int page_size = 64; //64 register per page
        private DriverAgent drvagent;

        public delegate void ReadRegistersCompletedCallBack(UInt32[] mem);

        public event OnFrame OnFrameEvent;

        public event OnFrame onReadRawEvent;

        public event OnRTFrameSizeChanged RTFrameSizeChangedEvent;

        public event OnRFFrameSizeChanged RFFrameSizeChangedEvent;

        public event OnViewStyleChanged ViewStyleChanged;

        public event OnChannelConnected ChannelConnected;

        private List<ReadRegistersCompletedCallBack> RegisterCompletedObservers;
        private static PDAQ4Controller s_instance = null;
        private volatile float read_register_thread_interval = 1.0f; // 1 second
        private FiFoConnection connection_rf = null;
        private FiFoConnection connection_rt = null;
        private FiFoConnection connection_rf_raw = null;
        private int ispdaq4found = 0;

        public bool AutoRefresh
        { get; set; }

        public DriverAgent DrvAgent
        {
            get { return drvagent; }
        }

        public static PDAQ4Controller GetInstance()
        {
            if (s_instance == null)
                s_instance = new PDAQ4Controller();
            return s_instance;
        }

        private PDAQ4Controller()
        {
            RegisterCompletedObservers = new List<ReadRegistersCompletedCallBack>();
            drvagent = new DriverAgent();
            AutoRefresh = false;
        }

        public void OnFrame(Frame frame)
        {
            Console.WriteLine("Frameblock size = {0}", frame.FrameBlock.Length);

            OnFrameEvent(frame);
        }

        public void OnReadFrame(Frame frame)
        {
            onReadRawEvent(frame);
            // fill the frame.FrameBlock
        }

        public int IsPDAQ4Found
        {
            get { return ispdaq4found; }
            set { Interlocked.Exchange(ref ispdaq4found, value); }
        }

        public IPDAQDevice FindPDAQ4_CARD()
        {
            IPDAQDevice device = drvagent.FindPDAQ4();
            return device;
        }

        public void OpenPDAQ4_CARD()
        {
            drvagent.OpenPDAQ4();
        }

        public void CreateACQChannels(DMAInfo dmainfo)
        {
            FifoInfo[] fifos = drvagent.QueryFIFO();

            ChannelHeader rf_channelheader = new ChannelHeader();
            rf_channelheader.ChannelIndex = 0;
            rf_channelheader.ChannelType = ChannelType.RF_CHANNEL;
            rf_channelheader.FrameHdrSize = 96; // TODO - use type size
            rf_channelheader.FrameRate = 30;
            rf_channelheader.VectorHdrSize = 16;
            rf_channelheader.VectorsPerFrame = (ushort)dmainfo.vector_per_frame;
            rf_channelheader.SamplesPerVector = (ushort)dmainfo.rf_sample_per_vector;
            rf_channelheader.BitsPerSample = 16;
            rf_channelheader.SampleMask = 0x0fff;
            rf_channelheader.SwVersion = 0x0300;
            connection_rf = new Frame_Connection(rf_channelheader, fifos[0]);
            RFFrameSizeChangedEvent((int)dmainfo.vector_per_frame, (int)dmainfo.rf_sample_per_vector);

            ChannelHeader rt_channelheader = new ChannelHeader();
            rt_channelheader.ChannelType = ChannelType.RT_CHANNEL;
            rt_channelheader.ChannelIndex = 1;
            rt_channelheader.FrameHdrSize = 96; // TODO - use type size
            rt_channelheader.FrameRate = 30;
            rt_channelheader.VectorHdrSize = 16;
            rt_channelheader.VectorsPerFrame = (ushort)dmainfo.vector_per_frame;
            rt_channelheader.SamplesPerVector = (ushort)dmainfo.rt_sample_per_vector;
            rt_channelheader.BitsPerSample = 8;

            RTFrameSizeChangedEvent((int)dmainfo.vector_per_frame, (int)dmainfo.rt_sample_per_vector);
            connection_rt = new Frame_Connection(rt_channelheader, fifos[1]);

            //ChannelHeader rf_raw_channelheader = new ChannelHeader();
            //rf_raw_channelheader.ChannelType = ChannelType.RF_RAW_CHANNEL;

            //rf_raw_channelheader.ChannelIndex = 1;
            //rf_raw_channelheader.FrameHdrSize = 0; // TODO - use type size
            //rf_raw_channelheader.FrameRate = 30;
            //rf_raw_channelheader.VectorHdrSize = 0;
            //rf_raw_channelheader.VectorsPerFrame = (ushort)dmainfo.vector_per_frame;
            //rf_raw_channelheader.SamplesPerVector = (ushort)dmainfo.rf_sample_per_vector;
            //rf_raw_channelheader.BitsPerSample = 16;

            //connection_rf_raw = new Diag_Frame_Connection(rf_raw_channelheader, fifos[2]);

            drvagent.PDAQ4ConnectFIFOChannel(connection_rf);
            drvagent.PDAQ4ConnectFIFOChannel(connection_rt);
            //drvagent.PDAQ4ConnectFIFOChannel(connection_rf_raw);
            //ChannelConnected(dmainfo, fifos[2]);
            connection_rf.FrameReadyEvent += new DeviceGenericEvent<Frame>(this.OnFrame);
            connection_rt.FrameReadyEvent += new DeviceGenericEvent<Frame>(this.OnFrame);
            //connection_rf_raw.FrameReadyEvent += new DeviceGenericEvent<Frame>(this.OnReadFrame);
            connection_rt.start();
            connection_rf.start();
            //connection_rf_raw.start();
        }

        public bool WriteRegisterValue(RegisterType type, Enum offset, uint value)
        {
            bool success = drvagent.WriteRegisterValue((uint)type, Convert.ToUInt32(offset), value);

            return success;
        }

        public bool WriteRegisterValue(RegisterType type, uint offset, uint value)
        {
            if (drvagent == null)
            {
                return false;
            }

            bool success = drvagent.WriteRegisterValue((uint)type, (uint)offset, value);

            return success;
        }

        internal bool WriteRegisterValue(RegisterModel register, uint value)
        {
            bool success = WriteRegisterValue(register.RegisterType, register.Offset, value);

            return success;
        }

        public void ClosePDAQ4_CARD()
        {
            if (drvagent != null)
            {
                drvagent.StopDMA();
                //drvagent.StopRawRFDMA();
                drvagent.ClosePDAQ4();
            }

            if (connection_rf != null)
            {
                connection_rt.stop();
            }

            if (connection_rt != null)
            {
                connection_rf.stop();
            }
            //connection_rf_raw.stop();
        }

        public DMAInfo GetDMAInfo()
        {
            return drvagent.GetDMAInfo();
        }

        public void InitFPGA()
        {
        }

        public void InitFPGA_InPDAQTool(Action<string> strAct, Action<bool> iniAct)
        {
            bool succeed = true;
            iniAct.Invoke(true);
            string text;
            text = "Init ADC...";
            strAct.Invoke(text);
            succeed = InitADC() & succeed;

            text = "Init ADC Receiver...";
            strAct.Invoke(text);
            succeed = InitADCReceiver() & succeed;
            Thread.Sleep(200);

            text = "Init MDU...";
            strAct.Invoke(text);
            succeed = InitMDU() & succeed;
            Thread.Sleep(200);

            text = "Init TX High Voltage...";
            strAct.Invoke(text);
            succeed = InitTX_HVWG() & succeed;
            Thread.Sleep(200);

            text = "Init RX AFE...";
            strAct.Invoke(text);
            succeed = InitRX_AFE() & succeed;
            Thread.Sleep(200);

            text = "Init RF TXHOLDOFF...";
            strAct.Invoke(text);
            succeed = InitRF_TX_HOLDOFF() & succeed;
            Thread.Sleep(200);

            text = "Init RF Framer...";
            strAct.Invoke(text);
            succeed = InitRF_FRAMER() & succeed;
            Thread.Sleep(200);

            text = "Init RT TXHOLDOFF...";
            strAct.Invoke(text);
            succeed = InitRT_TX_HOLDOFF() & succeed;
            Thread.Sleep(50);

            text = "Init RT Framer...";
            strAct.Invoke(text);
            succeed = InitRT_FRAMER() & succeed;
            Thread.Sleep(50);

            text = "Init DSP...";
            succeed = InitDSP() & succeed;
            Thread.Sleep(200);

            text = "Init I2C...";
            strAct.Invoke(text);
            succeed = InitI2C() & succeed;

            iniAct.Invoke(false);

            if (succeed)
            {
                MessageBox.Show("Initialize Succeed");
            }
            else
            {
                MessageBox.Show("Initialize failed,please try another way");
            }
        }

        private bool InitRX_AFE()
        {
            var flg = true;

            uint ramCount = 0;
            var attenVector = new int[] {
            0x00, // Baseline value. Don't change!
		    0x52,
            0x87,
            0x9C,
            0xA7,
            0xAD,
            0xB3,
            0xB7,
            0xBA,
            0xBC,
            0xBE,
            0xBE,
            0xBE,
            0xBE,
            0xBE,
            0xBE,
            0xBE,
            0xBE};

            int attenVectorSize = attenVector.Length;
            int extrapCount = 8;
            int ramVal = 0;

            for (int c = 0; c < (attenVectorSize - 1); c++)
            {
                for (int d = 0; d < extrapCount; d++)
                {
                    int vecVal = attenVector[c] + ((int)d) * (attenVector[c + 1] - attenVector[c]) / extrapCount;
                    // Figure out the associated voltage in millivolts.
                    // TODO: Upper limit it too high! This will cause non-linear behavior
                    // in some cases! This is being set as such to emulate V3.
                    int upperLim = (1024);
                    int lowerLimB = (2048 + 512);
                    int lowerLimC = (2048 + 512);
                    int dacOffset = (2047);//-64);
                    int dacFS = 7250;

                    int V = 1587 - vecVal * 34;
                    int VtgcB = V - upperLim;

                    // Make sure the voltage is not out of range.
                    if (VtgcB < -lowerLimB) VtgcB = -lowerLimB;
                    if (VtgcB > upperLim) VtgcB = upperLim;

                    // Figure out how much is left over for TGC C
                    int VtgcC = V - VtgcB;

                    // Make sure the voltage is not out of range.
                    if (VtgcC < -lowerLimC) VtgcC = -lowerLimC;
                    if (VtgcC > upperLim) VtgcC = upperLim;

                    // Convert the old vector value for the new DAC.
                    // The 2 LSBs are TGC 1 (B) and the MSBs are TGC 2 (C).
                    ramVal = (((VtgcC * 4000 / dacFS) + dacOffset) << 16) | ((VtgcB * 4000 / dacFS) + dacOffset);

                    // And load...
                    flg = WriteRegisterValue(RegisterType.RX_AFE, (uint)Register_RX_AFE.TGC_VALUE_RAM + ramCount * 0x04, (uint)ramVal) & flg;
                    ramCount++;
                    //IOWR_TGC_RAM(PDAQ_RX_AFE_CONTROL_BASE, ramCount++, ramVal);
                }
            }

            // Stretch out the last entry.
            for (int c = 0; c < 2 * extrapCount; c++)
            {
                flg = WriteRegisterValue(RegisterType.RX_AFE, (uint)Register_RX_AFE.TGC_VALUE_RAM + ramCount * 0x04, (uint)ramVal) & flg;

                //WRITE_REGISTER_ULONG((PULONG)(Addr_RX_AFE + 0x4000 + ramCount * 0x04), ramVal);
                ramCount++;
                //IOWR_TGC_RAM(PDAQ_RX_AFE_CONTROL_BASE, ramCount++, ramVal);
            }
            flg = WriteRegisterValue(RegisterType.RX_AFE, Register_RX_AFE.TGC_MAX_REPLAY_REG, (uint)(ramCount - 1)) & flg;

            // Set a hold-off value.
            flg = WriteRegisterValue(RegisterType.RX_AFE, Register_RX_AFE.TGC_HOLDOFF_REG, 110) & flg;

            // Set the control value.
            // Bit 0 selects the HPF, 5 MHz is 0, 20 MHz is 1.
            // Bit 1 selects the LPF, 45 MHz is 0, 88 MHz is 1.
            // For now, set to 3.
            flg = WriteRegisterValue(RegisterType.RX_AFE, Register_RX_AFE.RX_AFE_CONTROL_REG, 0x03) & flg;
            // Turn on the high voltage.
            // Now write the bit to turn on the supply.
            flg = WriteRegisterValue(RegisterType.TX_Control, Register_TX_Control.TX_CONTROL_REG, 0x01) & flg;

            if (!flg)
            {
                MessageBox.Show("failed in RX AFE");
            }

            return flg;
        }

        private bool InitI2C()
        {
            var flg = true;
            flg = WriteRegisterValue(RegisterType.I2C, RegCon.AD5593_CTL_CFG_WR | RegCon.AD5593_REG_SOFT_RST, 0x0DAC) & flg;

            Thread.Sleep(1000);

            flg = WriteRegisterValue(RegisterType.I2C, RegCon.AD5593_CTL_CFG_WR | RegCon.AD5593_REG_DAC_PIN, 0x0001) & flg;
            flg = WriteRegisterValue(RegisterType.I2C, RegCon.AD5593_CTL_CFG_WR | RegCon.AD5593_REG_PD_REF, 0x0200) & flg;
            flg = WriteRegisterValue(RegisterType.I2C, RegCon.AD5593_CTL_DAC_WR, 0x0937) & flg;

            if (!flg)
            {
                MessageBox.Show("failed in I2C");
            }

            return flg;
        }

        private bool InitDSP()
        {
            var flg = true;

            flg = WriteRegisterValue(RegisterType.DSP, 0, 0x01) & flg;
            flg = WriteRegisterValue(RegisterType.DSP, 0x04, 0x00) & flg;
            flg = WriteRegisterValue(RegisterType.DSP, 0x08, 0x00) & flg;
            flg = WriteRegisterValue(RegisterType.DSP, 0x0C, 0x00) & flg;
            flg = WriteRegisterValue(RegisterType.DSP, 0x400, 0x01) & flg;
            flg = WriteRegisterValue(RegisterType.DSP, 0x404, 0x00) & flg;
            flg = WriteRegisterValue(RegisterType.DSP, 0x408, 0x00) & flg;
            flg = WriteRegisterValue(RegisterType.DSP, 0x40C, 0x00) & flg;
            flg = WriteRegisterValue(RegisterType.DSP, 0x800, 0x00) & flg;
            flg = WriteRegisterValue(RegisterType.DSP, 0x804, 0x00) & flg;
            flg = WriteRegisterValue(RegisterType.DSP, 0x808, 0x00) & flg;
            flg = WriteRegisterValue(RegisterType.DSP, 0x80C, 0x00) & flg;
            flg = WriteRegisterValue(RegisterType.DSP, 0xC04, 0x04) & flg;
            flg = WriteRegisterValue(RegisterType.DSP, 0xC08, 0x80000) & flg;
            flg = WriteRegisterValue(RegisterType.DSP, 0xC0C, 0x00) & flg;
            flg = WriteRegisterValue(RegisterType.DSP, 0xC10, 0x00) & flg;

            flg = WriteRegisterValue(RegisterType.DSP, 0x100, 0x7FFF) & flg;
            flg = WriteRegisterValue(RegisterType.DSP, 0x104, 0x00) & flg;
            flg = WriteRegisterValue(RegisterType.DSP, 0x104, 0x00) & flg;
            flg = WriteRegisterValue(RegisterType.DSP, 0x10C, 0x00) & flg;
            flg = WriteRegisterValue(RegisterType.DSP, 0x110, 0x00) & flg;
            flg = WriteRegisterValue(RegisterType.DSP, 0x114, 0x00) & flg;
            flg = WriteRegisterValue(RegisterType.DSP, 0x118, 0x00) & flg;
            flg = WriteRegisterValue(RegisterType.DSP, 0x11C, 0x00) & flg;
            flg = WriteRegisterValue(RegisterType.DSP, 0x120, 0x00) & flg;
            flg = WriteRegisterValue(RegisterType.DSP, 0x124, 0x00) & flg;
            flg = WriteRegisterValue(RegisterType.DSP, 0x128, 0x00) & flg;
            flg = WriteRegisterValue(RegisterType.DSP, 0x12C, 0x00) & flg;
            flg = WriteRegisterValue(RegisterType.DSP, 0x130, 0x00) & flg;
            flg = WriteRegisterValue(RegisterType.DSP, 0x134, 0x00) & flg;
            flg = WriteRegisterValue(RegisterType.DSP, 0x138, 0x00) & flg;
            flg = WriteRegisterValue(RegisterType.DSP, 0x13C, 0x00) & flg;

            ////////
            flg = WriteRegisterValue(RegisterType.DSP, 0x500, 0x07) & flg;
            flg = WriteRegisterValue(RegisterType.DSP, 0x504, 0xFFFE) & flg;
            flg = WriteRegisterValue(RegisterType.DSP, 0x508, 0x08) & flg;
            flg = WriteRegisterValue(RegisterType.DSP, 0x50C, 0x08) & flg;
            flg = WriteRegisterValue(RegisterType.DSP, 0x510, 0xFFE5) & flg;
            flg = WriteRegisterValue(RegisterType.DSP, 0x514, 0xFFE5) & flg;
            flg = WriteRegisterValue(RegisterType.DSP, 0x518, 0x02) & flg;
            flg = WriteRegisterValue(RegisterType.DSP, 0x51C, 0xFFDD) & flg;
            flg = WriteRegisterValue(RegisterType.DSP, 0x520, 0xFFE5) & flg;
            flg = WriteRegisterValue(RegisterType.DSP, 0x524, 0x4C) & flg;
            flg = WriteRegisterValue(RegisterType.DSP, 0x528, 0x3B) & flg;
            flg = WriteRegisterValue(RegisterType.DSP, 0x52C, 0x01) & flg;
            flg = WriteRegisterValue(RegisterType.DSP, 0x530, 0x6C) & flg;
            flg = WriteRegisterValue(RegisterType.DSP, 0x534, 0x41) & flg;
            flg = WriteRegisterValue(RegisterType.DSP, 0x538, 0xFF5A) & flg;
            flg = WriteRegisterValue(RegisterType.DSP, 0x53C, 0xFF9D) & flg;
            flg = WriteRegisterValue(RegisterType.DSP, 0x540, 0xFFEE) & flg;
            flg = WriteRegisterValue(RegisterType.DSP, 0x544, 0xFEF3) & flg;
            flg = WriteRegisterValue(RegisterType.DSP, 0x548, 0xFF7B) & flg;
            flg = WriteRegisterValue(RegisterType.DSP, 0x54C, 0x131) & flg;
            flg = WriteRegisterValue(RegisterType.DSP, 0x550, 0x82) & flg;
            flg = WriteRegisterValue(RegisterType.DSP, 0x554, 0x3E) & flg;
            flg = WriteRegisterValue(RegisterType.DSP, 0x558, 0x242) & flg;
            flg = WriteRegisterValue(RegisterType.DSP, 0x55C, 0xF6) & flg;
            flg = WriteRegisterValue(RegisterType.DSP, 0x560, 0xFE08) & flg;
            flg = WriteRegisterValue(RegisterType.DSP, 0x564, 0xFF8B) & flg;
            flg = WriteRegisterValue(RegisterType.DSP, 0x568, 0xFF5C) & flg;
            flg = WriteRegisterValue(RegisterType.DSP, 0x56C, 0xFB74) & flg;
            flg = WriteRegisterValue(RegisterType.DSP, 0x570, 0xFE59) & flg;
            flg = WriteRegisterValue(RegisterType.DSP, 0x574, 0x318) & flg;
            flg = WriteRegisterValue(RegisterType.DSP, 0x578, 0xFFEC) & flg;
            flg = WriteRegisterValue(RegisterType.DSP, 0x57C, 0x198) & flg;
            flg = WriteRegisterValue(RegisterType.DSP, 0x580, 0x990) & flg;
            flg = WriteRegisterValue(RegisterType.DSP, 0x584, 0x354) & flg;
            flg = WriteRegisterValue(RegisterType.DSP, 0x588, 0xFAAF) & flg;
            flg = WriteRegisterValue(RegisterType.DSP, 0x58C, 0x26B) & flg;
            flg = WriteRegisterValue(RegisterType.DSP, 0x590, 0xFAD6) & flg;
            flg = WriteRegisterValue(RegisterType.DSP, 0x594, 0xDFDD) & flg;
            flg = WriteRegisterValue(RegisterType.DSP, 0x598, 0xF1F9) & flg;
            flg = WriteRegisterValue(RegisterType.DSP, 0x59C, 0x2AB2) & flg;

            ////
            flg = WriteRegisterValue(RegisterType.DSP, 0x900, 0xFFFB) & flg;
            flg = WriteRegisterValue(RegisterType.DSP, 0xA00, 0xFFFB) & flg;
            flg = WriteRegisterValue(RegisterType.DSP, 0x904, 0xFFE4) & flg;
            flg = WriteRegisterValue(RegisterType.DSP, 0xA04, 0xFFE4) & flg;
            flg = WriteRegisterValue(RegisterType.DSP, 0x908, 0xFFE4) & flg;
            flg = WriteRegisterValue(RegisterType.DSP, 0xA08, 0xFFE4) & flg;
            flg = WriteRegisterValue(RegisterType.DSP, 0x90C, 0x0D) & flg;
            flg = WriteRegisterValue(RegisterType.DSP, 0xA0C, 0x0D) & flg;
            flg = WriteRegisterValue(RegisterType.DSP, 0x910, 0x58) & flg;
            flg = WriteRegisterValue(RegisterType.DSP, 0xA10, 0x58) & flg;
            flg = WriteRegisterValue(RegisterType.DSP, 0x914, 0x79) & flg;
            flg = WriteRegisterValue(RegisterType.DSP, 0xA14, 0x79) & flg;
            flg = WriteRegisterValue(RegisterType.DSP, 0x918, 0x19) & flg;
            flg = WriteRegisterValue(RegisterType.DSP, 0xA18, 0x19) & flg;
            flg = WriteRegisterValue(RegisterType.DSP, 0x91C, 0xFF47) & flg;
            flg = WriteRegisterValue(RegisterType.DSP, 0xA1C, 0xFF47) & flg;
            flg = WriteRegisterValue(RegisterType.DSP, 0x920, 0xFEB1) & flg;
            flg = WriteRegisterValue(RegisterType.DSP, 0xA20, 0xFEB1) & flg;
            flg = WriteRegisterValue(RegisterType.DSP, 0x924, 0xFF3D) & flg;
            flg = WriteRegisterValue(RegisterType.DSP, 0xA24, 0xFF3D) & flg;
            flg = WriteRegisterValue(RegisterType.DSP, 0x928, 0x010B) & flg;
            flg = WriteRegisterValue(RegisterType.DSP, 0xA28, 0x010B) & flg;
            flg = WriteRegisterValue(RegisterType.DSP, 0x92C, 0x02D5) & flg;
            flg = WriteRegisterValue(RegisterType.DSP, 0xA2C, 0x02D5) & flg;
            flg = WriteRegisterValue(RegisterType.DSP, 0x930, 0x028D) & flg;
            flg = WriteRegisterValue(RegisterType.DSP, 0xA30, 0x028D) & flg;
            flg = WriteRegisterValue(RegisterType.DSP, 0x934, 0xff3b) & flg;
            flg = WriteRegisterValue(RegisterType.DSP, 0xA34, 0xff3b) & flg;
            flg = WriteRegisterValue(RegisterType.DSP, 0x938, 0xfaa3) & flg;
            flg = WriteRegisterValue(RegisterType.DSP, 0xA38, 0xfaa3) & flg;
            flg = WriteRegisterValue(RegisterType.DSP, 0x93c, 0xf903) & flg;
            flg = WriteRegisterValue(RegisterType.DSP, 0xA3c, 0xf903) & flg;
            flg = WriteRegisterValue(RegisterType.DSP, 0x940, 0xfe60) & flg;
            flg = WriteRegisterValue(RegisterType.DSP, 0xA40, 0xfe60) & flg;
            flg = WriteRegisterValue(RegisterType.DSP, 0x944, 0x0b13) & flg;
            flg = WriteRegisterValue(RegisterType.DSP, 0xA44, 0x0b13) & flg;
            flg = WriteRegisterValue(RegisterType.DSP, 0x948, 0x1a69) & flg;
            flg = WriteRegisterValue(RegisterType.DSP, 0xA48, 0x1a69) & flg;
            flg = WriteRegisterValue(RegisterType.DSP, 0x94c, 0x24e2) & flg;
            flg = WriteRegisterValue(RegisterType.DSP, 0xA4c, 0x24e2) & flg;

            if (!flg)
            {
                MessageBox.Show("failed in DSP");
            }

            return flg;
        }

        private bool InitRT_FRAMER()
        {
            var flg = true;
            //flg = WriteRegisterValue(RegisterType., RegCon.AD5593_CTL_CFG_WR | RegCon.AD5593_REG_SOFT_RST, 0x0DAC) & flg;

            return flg;
        }

        private bool InitRF_TX_HOLDOFF()
        {
            var flg = true;
            flg = WriteRegisterValue(RegisterType.RF_TXHOLDOFF, Register_TX_HOLDOFF.TX_HOLDOFF_DELAY_REG, 105) & flg;

            if (!flg)
            {
                MessageBox.Show("failed in RF TXHOLDOFF");
            }

            return flg;
        }

        private bool InitRF_FRAMER()
        {
            return true;
        }

        private bool InitRT_TX_HOLDOFF()
        {
            var flg = true;
            flg = WriteRegisterValue(RegisterType.RT_TXHOLDOFF, Register_TX_HOLDOFF.TX_HOLDOFF_LENGTH_REG, 0x280) & flg;
            flg = WriteRegisterValue(RegisterType.RT_TXHOLDOFF, Register_TX_HOLDOFF.TX_HOLDOFF_DELAY_REG, 105) & flg;

            if (!flg)
            {
                MessageBox.Show("failed in RT TXHOLDOFF");
            }

            return flg;
        }

        private bool InitTX_HVWG()
        {
            bool flg = true;

            flg = WriteRegisterValue(RegisterType.TX_Control, (uint)Register_TX_Control.TX_BURST_RAM, 0x00) & flg;
            flg = WriteRegisterValue(RegisterType.TX_Control, (uint)Register_TX_Control.TX_BURST_RAM + 0x04, 0x01) & flg;
            flg = WriteRegisterValue(RegisterType.TX_Control, (uint)Register_TX_Control.TX_BURST_RAM + 0x08, 0x01) & flg;
            flg = WriteRegisterValue(RegisterType.TX_Control, (uint)Register_TX_Control.TX_BURST_RAM + 0x0c, 0x02) & flg;
            flg = WriteRegisterValue(RegisterType.TX_Control, (uint)Register_TX_Control.TX_BURST_RAM + 0x10, 0x02) & flg;
            flg = WriteRegisterValue(RegisterType.TX_Control, (uint)Register_TX_Control.TX_BURST_RAM + 0x14, 0x00) & flg;

            flg = WriteRegisterValue(RegisterType.TX_Control, (uint)Register_TX_Control.TX_ACTIVE_REG, 6) & flg;
            flg = WriteRegisterValue(RegisterType.TX_Control, (uint)Register_TX_Control.TX_HOLDOFF_REG, 45) & flg;

            if (!flg)
            {
                MessageBox.Show("failed in TX_HVWG");
            }

            return flg;
        }

        private bool InitMDU()
        {
            var flg = true;
            flg = WriteRegisterValue(RegisterType.Mdu_Control, Register_MDU_Control.MDU_CONTROL_REG, 0x00) & flg;
            flg = WriteRegisterValue(RegisterType.Mdu_Control, Register_MDU_Control.WATCHDOG_TIMEOUT_VALUE_REG, 1000) & flg;
            flg = WriteRegisterValue(RegisterType.Mdu_Control, Register_MDU_Control.WATCHDOG_CONTROL_REG, 0) & flg;

            int rpm = 1500;
            int speedTarget = (int)(200.0e6 * 60.0 / rpm / 200);

            uint pwm_target_speed_value;
            uint pwm_target_setup_change;

            if (speedTarget >= 0)
            {
                pwm_target_speed_value = 3000;
                pwm_target_setup_change = 200;
            }
            else
            {
                pwm_target_speed_value = 0;
                pwm_target_setup_change = 0;
            }
            flg = WriteRegisterValue(RegisterType.Mdu_Control, Register_MDU_Control.MOTOR_SPEED_TARGET_VALUE_REG, (uint)speedTarget) & flg;
            flg = WriteRegisterValue(RegisterType.Mdu_Control, Register_MDU_Control.MOTOR_SPEED_TOLERANCE_REG, (uint)(speedTarget * 5.0 / 100.0)) & flg;
            flg = WriteRegisterValue(RegisterType.Mdu_Control, Register_MDU_Control.PWM_TARGET_SPEED_VALUE_REG, pwm_target_speed_value) & flg;
            flg = WriteRegisterValue(RegisterType.Mdu_Control, Register_MDU_Control.PWM_TARGET_HOLDOFF_REG, 100) & flg;
            flg = WriteRegisterValue(RegisterType.Mdu_Control, Register_MDU_Control.PWM_TARGET_SETUP_CHANGE_REG, pwm_target_setup_change) & flg;

            if (!flg)
            {
                MessageBox.Show("failed in MDU");
            }
            return flg;
        }

        private bool InitADCReceiver()
        {
            var flg = true;
            flg = WriteRegisterValue(RegisterType.ADC_Receiver, Register_ADC_Receiver.ADC_RECV_CONTROL_REG, 0x02) & flg;

            return flg;
        }

        private bool InitADC()
        {
            var flg = true;
            flg = WriteRegisterValue(RegisterType.ADC_SPI, 0, 0x000081) & flg;
            Thread.Sleep(1000);

            flg = WriteRegisterValue(RegisterType.ADC_SPI, 0, 0x057115) & flg;
            Thread.Sleep(1000);

            flg = WriteRegisterValue(RegisterType.ADC_SPI, 0, 0x055000) & flg;
            Thread.Sleep(1000);

            flg = WriteRegisterValue(RegisterType.ADC_SPI, 0, 0x056E00) & flg;
            Thread.Sleep(1000);

            flg = WriteRegisterValue(RegisterType.ADC_SPI, 0, 0x057001) & flg;
            Thread.Sleep(1000);

            flg = WriteRegisterValue(RegisterType.ADC_SPI, 0, 0x058D0F) & flg;
            Thread.Sleep(1000);

            flg = WriteRegisterValue(RegisterType.ADC_SPI, 0, 0x058F0D) & flg;
            Thread.Sleep(1000);

            flg = WriteRegisterValue(RegisterType.ADC_SPI, 0, 0x002509) & flg;
            Thread.Sleep(1000);

            flg = WriteRegisterValue(RegisterType.ADC_SPI, 0, 0x003018) & flg;
            Thread.Sleep(1000);

            flg = WriteRegisterValue(RegisterType.ADC_SPI, 0, 0x05B0FE) & flg;
            Thread.Sleep(1000);

            flg = WriteRegisterValue(RegisterType.ADC_SPI, 0, 0x057114) & flg;
            Thread.Sleep(1000);

            flg = WriteRegisterValue(RegisterType.ADC_SPI, 0, 0x856F00) & flg;
            Thread.Sleep(1000);

            flg = WriteRegisterValue(RegisterType.ADC_SPI, 0, 0x01) & flg;
            Thread.Sleep(1000);

            return flg;
        }

        public ArrayList ReadPDAQ4_Registers(UInt32 registerType, UInt32 offset, UInt32 number = 1)
        {
            return drvagent.ReadRegisterByIC(registerType, offset, number);
        }

        public uint ReadPDAQ4_CARD_RegSpaceBytesLength()
        {
            return drvagent.ReadPDAQ4RegSpaceBytesLength();
        }

        public UInt32[] ReadPDAQ4_CARD_RegSpace(UInt32 start_reg_offset, UInt32 reg_number_to_red)
        {
            return drvagent.ReadPDAQ4RegSpace(start_reg_offset, reg_number_to_red);
        }

        public void WritePDAQ4_CARD_Reg(uint reg_address_byte4_offset, uint value)
        {
            drvagent.WritePDAQ4Reg(reg_address_byte4_offset, value);
        }

        public void WritePDAQ4_CARD_RegBlock(uint StartBARAddress, uint[] dataspace)
        {
            drvagent.WritePDAQ4RegBlock(StartBARAddress, dataspace);
        }

        public void StartDMA()
        {
            // drvagent.PDAQ4Reset();
            DMAInfo dmainfo = drvagent.PDAQ4DMAInit();
            CreateACQChannels(dmainfo);
            //drvagent.EnableDMAReadInt(true);
            //drvagent.EnableDMAWriteInt(true);
            //drvagent.EnableHostLatency(true);
            //drvagent.StartRawRFDMA();
            //drvagent.StartDMA();
            drvagent.MotorStart();
        }

        public void StopDMA()
        {
            connection_rf.stop();
            connection_rt.stop();
            //connection_rf_raw.stop();
            //drvagent.EnableDMAReadInt(false);
            //drvagent.EnableDMAWriteInt(false);
            //drvagent.EnableHostLatency(false);
            drvagent.MotorStop();
            drvagent.PDAQ4DisConnectFIFOChannel(connection_rf);
            drvagent.PDAQ4DisConnectFIFOChannel(connection_rt);
            //drvagent.StopDMA();
            //drvagentdrvagent.StopRawRFDMA();
        }

        public float ReadRegisterInterval
        {
            get
            { return this.read_register_thread_interval; }
            set
            {
                this.read_register_thread_interval = value;
            }
        }
    }

    public class FrameQueue
    {
        public FrameQueue(BinaryWriter wr, int bytes)
        {
            _bytes = bytes;
            _wr = wr;
            _evData = new ManualResetEvent(false);
            _queue = new Queue<byte[]>();
        }

        ~FrameQueue()
        {
            Stop();
            _queue = null;
            _evData.Dispose();
        }

        public void Start()
        {
            _queue.Clear();
            //if (_thread == null)
            //{
            //    _thread = new Thread(new ThreadStart(this.QueueThread));
            //    _thread.SetApartmentState(ApartmentState.MTA);
            //}
            //_thread.Start();

            if (_task == null)
            {
                _cancellationTokenSource = new CancellationTokenSource();

                _task = new Task(QueueThread, _cancellationTokenSource.Token);

                _task.Start();
            }
        }

        public void Stop()
        {
            //if (_thread != null)
            //{
            //    _thread.Abort();
            //    while (_thread.IsAlive) Thread.Sleep(10);
            //    _thread = null;
            //}

            if (_task != null)
            {
                _cancellationTokenSource.Cancel();
                _task = null;
                _cancellationTokenSource = null;
            }
        }

        public void OnFrameData(Frame f)
        {
            byte[] ar = new byte[_bytes];

            Array.Copy(f.FrameBlock, ar, _bytes);

            _queue.Enqueue(ar);
            _evData.Set();
        }

        private Queue<byte[]> _queue;
        private ManualResetEvent _evData;
        private Thread _thread;
        private Task _task;
        private CancellationTokenSource _cancellationTokenSource;
        private int _bytes;
        private BinaryWriter _wr;

        private void QueueThread()
        {
            byte[] f = null;
            try
            {
                while (true)
                {
                    if (_cancellationTokenSource.IsCancellationRequested)
                    {
                        return;
                    }

                    _evData.WaitOne();

                    while (_queue.Count > 0)
                    {
                        f = _queue.Dequeue();

                        _wr.Write(f);
                        f = null;
                        _wr.Flush();
                    }
                    _evData.Reset();
                }
            }
            catch (ThreadAbortException)
            {
                //while (_queue.Count > 0)
                //{
                //    f = _queue.Dequeue();
                //    _wr.Write(f);
                //    f = null;
                //    _wr.Flush();
                //}
                //Thread.ResetAbort();
            }
        }
    }
}