﻿using AcquistionInspector.Models;
using AcquistionInspector.Utils;
using ClosedXML.Excel;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using Microsoft.Win32;

//using NPOI.SS.UserModel;
//using NPOI.XSSF.UserModel;

//using Microsoft.Win32;
//using NPOI.SS.UserModel;
//using NPOI.XSSF.UserModel;
using PDAQ4DriverAgent.PDAQ4Implementation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Input;

namespace AcquistionInspector.ViewModels
{
    public class RegisterListPanelViewModel : ViewModelBase
    {
        private Helper _helper = Helper.GetInstance();

        private PDAQ4Controller _controller = PDAQ4Controller.GetInstance();

        public static List<string> REGISTER_NAME_LIST = new List<string>() {
         "FRAMER_VERSION_REG",
         "FRAMER_STATUS_REG",
         "FRAMER_CONTROL_REG",
         "FRAMER_MEMORY_BASE",
         "FRAMER_MEMORY_LENGTH",
         "FRAMER_MSI_DATA",
         "",
         "",
        };

        #region Field

        private bool _isDecMode = true;
        private PDAQ4Controller controller = PDAQ4Controller.GetInstance();

        private RegisterModel _detailedRegister;

        private RelayCommand _readCustomRegTableCommand;
        private RelayCommand _readCustomRegsCommand;

        private RelayCommand<RegisterModel> _selectWriteRegisterCommand;

        private RelayCommand<RegisterModel> _readRegisterCommand;

        private RelayCommand<RegisterModel> _resetRegisterCommand;

        private RegisterModel _selectToWriteRegister;

        private RelayCommand<string> _writeRegisterCommand;

        private RelayCommand _readAllRegisterCommand;
        private RelayCommand _exportRegisterFileCommand;
        private RelayCommand<RegisterModel> _readRegisterDetailCommand;
        private RelayCommand<RegisterModel> _readRegisterDetailListCommand;

        private bool _writePopIsOpen;

        #endregion Field

        #region Command

        public RelayCommand<RegisterModel> ReadRegisterDetailListCommand
        {
            get => _readRegisterDetailListCommand; set
            {
                _readRegisterDetailListCommand = value;

                RaisePropertyChanged(() => ReadRegisterDetailListCommand);
            }
        }

        public RelayCommand<RegisterModel> ReadRegisterDetailCommand
        {
            get => _readRegisterDetailCommand; set
            {
                _readRegisterDetailCommand = value;

                RaisePropertyChanged(() => ReadRegisterDetailCommand);
            }
        }

        public RelayCommand ExportRegisterFileCommand
        {
            get => _exportRegisterFileCommand; set
            {
                _exportRegisterFileCommand = value;

                RaisePropertyChanged(() => ExportRegisterFileCommand);
            }
        }

        public RelayCommand ReadCustomRegsCommand
        {
            get => _readCustomRegsCommand; set
            {
                _readCustomRegsCommand = value;

                RaisePropertyChanged(() => ReadCustomRegsCommand);
            }
        }

        public RelayCommand ReadCustomRegTableCommand
        {
            get => _readCustomRegTableCommand; set
            {
                _readCustomRegTableCommand = value;

                RaisePropertyChanged(() => ReadCustomRegTableCommand);
            }
        }

        public RelayCommand<RegisterModel> SelectWriteRegisterCommand
        {
            get => _selectWriteRegisterCommand; set
            {
                _selectWriteRegisterCommand = value;

                RaisePropertyChanged(() => SelectWriteRegisterCommand);
            }
        }

        public RelayCommand<RegisterModel> ReadRegisterCommand
        {
            get => _readRegisterCommand; set
            {
                _readRegisterCommand = value;

                RaisePropertyChanged(() => ReadRegisterCommand);
            }
        }

        public RelayCommand<RegisterModel> ResetRegisterCommand
        {
            get => _resetRegisterCommand; set
            {
                _resetRegisterCommand = value;

                RaisePropertyChanged(() => ResetRegisterCommand);
            }
        }

        public RegisterModel SelectToWriteRegister
        {
            get => _selectToWriteRegister;
            set
            {
                _selectToWriteRegister = value;
                RaisePropertyChanged(() => SelectToWriteRegister);
            }
        }

        public RelayCommand<string> WriteRegisterCommand
        {
            get => _writeRegisterCommand; set
            {
                _writeRegisterCommand = value;

                RaisePropertyChanged(() => WriteRegisterCommand);
            }
        }

        public RelayCommand ReadAllRegisterCommand
        {
            get => _readAllRegisterCommand; set
            {
                _readAllRegisterCommand = value;

                RaisePropertyChanged(() => ReadAllRegisterCommand);
            }
        }

        #endregion Command

        public RegisterModel DetailedRegister
        {
            get => _detailedRegister;
            set
            {
                _detailedRegister = value;
                RaisePropertyChanged(() => DetailedRegister);
            }
        }

        public bool WritePopIsOpen
        {
            get => _writePopIsOpen;
            set
            {
                _writePopIsOpen = value;
                RaisePropertyChanged(() => WritePopIsOpen);
            }
        }

        public bool IsDecMode
        {
            get => _isDecMode;
            set
            {
                _isDecMode = value;
                RaisePropertyChanged(() => IsDecMode);
            }
        }

        private void ReadRegisterList(RegisterType type, uint offset, uint number
            )
        {
            ObservableCollection<RegisterModel> registerList = null;
            Type nameType = null;

            switch (type)
            {
                case RegisterType.RfFramer:
                    registerList = RfFramerRegisterList;
                    nameType = typeof(RegisterName_RfRtFramer);
                    break;

                case RegisterType.RtFramer:
                    registerList = RtFramerRegisterList;
                    nameType = typeof(RegisterName_RfRtFramer);
                    break;

                case RegisterType.DMA:
                    break;

                case RegisterType.ConfigurationSpace:
                    registerList = ConfigSpaceRegisterList;
                    break;

                default:
                    throw new ArgumentOutOfRangeException(nameof(type), type, null);
            }

            var list = _controller.DrvAgent.ReadRegisterByIC((uint)type, offset, number);

            uint i = 0;
            registerList.Clear();

            foreach (var item in list)
            {
                if (item is RegisterMd md)
                {
                    registerList.Add(new RegisterModel()
                    {
                        ReadOnly = EnumUtil.GetEnumReadOnly((RegisterName_RfRtFramer)(i * 4)),
                        RegisterType = type,
                        Value = md.value,
                        Address = md.address,
                        Name = Enum.GetName(nameType, i * 4),
                        Offset = i * 4,
                        Description = EnumUtil.GetEnumDescription((RegisterName_RfRtFramer)(i * 4)),
                    });
                }
                i++;
            }
        }

        public ObservableCollection<RegisterModel> RtFramerRegisterList { get; set; } = new ObservableCollection<RegisterModel>();
        public ObservableCollection<CustomRegisterModel> CustomizeRegisterList { get; set; } = new ObservableCollection<CustomRegisterModel>();
        public ObservableCollection<RegisterModel> ConfigSpaceRegisterList { get; set; } = new ObservableCollection<RegisterModel>();
        public ObservableCollection<RegisterModel> RfFramerRegisterList { get; set; } = new ObservableCollection<RegisterModel>();
        public ObservableCollection<RegisterModel> DMARegisterList { get; set; } = new ObservableCollection<RegisterModel>();
        public ObservableCollection<RegisterModel> MDUControlRegisterList { get; set; } = new ObservableCollection<RegisterModel>();
        public ObservableCollection<RegisterModel> TXControlRegisterList { get; set; } = new ObservableCollection<RegisterModel>();
        public ObservableCollection<RegisterModel> RX_AFE_RegisterList { get; set; } = new ObservableCollection<RegisterModel>();
        public ObservableCollection<RegisterModel> ADCRegisterList { get; set; } = new ObservableCollection<RegisterModel>();
        public ObservableCollection<RegisterModel> RF_TXHoldoffRegisterList { get; set; } = new ObservableCollection<RegisterModel>();
        public ObservableCollection<RegisterModel> RT_TXHoldoffRegisterList { get; set; } = new ObservableCollection<RegisterModel>();
        public ObservableCollection<RegisterModel> DSPRegisterList { get; set; } = new ObservableCollection<RegisterModel>();

        public RegisterListPanelViewModel()
        {
            RegisterCommands();
            //var msi_data = controller.DrvAgent.ReadRegisterValue(RegCon.RT_FRAMER_OFFSET + 0x14);
        }

        private void RegisterCommands()
        {
            ReadRegisterDetailListCommand = new RelayCommand<RegisterModel>(reg =>
            {
                if (DetailedRegister == null || DetailedRegister.SubRegisterList == null) return;
                foreach (var reg_ in reg.SubRegisterList)
                {
                    ReadRegisterCommand.Execute(reg_);
                }
            });

            ReadRegisterDetailCommand = new RelayCommand<RegisterModel>(regMd =>
            {
                if (!regMd.IsRegion)
                {
                    regMd.SubRegisterList = null;
                    return;
                }

                if (regMd.SubRegisterList == null)
                {
                    regMd.SubRegisterList = new ObservableCollection<RegisterModel>();
                }

                regMd.SubRegisterList.Clear();

                for (uint i = regMd.Offset; i < regMd.LastEntry; i += 4)
                {
                    var regMd_ = new RegisterModel()
                    {
                        RegisterType = regMd.RegisterType,

                        ReadOnly = regMd.ReadOnly,
                        Offset = regMd.Offset + (i - regMd.Offset),
                        //Description = reg.GetEnumDescription(),
                        //LastEntry = reg.GetEnumLastEntry(),
                    };

                    regMd.SubRegisterList.Add(regMd_);
                }

                DetailedRegister = regMd;

                MessengerInstance.Send("ShowRegisterDetailMsg");
            });

            ExportRegisterFileCommand = new RelayCommand(() =>
            {
                var fileDg = new SaveFileDialog();

                fileDg.FileName = fileDg.FileName + DateTime.Now.ToString("dd-MMM-yyyy", DateTimeFormatInfo.InvariantInfo);
                fileDg.AddExtension = true;
                fileDg.DefaultExt = "xlsx";

                if (fileDg.ShowDialog() == true)
                {
                    var path = fileDg.FileName;

                    using (var workbook = new XLWorkbook())
                    {
                        CreateSheetForRegList(RfFramerRegisterList, workbook, "RF_FRAMER");
                        CreateSheetForRegList(RtFramerRegisterList, workbook, "RT_FRAMER");
                        CreateSheetForRegList(MDUControlRegisterList, workbook, "MDU_Control");
                        CreateSheetForRegList(TXControlRegisterList, workbook, "TX_Control");
                        CreateSheetForRegList(RX_AFE_RegisterList, workbook, "RX_AFE");
                        CreateSheetForRegList(ADCRegisterList, workbook, "ADC_Receiver");
                        CreateSheetForRegList(RF_TXHoldoffRegisterList, workbook, "RF_TXHoldoff");
                        CreateSheetForRegList(RT_TXHoldoffRegisterList, workbook, "RT_TXHoldoff");
                        CreateSheetForRegList(DSPRegisterList, workbook, "DSP");

                        workbook.SaveAs(path);
                    }

                    Process p = new Process();
                    p.StartInfo.FileName = "explorer.exe";
                    p.StartInfo.Arguments = @" /select, " + path;
                    p.Start();
                }
            });

            ReadCustomRegsCommand = new RelayCommand(() =>
            {
                if (CustomizeRegisterList == null || CustomizeRegisterList.Count == 0)
                {
                    return;
                }

                foreach (var item in CustomizeRegisterList)
                {
                    ReadRegisterCommand.Execute(item);
                }
            });

            ReadCustomRegTableCommand = new RelayCommand(ReadCustomRegTableExc);

            ResetRegisterCommand = new RelayCommand<RegisterModel>(ResetRegisterExe);

            ReadAllRegisterCommand = new RelayCommand(() =>
            {
                ReadRegisterList(RegisterType.RfFramer, 0, RegCon.RT_FRAMER_REG_COUNT);
                ReadRegisterList(RegisterType.RtFramer, 0, RegCon.RF_FRAMER_REG_COUNT);

                ReadRegisterList(RegisterType.Mdu_Control, MDUControlRegisterList);
                ReadRegisterList(RegisterType.TX_Control, TXControlRegisterList);
                ReadRegisterList(RegisterType.DSP, DSPRegisterList);
                ReadRegisterList(RegisterType.ADC_Receiver, ADCRegisterList);
                ReadRegisterList(RegisterType.RX_AFE, RX_AFE_RegisterList);
                ReadRegisterList(RegisterType.RF_TXHOLDOFF, RF_TXHoldoffRegisterList);
                ReadRegisterList(RegisterType.RT_TXHOLDOFF, RT_TXHoldoffRegisterList);
                //ReadRegisterList(RegisterType.ConfigurationSpace, 0, RegCon.COF_SPACE_REG_COUNT);
            });

            ReadRegisterCommand = new RelayCommand<RegisterModel>(reg =>
            {
                var list = _controller.DrvAgent.ReadRegisterByIC((uint)reg.RegisterType, (uint)reg.Offset, 1);

                if (list.Count < 1)
                {
                    _helper.Error(string.Format("Read Register:'{0}' failed", reg.Name));
                }
                else
                {
                    var registerInfo = (RegisterMd)list[0];

                    reg.Value = registerInfo.value;
                    reg.Address = registerInfo.address;

                    _helper.Info(string.Format("Read Register:'{0}' succeed,value = 0x{1:X8}", reg.Name, reg.Value));
                    //Message success TODO
                }
            });

            SelectWriteRegisterCommand = new RelayCommand<RegisterModel>(reg =>
            {
                SelectToWriteRegister = reg;
                WritePopIsOpen = true;
            });

            WriteRegisterCommand = new RelayCommand<string>(value =>
            {
                if (SelectToWriteRegister == null)
                {
                    MessageBox.Show("No selected Register,Please try again~", "Error", MessageBoxButton.OK,
                        MessageBoxImage.Error);
                    return;
                }

                bool pass = false;

                uint num = 0;

                NumberStyles numerStyle = IsDecMode ? NumberStyles.None : NumberStyles.AllowHexSpecifier;

                CultureInfo us = CultureInfo.GetCultureInfo("en-US");

                pass = UInt32.TryParse(value.Trim(), numerStyle, us.NumberFormat, out num);

                if (pass)
                {
                    WritePopIsOpen = false;
                    bool success = controller.WriteRegisterValue(SelectToWriteRegister, num);

                    if (success)
                    {
                        ReadRegisterCommand.Execute(SelectToWriteRegister);
                        _helper.Info(string.Format("Register:'{0}'has written value {1:X8}", SelectToWriteRegister.Name, num));
                    }
                    else
                    {
                        _helper.Error(string.Format("Register:'{0}' write value failed", SelectToWriteRegister.Name));
                    }
                }
                else
                {
                    MessageBox.Show("The input content is illegal~", "Error", MessageBoxButton.OK,
                        MessageBoxImage.Error);
                    return;
                }
            });
        }

        private void CreateSheetForRegList(ObservableCollection<RegisterModel> rfFramerRegisterList, XLWorkbook workbook, string sheetName)
        {
            var sheet = workbook.Worksheets.Add(sheetName);
            if (RfFramerRegisterList != null && RfFramerRegisterList.Count != 0)
            {
                var rowNum = 2;
                //var colNum = 1;

                sheet.Cell(1, 1).Value = "Name";
                sheet.Cell(1, 2).Value = "Offset";
                sheet.Cell(1, 3).Value = "Value";

                for (int i = 0; i < RfFramerRegisterList.Count; i++)
                {
                    var regMd = RfFramerRegisterList[i];
                    sheet.Cell(rowNum, 1).Value = regMd.Name;
                    sheet.Cell(rowNum, 2).Value = regMd.Offset;
                    sheet.Cell(rowNum, 3).Value = regMd.Value;

                    rowNum++;
                }
            }
        }

        private void ReadRegisterList(RegisterType registerType, ObservableCollection<RegisterModel> registerList)
        {
            if (registerList == null) return;

            registerList.Clear();

            var mType = registerType.GetEnumMatchType();

            if (mType == null) return;

            var regs = Enum.GetValues(mType);

            foreach (Enum reg in regs)
            {
                var regMd = new RegisterModel()
                {
                    RegisterType = registerType,

                    Name = Enum.GetName(mType, reg),
                    IsRegion = reg.GetEnumIsRegion(),
                    ReadOnly = reg.GetEnumReadOnly(),
                    Offset = Convert.ToUInt32(reg),
                    Description = reg.GetEnumDescription(),
                    LastEntry = reg.GetEnumLastEntry(),
                };

                registerList.Add(regMd);
            }
        }

        private void ReadCustomRegTableExc()
        {
            var fileDg = new OpenFileDialog();

            fileDg.Filter = "xlsx File|*.xlsx";
            if (fileDg.ShowDialog() == true)
            {
                CustomizeRegisterList.Clear();
                DataTable dtTable = new DataTable();
                var filePath = fileDg.FileName;

                var workbook = new XLWorkbook(filePath);

                IXLWorksheet sheet = workbook.Worksheet(1);

                var firstRowNum = sheet.FirstRowUsed().RowNumber() + 1;
                var lastRowNum = sheet.LastRowUsed().RowNumber() + 1;

                for (int i = firstRowNum; i < lastRowNum; i++)
                {
                    var row = sheet.Row(i);
                    if (row == null) continue;

                    if (row.Cells(false).All(d => d.GetString() == string.Empty)) continue;
                    var name = row.Cell(1).GetString();

                    bool valid = false;
                    int bar = 0;

                    valid = int.TryParse(row.Cell(2).GetString(), out bar);
                    if (!valid || (bar != 0 && bar != 2))
                    {
                        _helper.Error("Read excel row " + i + " error, invalid bar");
                        continue;
                    }

                    uint offset = 0;
                    var cell3 = row.Cell(3);
                    valid = uint.TryParse(cell3.CachedValue.ToString(), out offset);
                    if (!valid)
                    {
                        _helper.Error("Read excel row " + i + " error, invalid offset");
                        continue;
                    }

                    var decs = row.Cell(4).GetString();

                    bool writable = true;
                    if (row.Cell(5).GetString() == "N")
                    {
                        writable = false;
                    }

                    CustomRegisterModel regMd = new CustomRegisterModel()
                    {
                        Name = name,
                        RegisterBar = (RegisterBar)bar,
                        Offset = offset,
                        Description = decs,
                        ReadOnly = !writable,
                    };

                    CustomizeRegisterList.Add(regMd);
                }
            }

            if (CustomizeRegisterList == null || CustomizeRegisterList.Count == 0) return;

            foreach (RegisterModel register in CustomizeRegisterList)
            {
                //ReadRegisterCommand.Execute(register);
            }
        }

        private void ResetRegisterExe(RegisterModel reg)
        {
            if (reg.ReadOnly)
            {
                _helper.Info(string.Format("This Register is readonly"));
                return;
            }

            List<UInt32> defaultValues = null;
            UInt32 value = 0;

            bool noValidValue = false;
            switch (reg.RegisterType)
            {
                case RegisterType.RfFramer:

                    defaultValues = EnumUtil.GetEnumDefaultValues((RegisterName_RfRtFramer)reg.Offset);
                    if (defaultValues == null || defaultValues.Count < 1)
                    {
                        noValidValue = true;
                    }
                    else
                    {
                        value = EnumUtil.GetEnumDefaultValues((RegisterName_RfRtFramer)reg.Offset)[0];
                    }
                    break;

                case RegisterType.RtFramer:
                    defaultValues = EnumUtil.GetEnumDefaultValues((RegisterName_RfRtFramer)reg.Offset);
                    if (defaultValues == null || defaultValues.Count < 2)
                    {
                        noValidValue = true;
                    }
                    else
                    {
                        value = EnumUtil.GetEnumDefaultValues((RegisterName_RfRtFramer)reg.Offset)[1];
                    }
                    break;

                case RegisterType.DMA:
                    return;
                    break;

                default:
                    break;
            }

            if (noValidValue)
            {
                MessageBox.Show("No default value for this register now", "Error", MessageBoxButton.OK,
                        MessageBoxImage.Error);

                return;
                //_helper.Error(string.Format("Register:'{0}'no default value for this register now", SelectToWriteRegister.Name));
            }

            bool success = controller.WriteRegisterValue(reg, value);

            if (success)
            {
                _helper.Info(string.Format("Register:'{0}'reset to value {1:X8} succeed", SelectToWriteRegister.Name, value));
            }
            else
            {
                _helper.Error(string.Format("Register:'{0}' reset value failed", SelectToWriteRegister.Name));
            }
        }
    }
}