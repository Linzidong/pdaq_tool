﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using Microsoft.Win32;
using PDAQ4DriverAgent.PDAQ4Implementation;
using PDAQ4DriverInterface;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Threading;

namespace AcquistionInspector.ViewModels
{
    public class ImageLoaderViewModel : ViewModelBase
    {
        #region

        /// <summary>
        /// This constant defines the maximum number of frames recorded
        /// when recording a run.
        /// </summary>
        public const int MaxRecordedFrames = 18000 + (2 * PadFrameCount);

        /// <summary>
        /// This constant defines the number of frames at the beginning and end of the
        /// run that are stored but not displayed in order for DDP and Trace Assist to
        /// be performed on all frames that are shown
        /// </summary>
        public const int PadFrameCount = 3;

        public const int MaxFrameTimeCount = 65536;

        #endregion

        private static long RF_FRAME_SIZE = 96 + 256 * (16 + 1024 * 2);
        private static long RT_FRAME_SIZE = 96 + 256 * (16 + 256 * 1);
        private static long FRAME_HEADER_SIZE = 96;
        private static long RF_FRAME_CONTENT_SIZE = RF_FRAME_SIZE - FRAME_HEADER_SIZE;
        private static long RT_FRAME_CONTENT_SIZE = RT_FRAME_SIZE - FRAME_HEADER_SIZE;

        private static int __vectorperframe = 256;
        private static int __samplepervector = 256;
        private ObservableCollection<Frame> _frameList;

        private DispatcherTimer _dsTimer = new DispatcherTimer();

        public ObservableCollection<Frame> FrameList
        {
            get { return _frameList; }
            set
            {
                _frameList = value;
                RaisePropertyChanged(() => FrameList);
            }
        }

        private int _frameLength;

        public int FrameLength
        {
            get { return _frameLength; }
            set
            {
                _frameLength = value;
                RaisePropertyChanged(() => FrameLength);
            }
        }

        public int FrameIndex0
        {
            get => FrameIndex + 1;
        }

        private int _frameIndex;

        public int FrameIndex
        {
            get { return _frameIndex; }
            set
            {
                if (value < 0 || value > FrameLength - 1) return;
                _frameIndex = value;
                RaisePropertyChanged(() => FrameIndex);
                RaisePropertyChanged(() => FrameIndex0);
                DisplayFrame(value);
            }
        }

        private WriteableBitmap _frameBitmap;

        public WriteableBitmap FrameBitmap
        {
            get { return _frameBitmap; }
            set
            {
                _frameBitmap = value;
                RaisePropertyChanged(() => FrameBitmap);
            }
        }

        private ChannelHeader Channelheader { get; set; }

        private RelayCommand<int> _playFramesCommand;

        public RelayCommand<int> PlayFramesCommand
        {
            get { return _playFramesCommand; }
            set
            {
                _playFramesCommand = value;
                RaisePropertyChanged(() => PlayFramesCommand);
            }
        }

        //private RelayCommand _pauseFramesCommand;

        //public RelayCommand PauseFramesCommand
        //{
        //    get { return _pauseFramesCommand; }
        //    set
        //    {
        //        _pauseFramesCommand = value;
        //        RaisePropertyChanged(() => PauseFramesCommand);
        //    }
        //}

        //private RelayCommand _pauseFramesCommand;

        //public RelayCommand PauseFramesCommand
        //{
        //    get { return _pauseFramesCommand; }
        //    set
        //    {
        //        _pauseFramesCommand = value;
        //        RaisePropertyChanged(() => PauseFramesCommand);
        //    }
        //}

        private RelayCommand _backFrameIndexCommand;

        public RelayCommand BackFrameCommand
        {
            get { return _backFrameIndexCommand; }
            set
            {
                _backFrameIndexCommand = value;
                RaisePropertyChanged(() => BackFrameCommand);
            }
        }

        private RelayCommand _nextFrameIndexCommand;

        public RelayCommand NextFrameIndexCommand
        {
            get { return _nextFrameIndexCommand; }
            set
            {
                _nextFrameIndexCommand = value;
                RaisePropertyChanged(() => NextFrameIndexCommand);
            }
        }

        private RelayCommand _saveToBMPCommand;

        public RelayCommand SaveToBMPCommand
        {
            get { return _saveToBMPCommand; }
            set
            {
                _saveToBMPCommand = value;
                RaisePropertyChanged(() => SelectFrameFileCommand);
            }
        }

        private RelayCommand _addHeaderCommand;

        public RelayCommand AddHeaderCommand
        {
            get { return _addHeaderCommand; }
            set
            {
                _addHeaderCommand = value;
                RaisePropertyChanged(() => AddHeaderCommand);
            }
        }

        private RelayCommand<int> _selectFrameFileCommand;

        public RelayCommand<int> SelectFrameFileCommand
        {
            get { return _selectFrameFileCommand; }
            set
            {
                _selectFrameFileCommand = value;
                RaisePropertyChanged(() => SelectFrameFileCommand);
            }
        }

        public ImageLoaderViewModel()
        {
            FrameList = new ObservableCollection<Frame>();
            FrameBitmap = new WriteableBitmap(256, 256, 96, 96, PixelFormats.Gray8, null);

            InitCommands();

            InitObjects();
        }

        private void InitObjects()
        {
            _dsTimer = new DispatcherTimer();
            _dsTimer.Interval = new TimeSpan(0, 0, 0, 0, 33);
            _dsTimer.Tick += _dsTimer_Tick;
        }

        private void _dsTimer_Tick(object sender, EventArgs e)
        {
            if (FrameLength == 0 || FrameIndex0 == FrameLength)
            {
                _dsTimer.Stop();
                return;
            }

            FrameIndex++;
            //DisplayFrame(FrameIndex);
        }

        private void InitCommands()
        {
            AddHeaderCommand = new RelayCommand(() =>
            {
                CreateRTFileHeader();
            });

            PlayFramesCommand = new RelayCommand<int>(action =>
            {
                if (action == 0)
                {
                    if (FrameIndex0 == FrameLength)
                    {
                        FrameIndex = 0;
                    }
                    _dsTimer.Start();
                }
                else if (action == 1)
                {
                    _dsTimer.Stop();
                }
                else if (action == 2)
                {
                    _dsTimer.Stop();
                    FrameIndex = 0;
                }
            });

            SelectFrameFileCommand = new RelayCommand<int>(SelectFrameFileExe);
            BackFrameCommand = new RelayCommand(() =>
            {
                FrameIndex -= 1;
                //DisplayFrame(FrameIndex - 1);
            });

            NextFrameIndexCommand = new RelayCommand(() =>
            {
                FrameIndex += 1;
                //DisplayFrame(FrameIndex + 1);
            });

            SaveToBMPCommand = new RelayCommand(SaveToBmpExe);
        }

        private void SaveToBmpExe()
        {
            BmpBitmapEncoder bpe = new BmpBitmapEncoder();
            var folder = AppDomain.CurrentDomain.BaseDirectory + "SavedImage\\";
            if (!Directory.Exists(folder))
            {
                Directory.CreateDirectory(folder);
            }
            string path = folder + DateTime.Now.ToString("yyyyMMddfff") + ".bmp";
            bpe.Frames.Add(BitmapFrame.Create(FrameBitmap));
            using (Stream stream = File.Create(path))
            {
                bpe.Save(stream);
            }
        }

        private void SelectFrameFileExe(int type)
        {
            long frameSize = 0;

            OpenFileDialog op = new OpenFileDialog();

            op.Filter = "Frame File|*.bin";

            string filePath = string.Empty;

            if (op.ShowDialog() == true)
            {
                filePath = op.FileName;
            }
            else
            {
            }

            if (string.IsNullOrEmpty(filePath))
            {
                return;
            }

            if (type == 0)
            {
                frameSize = RF_FRAME_CONTENT_SIZE;
                Channelheader = new ChannelHeader();
                Channelheader.ChannelIndex = 0;
                Channelheader.ChannelType = ChannelType.RF_CHANNEL;
                Channelheader.FrameHdrSize = 96; // TODO - use type size
                Channelheader.FrameRate = 30;
                Channelheader.VectorHdrSize = 16;
                Channelheader.VectorsPerFrame = 256;
                Channelheader.SamplesPerVector = 1024;
                Channelheader.BitsPerSample = 16;
                Channelheader.SampleMask = 0x0fff;
                Channelheader.SwVersion = 0x0300;
            }
            else if (type == 1)
            {
                frameSize = RT_FRAME_SIZE;
                Channelheader = new ChannelHeader();
                Channelheader.ChannelType = ChannelType.RT_CHANNEL;
                Channelheader.ChannelIndex = 1;
                Channelheader.FrameHdrSize = 96; // TODO - use type size
                Channelheader.FrameRate = 30;
                Channelheader.VectorHdrSize = 16;
                Channelheader.VectorsPerFrame = 256;
                Channelheader.SamplesPerVector = 256;
                Channelheader.BitsPerSample = 8;
            }

            using (FileStream fs = new FileStream(filePath, FileMode.Open))
            {
                var bta = new byte[fs.Length];
                fs.Read(bta, 0, bta.Length);

                FrameList.Clear();

                //var connection_rf = new Frame_Connection(rf_channelheader, fifos[0]);
                long i = 0;
                while (bta.Length - i >= frameSize)
                {
                    Frame frame = new Frame(Channelheader);
                    Array.Copy(bta, i, frame.FrameBlock, 0, frameSize);
                    FrameList.Add(frame);
                    i += frameSize;
                }

                //for (long i = 0; i < bta.Length; i++)
                //{
                //    Frame frame = new Frame(Channelheader);
                //    //i += FRAME_HEADER_SIZE;

                //    if (bta.Length - i < frameSize)
                //    {
                //        break;
                //    }
                //    Array.Copy(bta, i, frame.FrameBlock, 0, frameSize);
                //    FrameList.Add(frame);
                //    i += frameSize;
                //}
            }

            FrameIndex = 0;
            FrameLength = FrameList.Count;
            DisplayFrame(FrameIndex);
        }

        public void CreateRTFileHeader()
        {
            var fileDiag = new OpenFileDialog();

            FileStream readFs = null;
            byte[] binArry = null;

            var frameCount = 0;

            if (fileDiag.ShowDialog() == true)
            {
                readFs = new FileStream(fileDiag.FileName, FileMode.Open);

                binArry = new byte[readFs.Length];

                readFs.Read(binArry, 0, binArry.Length);

                frameCount = binArry.Length / 0x11060;
            }
            else
            {
                return;
            }

            Channelheader = new ChannelHeader();
            Channelheader.ChannelType = ChannelType.RT_CHANNEL;
            Channelheader.ChannelIndex = 1;
            Channelheader.FrameHdrSize = 96; // TODO - use type size
            Channelheader.FrameRate = 30;
            Channelheader.VectorHdrSize = 16;
            Channelheader.VectorsPerFrame = 256;
            Channelheader.SamplesPerVector = 256;
            Channelheader.BitsPerSample = 8;

            var _caseId = Guid.NewGuid();
            var _runId = Guid.NewGuid();

            var rtPath = AppDomain.CurrentDomain.BaseDirectory + "RTFiles\\";

            if (!Directory.Exists(rtPath))
            {
                Directory.CreateDirectory(rtPath);
            }
            var filePath = rtPath + _runId + ".RT";

            var stream = new FileStream(filePath, FileMode.OpenOrCreate, FileAccess.ReadWrite);

            var headerLength = TotalHeaderLength();

            var fileHeaderData = new byte[headerLength];

            long capacity = (Channelheader.CalculateFrameSize() * frameCount) + headerLength;

            capacity += (MaxFrameTimeCount * sizeof(ulong))
                        + (MaxFrameTimeCount * sizeof(ulong));

            stream.SetLength(capacity);

            using (var saveStream = new MemoryStream(fileHeaderData))
            {
                saveStream.Write(BitConverter.GetBytes(headerLength), 0, sizeof(int));
                saveStream.Write(BitConverter.GetBytes('3'), 0, sizeof(char));

                var guidString = _caseId.ToString();
                var guidBytes = Encoding.ASCII.GetBytes(guidString);
                saveStream.Write(guidBytes, 0, guidBytes.Length);
                guidString = _runId.ToString();
                guidBytes = Encoding.ASCII.GetBytes(guidString);
                saveStream.Write(guidBytes, 0, guidBytes.Length);
                saveStream.Write(BitConverter.GetBytes(96), 0, sizeof(int));
                saveStream.Write(BitConverter.GetBytes(RT_FRAME_SIZE), 0, sizeof(int));

                if (Channelheader != null)
                {
                    Channelheader.Serialize(saveStream);
                }
            }

            stream.Write(fileHeaderData, 0, fileHeaderData.Length);

            var frameTimes = new ulong[MaxFrameTimeCount];
            var frameNumbers = new ulong[MaxFrameTimeCount];

            for (ulong i = 0; i < (ulong)frameCount; i++)
            {
                if (i == 0)
                {
                    frameTimes[i] = 0;
                }
                else if (i == 1)
                {
                    frameTimes[i] = 0;
                }
                else
                {
                    frameTimes[i] = (i - 1) * 3300000;
                }

                frameNumbers[i] = i;
            }

            StoreFrameTimes(stream, frameTimes);
            StoreFrameNumbers(stream, frameNumbers);

            stream.Write(binArry, 0, binArry.Length);

            stream.Flush();
            stream.Close();
        }

        protected int TotalHeaderLength()
        {
            var totalLength = CalculateFileHeaderLength();
            totalLength += 41;
            if (totalLength % 2 == 1)
            {
                totalLength += 1;
            }

            return totalLength;
        }

        public void StoreFrameNumbers(Stream saveStream, ulong[] frameNumbers)
        {
            // Skip the header and the frame times.
            var headerLength = TotalHeaderLength();
            saveStream.Seek(
                             headerLength + (MaxFrameTimeCount * sizeof(ulong)),
                             SeekOrigin.Begin);

            var writeBuffer = new byte[frameNumbers.Length * sizeof(ulong)];
            using (var numberStream = new MemoryStream(writeBuffer))
            {
                foreach (var t in frameNumbers)
                {
                    numberStream.Write(BitConverter.GetBytes(t), 0, sizeof(ulong));
                }
            }

            saveStream.Write(writeBuffer, 0, writeBuffer.Length);
        }

        public void StoreFrameTimes(Stream saveStream, ulong[] frameTimes)
        {
            // Skip just the header.
            var headerLength = TotalHeaderLength();
            saveStream.Seek(headerLength, SeekOrigin.Begin);

            var writeBuffer = new byte[frameTimes.Length * sizeof(ulong)];
            using (var timeStream = new MemoryStream(writeBuffer))
            {
                foreach (var t in frameTimes)
                {
                    timeStream.Write(BitConverter.GetBytes(t), 0, sizeof(ulong));
                }
            }

            saveStream.Write(writeBuffer, 0, writeBuffer.Length);
        }

        /// <summary>
        /// Calculate the size of the header data for the file
        /// </summary>
        /// <returns>
        /// byte count for the file header
        /// </returns>
        protected static int CalculateFileHeaderLength()
        {
            var idLength = new Guid().ToString().Length;

            var headerLength = sizeof(int); // header length
            headerLength += sizeof(char);   // version id
            headerLength += idLength * 2; // two GUIDs as strings
            headerLength += sizeof(int); // frame header length
            headerLength += sizeof(int); // frame length
            return headerLength;
        }

        public void Imaging(PDAQ4DriverInterface.Frame f)
        {
            double time = 1.0e-5 * (f.HwFrameTime - f.PrevHwFrameTime);

            byte[] data = new byte[__vectorperframe * __samplepervector];
            for (int i = 0; i < __vectorperframe; i++)
            {
                PDAQ4DriverInterface.Vector v = (PDAQ4DriverInterface.Vector)f.GetVector(i);

                for (int j = 0; j < __samplepervector; j++)
                {
                    data[i * __samplepervector + j] = v.GetSample((j) % __samplepervector);
                }
            }
            try
            {
                DrawImage(data);
            }
            catch
            {
            }
        }

        public void DrawImage(byte[] data)
        {
            Marshal.Copy(data, 0, FrameBitmap.BackBuffer, __vectorperframe * __samplepervector);

            try
            {
                FrameBitmap.Lock();
                FrameBitmap.AddDirtyRect(new Int32Rect(0, 0, __samplepervector, __vectorperframe));
            }
            finally
            {
                FrameBitmap.Unlock();
            }
        }

        private void DisplayFrame(int frameIndex)
        {
            if (FrameList == null || frameIndex < 0 || FrameList.Count - 1 < frameIndex)
            {
                return;
            }

            var frame = FrameList[frameIndex];

            Imaging(frame);
            //FrameIndex = frameIndex;
        }
    }
}