﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AcquistionInspector.ViewModels
{
    public static class VMProvider
    {
        public static MainWindowViewModel MainWindowViewModel { get; } = new MainWindowViewModel();

        public static RegisterListPanelViewModel RegisterListPanelViewModel { get; } = new RegisterListPanelViewModel();
    }
}