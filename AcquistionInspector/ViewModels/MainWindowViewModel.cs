﻿using AcquistionInspector.Models;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using System;
using System.Threading.Tasks;

namespace AcquistionInspector.ViewModels
{
    public class MainWindowViewModel : ViewModelBase
    {
        public static bool DoShot { get; set; }

        private bool _initing;

        public bool Initing
        {
            get => _initing;
            set
            {
                _initing = value;
                RaisePropertyChanged(() => Initing);
            }
        }

        private string _initString;

        public string InitString
        {
            get => _initString;
            set
            {
                _initString = value;
                RaisePropertyChanged(() => InitString);
            }
        }

        private PDAQ4Controller _controller = PDAQ4Controller.GetInstance();

        private RelayCommand _imageShotCommand;

        public RelayCommand ImageShotCommand
        {
            get => _imageShotCommand; set
            {
                _imageShotCommand = value;

                RaisePropertyChanged(() => ImageShotCommand);
            }
        }

        private RelayCommand _initFPGACommand;

        public RelayCommand InitFPGACommand
        {
            get => _initFPGACommand; set
            {
                _initFPGACommand = value;

                RaisePropertyChanged(() => InitFPGACommand);
            }
        }

        private RelayCommand<bool> _startMDUCommand;

        public RelayCommand<bool> StartMDUCommand
        {
            get => _startMDUCommand; set
            {
                _startMDUCommand = value;

                RaisePropertyChanged(() => StartMDUCommand);
            }
        }

        public MainWindowViewModel()
        {
            InitCommands();
        }

        private void InitCommands()
        {
            InitFPGACommand = new RelayCommand(() =>
            {
                var strAct = new Action<string>(str =>
                {
                    InitString = str;
                });

                var iniAct = new Action<bool>(flg =>
                {
                    Initing = flg;
                });
                Task.Factory.StartNew(() =>
                {
                    _controller.InitFPGA_InPDAQTool(strAct, iniAct);
                });
                //_controller.InitFPGA();
            });
            //ImageShotCommand = new RelayCommand(ImageShot);
            StartMDUCommand = new RelayCommand<bool>((flg) =>
            {
                //var mdu_control_reg = new RegisterModel()
                //{
                //    RegisterType = RegisterType.Mdu_Control,
                //    Offset = (uint)Register_MDU_Control.MDU_CONTROL_REG,
                //};
                if (flg)
                {
                    _controller.WriteRegisterValue(RegisterType.Mdu_Control, Register_MDU_Control.MDU_CONTROL_REG, 0x03);
                }
                else
                {
                    _controller.WriteRegisterValue(RegisterType.Mdu_Control, Register_MDU_Control.MDU_CONTROL_REG, 0x00);
                }
                //_controller.WriteRegisterValue()
            });
        }

        private void InitFPGA()
        {
            throw new NotImplementedException();
        }

        private void ImageShot()
        {
            DoShot = true;
        }
    }
}