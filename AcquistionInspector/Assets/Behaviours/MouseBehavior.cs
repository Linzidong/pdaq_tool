﻿using GalaSoft.MvvmLight.Command;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Interactivity;

namespace AcquistionInspector.Assets.Behaviours
{
    public class MouseBehavior : Behavior<FrameworkElement>
    {
        public RelayCommand MouseLefButtonDownCmd
        {
            get { return (RelayCommand)GetValue(MouseLefButtonDownCmdProperty); }
            set { SetValue(MouseLefButtonDownCmdProperty, value); }
        }

        // Using a DependencyProperty as the backing store for MyProperty.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty MouseLefButtonDownCmdProperty =
            DependencyProperty.Register("MouseLefButtonDownCmd", typeof(RelayCommand), typeof(MouseBehavior), new PropertyMetadata(null));

        protected override void OnAttached()
        {
            base.OnAttached();

            if (MouseLefButtonDownCmd != null)
            {
                AssociatedObject.MouseLeftButtonDown += (sender, args) =>
                {
                    MouseLefButtonDownCmd.Execute(null);
                };
            }
        }

        protected override void OnDetaching()
        {
            base.OnDetaching();
        }
    }
}