﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.IO;
using System.Data;
using PDAQ4DriverInterface;
using System.Windows.Controls.DataVisualization.Charting;


namespace AcquistionInspector
{

    /// <summary>
    /// Interaction logic for FrameViewer.xaml
    /// </summary>
    /// 
    public partial class FrameViewer : Window
    {
        private BinaryReader _reader;
        private IvusFileHeader _hdr;
        private byte[] _frame;
        private int frame_index = 0;
        private int vector_index = 0;
        private int nframes = 0;
        FileStream _stream;
        public FrameViewer()
        {
            InitializeComponent();
        }
        public void LoadArchive(string archivefile)
        {
            byte []hr = new byte[14];
            _stream = File.Open(archivefile, FileMode.Open, System.IO.FileAccess.Read);
            _reader = new BinaryReader(_stream);
            _reader.Read(hr, 0, 14);
            
            _hdr = new IvusFileHeader(hr);
            nframes = _hdr.Frames;
            m_frameindex.Text = Convert.ToString(frame_index);
            imagepanel.OnRTFrameSizeChanged(_hdr.VectorPerFrame, _hdr.SamplePerVector);
            DumpFrame(0);

        }
        private void DumpFrame(int index)
        {

            m_frameindex.Text = Convert.ToString(index);
            _frame = null;
            _reader.BaseStream.Position = 14 + index * _hdr.FrameSize;

            _frame = new byte[_hdr.FrameSize];
            _reader.Read(_frame, 0, _hdr.FrameSize);

            string tgc = "";

            for (int i = 0; i < 17; i++)
                tgc += _frame[56 + i].ToString("X2") + " ";

            List<TableItem> ds = new List<TableItem>{
														new TableItem("Sync Pattern", BitConverter.ToUInt32(_frame, 0).ToString("X")),
														new TableItem("ChannelIndex", _frame[4].ToString()),
                                                        new TableItem("ChannelType",  _frame[5].ToString()),
                                                        new TableItem("Vectors/Frame", BitConverter.ToUInt16(_frame,2).ToString()),
 														new TableItem("Samples/Vector",  BitConverter.ToUInt32(_frame, 8).ToString()),
														new TableItem("Frame Counter",  BitConverter.ToUInt32(_frame,12).ToString()),
														new TableItem("Frame Time",  BitConverter.ToInt64(_frame,16).ToString()),
														new TableItem("Prev Frame Time",  BitConverter.ToInt64(_frame,24).ToString()),
                                                        new TableItem("Frame Rate", Convert.ToString(1.0e8 / (BitConverter.ToInt64(_frame,16) - BitConverter.ToInt64(_frame,24)))),
                                                        new TableItem("Prev Frame Counter",  BitConverter.ToUInt32(_frame,32).ToString()),
														new TableItem("Frame Size",  BitConverter.ToUInt32(_frame, 48).ToString()),
														new TableItem("SampleSpacing",  BitConverter.ToInt32(_frame, 40).ToString()),
														new TableItem("TGC", tgc)
													};

            m_frameheader.ItemsSource = ds;
           

            if (!_hdr.HeaderOnly && (_hdr.Type == ChannelType.RT_CHANNEL))
            {
                byte[] data = new byte[_hdr.VectorPerFrame* _hdr.SamplePerVector];
                int n = 96 + 16;
                for (int i = 0; i < _hdr.VectorPerFrame; i++)
                {
                    for (int j = 0; j < _hdr.SamplePerVector; j++)
                    {
                        data[i * _hdr.SamplePerVector + j] = _frame[n + j];
                    }
                    n += (_hdr.SamplePerVector + 16);
                }
                imagepanel.DrawImage(data);
                data = null;
            }
            DumpVector(0);
        }
        private void DumpVector(int index)
        {
            if (_hdr.HeaderOnly)
                return;

            m_vectorindex.Text = Convert.ToString(index);
            int pos;
            byte[] vData;
            if (_hdr.Type == ChannelType.RT_CHANNEL) // ENV
            {
                pos = 96 + index * (16 + _hdr.SamplePerVector);   // byte sample, f_header + 256 * (vhdr + 256 * sizeof(byte)) 
                vData = new byte[_hdr.SamplePerVector];
                Array.Copy(_frame, pos + 16, vData, 0, _hdr.SamplePerVector);
            }
            else
            {
                pos = 96 + index * (16 + 2 * _hdr.SamplePerVector);  // short sample,  f_header + 256 * (vhdr + 1024 * sizeof(short)) 
                vData = new byte[2 * _hdr.SamplePerVector];
                Array.Copy(_frame, pos + 16, vData, 0, 2 * _hdr.SamplePerVector);
            }

            List<TableItem> ds = new List<TableItem> {  new TableItem("Index", BitConverter.ToUInt16(_frame, pos).ToString()),
														new TableItem("Angle", BitConverter.ToUInt16(_frame, pos + 2).ToString()),
                                                        new TableItem("Flags", BitConverter.ToUInt32(_frame, pos + 4).ToString()),
 														new TableItem("Time", BitConverter.ToInt64(_frame, pos + 8).ToString()),
                                                     };
            this.liBoxVectorHeader.ItemsSource = ds;

            // hex display
            richTextBox1.Clear();

            string s = HexUtils.HexDump(vData, 32);
           richTextBox1.AppendText(s);
           s = null;
           //ploting
           VectorChart.Series.Clear();
           var lines = new LineSeries();
           lines.Background = Brushes.White;
           Style datapointStyle = new Style(typeof(LineDataPoint));
           datapointStyle.Setters.Add(new Setter(LineDataPoint.WidthProperty, .5));
           datapointStyle.Setters.Add(new Setter(LineDataPoint.HeightProperty, .5));
           datapointStyle.Setters.Add(new Setter(LineDataPoint.BackgroundProperty, new SolidColorBrush(Colors.Blue)));
           lines.DataPointStyle = datapointStyle;

           lines.IndependentValuePath= "X";
           lines.DependentValuePath="Y";
           lines.IsSelectionEnabled = false;
           lines.Name = "Vector" + index.ToString();
            if (_hdr.Type == ChannelType.RT_CHANNEL && (bool)isshowchart.IsChecked) // ENV
            {
                List<Point> listofpoints = new List<Point>();
                for (int i = 0; i < vData.Length; i++)
                {
                    listofpoints.Add(new Point(i, vData[i]));
                }
                lines.ItemsSource = listofpoints;
                VectorChart.Series.Add(lines);
            }
                /*
            else
            {
                short[] sData = new short[1024];
                Buffer.BlockCopy(vData, 0, sData, 0, 2048);
                //vData = null;
                List<Point> listofpoints = new List<Point>();
                for (int i = 0; i < sData.Length; i++)
                {
                    listofpoints.Add(new Point(i, sData[i]));
                } 
                lines.ItemsSource = listofpoints;
            }
                 * */
           
         
        }

        private void FramePrev_Click(object sender, RoutedEventArgs e)
        {
            if (frame_index > 0)
            {
                frame_index--;
                this.DumpFrame(frame_index);
            }

        }

        private void FrameNext_Click(object sender, RoutedEventArgs e)
        {
            if(frame_index < nframes-1)
            {
                frame_index++;
                this.DumpFrame(frame_index);
            }

        }

        private void VectorPrev_Click(object sender, RoutedEventArgs e)
        {
            if (vector_index > 0)
            {
                vector_index--;
                this.DumpVector(vector_index);
            }
        }

        private void VectorNext_Click(object sender, RoutedEventArgs e)
        {
            if (vector_index <  _hdr.VectorPerFrame)
            {
                vector_index++;
                this.DumpVector(vector_index);
            }

        }

        private void Window_Closed(object sender, EventArgs e)
        {
            _stream.Close();
        }

        private void m_frameindex_TextChanged(object sender, TextChangedEventArgs e)
        {
            try
            {
                frame_index = (int)Convert.ToUInt32(m_frameindex.Text);
                if (frame_index >= nframes) frame_index = nframes - 1;
                this.DumpFrame(frame_index);
            }
            catch(Exception es)
            {

            }

        }

        private void m_vectorindex_TextChanged(object sender, TextChangedEventArgs e)
        {
            try
            { 
                vector_index = (int)Convert.ToUInt32(m_vectorindex.Text);
                if (vector_index >= _hdr.VectorPerFrame) vector_index = _hdr.VectorPerFrame-1;
             this.DumpVector(vector_index);
            }
            catch (Exception)
            {

            }
        }

        private void liBoxVectorHeader_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void richTextBox1_TextChanged(object sender, TextChangedEventArgs e)
        {

        }
    }
    public class DataArray
    {
        public static DataSet ToDataSet(object[] sourceArray)
        {
            Type baseType = sourceArray.GetType().GetElementType();

            DataColumn dc = new DataColumn();
            dc.DataType = baseType;

            DataTable dt = new DataTable();
            dt.Columns.Add(dc);

            for (int currentIndex = 0; currentIndex < sourceArray.Length; currentIndex++)
                dt.Rows.Add(new object[] { sourceArray[currentIndex] });

            DataSet ds = new DataSet();
            ds.Tables.Add(dt);
            return ds;

        }

        public static DataSet ToDataSet(object[,] sourceArray)
        {
            Type baseType = sourceArray.GetType().GetElementType();
            int rowCount = sourceArray.GetLength(0);
            int columnCount = sourceArray.GetLength(1);

            DataTable dt = new DataTable();
            DataColumn[] dca = new DataColumn[columnCount];

            for (int currentColumnIndex = 0; currentColumnIndex < columnCount; currentColumnIndex++)
            {
                dca[currentColumnIndex] = new DataColumn();
                dca[currentColumnIndex].DataType = baseType;
            }
            dt.Columns.AddRange(dca);

            for (int currentRowIndex = 0; currentRowIndex < rowCount; currentRowIndex++)
            {
                object[] rowArr = new object[columnCount];
                for (int currentColumnIndex = 0; currentColumnIndex < columnCount; currentColumnIndex++)
                    rowArr[currentColumnIndex] = sourceArray.GetValue(currentRowIndex, currentColumnIndex);
                dt.Rows.Add(rowArr);
            }

            DataSet ds = new DataSet();
            ds.Tables.Add(dt);
            return ds;

        }

        public static DataSet ToDataSet(object[, ,] sourceArray)
        {
            Type baseType = sourceArray.GetType().GetElementType();
            int tableCount = sourceArray.GetLength(0);
            int rowCount = sourceArray.GetLength(1);
            int columnCount = sourceArray.GetLength(2);
            DataSet ds = new DataSet();

            for (int currentTableIndex = 0; currentTableIndex < tableCount; currentTableIndex++)
            {
                DataTable dt = new DataTable();
                DataColumn[] dca = new DataColumn[columnCount];

                for (int currentColumnIndex = 0; currentColumnIndex < columnCount; currentColumnIndex++)
                {
                    dca[currentColumnIndex] = new DataColumn();
                    dca[currentColumnIndex].DataType = baseType;
                }
                dt.Columns.AddRange(dca);

                for (int currentRowIndex = 0; currentRowIndex < rowCount; currentRowIndex++)
                {
                    object[] rowArr = new object[columnCount];
                    for (int currentColumnIndex = 0; currentColumnIndex < columnCount; currentColumnIndex++)
                        rowArr[currentColumnIndex] = sourceArray.GetValue(currentTableIndex, currentRowIndex, currentColumnIndex);
                    dt.Rows.Add(rowArr);
                }
                ds.Tables.Add(dt);
            }
            return ds;

        }
    }
    public class HexUtils
    {
        public static string HexDump(byte[] bytes, int bytesPerLine = 16)
        {
            if (bytes == null) return "<null>";
            int bytesLength = bytes.Length;

            char[] HexChars = "0123456789ABCDEF".ToCharArray();

            int firstHexColumn =
                  8                   // 8 characters for the address
                + 8;                  // 3 spaces

            int firstCharColumn = firstHexColumn
                + bytesPerLine * 3       // - 2 digit for the hexadecimal value and 1 space
                + (bytesPerLine - 1) / 8 // - 1 extra space every 8 characters from the 9th
                + 2;                  // 2 spaces 

            int lineLength = firstCharColumn
                + bytesPerLine           // - characters to show the ascii value
                + Environment.NewLine.Length; // Carriage return and line feed (should normally be 2)

            char[] line = (new String(' ', lineLength - 2) + Environment.NewLine).ToCharArray();
            int expectedLines = (bytesLength + bytesPerLine - 1) / bytesPerLine;
            StringBuilder result = new StringBuilder(expectedLines * lineLength);

            for (int i = 0; i < bytesLength; i += bytesPerLine)
            {
                line[0] = HexChars[(i >> 28) & 0xF];
                line[1] = HexChars[(i >> 24) & 0xF];
                line[2] = HexChars[(i >> 20) & 0xF];
                line[3] = HexChars[(i >> 16) & 0xF];
                line[4] = HexChars[(i >> 12) & 0xF];
                line[5] = HexChars[(i >> 8) & 0xF];
                line[6] = HexChars[(i >> 4) & 0xF];
                line[7] = HexChars[(i >> 0) & 0xF];

                int hexColumn = firstHexColumn;
                int charColumn = firstCharColumn;

                for (int j = 0; j < bytesPerLine; j++)
                {
                    if (j > 0 && (j & 7) == 0) hexColumn++;
                    if (i + j >= bytesLength)
                    {
                        line[hexColumn] = ' ';
                        line[hexColumn + 1] = ' ';
                        //line[charColumn] = ' ';
                    }
                    else
                    {
                        byte b = bytes[i + j];
                        line[hexColumn] = HexChars[(b >> 4) & 0xF];
                        line[hexColumn + 1] = HexChars[b & 0xF];
                        //line[charColumn] = (b < 32 ? '·' : (char)b);
                    }
                    hexColumn += 3;
                    charColumn++;
                }
                result.Append(line);
            }
            return result.ToString();
        }
    }
    class TableItem
    {
        private string __Name;
        private string __Value;
        public TableItem(String name, String value)
        { __Name = name;
          __Value = value;
        }
        public String Name
        {
            get { return __Name; }
        }
        public String Value
        {
            get { return __Value; }
        }
    }
}
