﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using AcquistionInspector.ReadWriteCsv;
namespace AcquistionInspector
{
    /// <summary>
    /// Interaction logic for SaveRegisterWindow.xaml
    /// </summary>
    public partial class SaveRegisterWindow : Window
    {

        private byte[] mem_space;
        private UInt32 current_offset;
        private UInt32 onepagesize;
        private Helper helper;
        public SaveRegisterWindow(Byte[] memspace,UInt32 pageoffset,UInt32 pagesize)
        {
            InitializeComponent();
            helper = Helper.GetInstance();
            this.OK.IsEnabled = false;
            this.CurrentPageCheck.IsChecked = true;
            this.StartAddress.IsEnabled = false;
            this.ByteLen.IsEnabled = false;
            mem_space = new byte[memspace.Length];
            memspace.CopyTo(mem_space,0);
            current_offset = pageoffset;
            onepagesize = pagesize;
            ErrorLabel.Visibility = Visibility.Collapsed;
            Validate();


        }

        private void OnChooseFile(object sender, RoutedEventArgs e)
        {
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();
            dlg.FileName = "Registers"; // Default file name
            dlg.DefaultExt = ".csv"; // Default file extension
            dlg.Filter = "CSV documents (.csv)|*.csv"; // Filter files by extension 
            dlg.CheckFileExists = false;
            
            // Show open file dialog box
            Nullable<bool> result = dlg.ShowDialog();

            // Process open file dialog box results 
            if (result == true)
            {
                // Open document 
                FileLocation.Text = dlg.FileName;
                Validate();
            }
        }

        private void OnOK(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;

            UInt32 start_address = 0;
            UInt32 length = 0;
            List<List<string>> recorders = new List<List<string>>();
            
            if ((bool)this.CurrentPageCheck.IsChecked)
            {
              start_address = current_offset;
              length = onepagesize;
            }
            else
            {

               start_address = Convert.ToUInt32(this.StartAddress.Text, 16);
               length = Convert.ToUInt32(this.ByteLen.Text, 16);
            }
            
            for (UInt32 i = 0 ; i<length/sizeof(UInt32) ;i++)
               {  
                   
                   UInt32 value = (UInt32)((mem_space[start_address]) + (mem_space[start_address + 1] << 8) + (mem_space[start_address + 2] << 16) + (mem_space[start_address + 3] << 24));
                   List<string> register = new List<string>();
                   register.Add(string.Format("{0:X8}", start_address));
                   register.Add(string.Format("{0:X8}", value));
                   recorders.Add(register);
                   start_address+=(sizeof(UInt32));
               }
          
    
            using (CsvFileWriter writer = new CsvFileWriter(FileLocation.Text))
            {
                for (int i = 0; i < recorders.Count; i++)
                {
                    CsvRow row = new CsvRow();
                    for (int j = 0; j < 2; j++)
                        row.Add(recorders[i][j]);
                    writer.WriteRow(row);
                }
                MessageBox.Show("The register has been written to the file", "Success!", MessageBoxButton.YesNo, MessageBoxImage.Information);
                helper.Info(string.Format("Save To {0}",FileLocation.Text));
            }

            
          
           
       }
        private void Validate()
        {
            if (FileLocation.Text.Equals(string.Empty))
            {
                ErrorLabel.Visibility = Visibility.Visible;
                ErrorLabel.Content = "File Location cannot be empty!";
                this.OK.IsEnabled = false;
            }
            else
            {
                if ((bool)this.CurrentPageCheck.IsChecked)
                {
                    ErrorLabel.Visibility = Visibility.Collapsed;
                    this.OK.IsEnabled = true;
                }
                else
                {
                    if (StartAddress.Text.Equals(string.Empty) || ByteLen.Text.Equals(string.Empty))
                    {
                        ErrorLabel.Visibility = Visibility.Visible;
                        ErrorLabel.Content = "StartAddress/Length cannot be empty";
                        this.OK.IsEnabled = false;
                    }
                    else if ((Convert.ToUInt32(StartAddress.Text, 16) % sizeof(UInt32) != 0) || (Convert.ToUInt32(ByteLen.Text, 16) % sizeof(UInt32) != 0))
                    {
                        ErrorLabel.Visibility = Visibility.Visible;
                        ErrorLabel.Content = "StartAddress/Length should be 4 byte alignment";
                        this.OK.IsEnabled = false;
                    }
                    else
                    {
                        ErrorLabel.Visibility = Visibility.Collapsed;
                        this.OK.IsEnabled = true;
                    }

                }   

            }
            
            

            
          

        }
        private void OnCancel(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }
        
        private void CheckAddress(object sender, TextCompositionEventArgs e)
        {
            int hexNumber;
            e.Handled = !int.TryParse(e.Text, NumberStyles.HexNumber, CultureInfo.CurrentCulture, out hexNumber);
            
        }

        private void CheckLength(object sender, TextCompositionEventArgs e)
        {
            int hexNumber;
            e.Handled = !int.TryParse(e.Text, NumberStyles.HexNumber, CultureInfo.CurrentCulture, out hexNumber);
          
        }

    


        private void TextChanged(object sender, TextChangedEventArgs e)
        {
            
           
            Validate();
        }

        private void OnCurrentPageCheck(object sender, RoutedEventArgs e)
        {
            this.StartAddress.IsEnabled = false;
            this.ByteLen.IsEnabled = false;

            ErrorLabel.Visibility = Visibility.Collapsed;
            Validate();
        }

        private void OnCurrentPageUncheck(object sender, RoutedEventArgs e)
        {
            this.StartAddress.IsEnabled = true;
            this.ByteLen.IsEnabled = true;
            Validate();
        }
    }
}
