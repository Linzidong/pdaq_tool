﻿using GalaSoft.MvvmLight;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace AcquistionInspector.Models
{
    public class RegisterModel : ObservableObject
    {
        private RegisterType _registerType;

        private string _description;

        public string Description
        {
            get => _description;
            set
            {
                _description = value;
                RaisePropertyChanged(() => Description);
            }
        }

        private bool _isRegion;

        private ulong _address;

        private ulong _value;

        private string _name;

        private bool _readOnly;

        private uint _offset;

        private uint _lastEntry;

        private ObservableCollection<RegisterModel> _subRegisterList;

        public ObservableCollection<RegisterModel> SubRegisterList
        {
            get => _subRegisterList;
            set
            {
                _subRegisterList = value;
                RaisePropertyChanged(() => SubRegisterList);
            }
        }

        public RegisterType RegisterType
        {
            get => _registerType;
            set
            {
                _registerType = value;
                RaisePropertyChanged(() => RegisterType);
            }
        }

        public uint LastEntry
        {
            get => _lastEntry;
            set
            {
                _lastEntry = value;
                RaisePropertyChanged(() => LastEntry);
            }
        }

        public uint Offset
        {
            get => _offset;
            set
            {
                _offset = value;
                RaisePropertyChanged(() => Offset);
            }
        }

        public bool IsRegion
        {
            get => _isRegion;
            set
            {
                _isRegion = value;

                RaisePropertyChanged(() => IsRegion);
            }
        }

        public bool ReadOnly
        {
            get => _readOnly;
            set
            {
                _readOnly = value;

                RaisePropertyChanged(() => ReadOnly);
            }
        }

        public string Name
        {
            get => _name;
            set
            {
                _name = value;
                RaisePropertyChanged(() => Name);
            }
        }

        public ulong Address
        {
            get => _address;
            set
            {
                _address = value;
                RaisePropertyChanged(() => Address);
            }
        }

        public ulong Value
        {
            get => _value;
            set
            {
                _value = value;
                RaisePropertyChanged(() => Value);
            }
        }
    }

    public class CustomRegisterModel : RegisterModel
    {
        private RegisterBar _registerBar;

        public RegisterBar RegisterBar
        {
            get { return _registerBar; }
            set
            {
                _registerBar = value;
                RaisePropertyChanged(() => RegisterBar);
                if (value == RegisterBar.Bar0)
                {
                    RegisterType = RegisterType.Bar0;
                }

                if (value == RegisterBar.Bar2)
                {
                    RegisterType = RegisterType.Bar2;
                }
            }
        }
    }
}