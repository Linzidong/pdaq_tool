﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using PDAQ4DriverInterface;
using PDAQ4DriverAgent.PDAQ4Implementation;
using GalaSoft.MvvmLight.Messaging;

namespace AcquistionInspector
{
    public enum RunMode
    {
        NORMAL_MODE,
        DEBUG_MODE
    }

    public delegate void DeviceFound(int isfound);

    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>

    public partial class MainWindow : Window
    {
        private PDAQ4Controller pdaq4controller = PDAQ4Controller.GetInstance();
        private Helper helper = Helper.GetInstance();
        private readonly string LAYOUT_FILE_NAME = "Layout.xml";
        private RunMode current_run_mode = RunMode.NORMAL_MODE;
        private bool is_dma_started = false;
        private IMessenger _messenger = Messenger.Default;

        public event DeviceFound devicefoundevent;

        public MainWindow()
        {
            InitializeComponent();

            helper.Info("Start App..............");
            devicefoundevent += registerrwpanel.OnDeviceFound;
            devicefoundevent += settingpanel.OnDeviceFound;
            devicefoundevent += registermappingpanel.OnDeviceFound;

            try
            {
                IPDAQDevice card = pdaq4controller.FindPDAQ4_CARD();
                pdaq4controller.IsPDAQ4Found = 1;
                devicefoundevent(1);
                pdaq4controller.OpenPDAQ4_CARD();
                CardOnBus.Source = new BitmapImage(new Uri("Img\\online.png", UriKind.Relative));
                //pdaq4controller.StartReadRegisterThread();
            }
            catch (PDAQ4SoftResetException e)
            {
                helper.Error(string.Format("{0}", e.Message));
            }
            catch (Exception e)
            {
                CardOnBus.Source = new BitmapImage(new Uri("Img\\offline.png", UriKind.Relative));
                helper.Error(string.Format("{0}", e.Message));
                pdaq4controller.IsPDAQ4Found = 0;
                devicefoundevent(0);
            }
        }

        private void ShowRegisterDetail(object obj)
        {
            throw new NotImplementedException();
        }

        private void OnClick(object sender, RoutedEventArgs e)
        {
        }

        private void MainFrame_Closed(object sender, EventArgs e)
        {
            try
            {
                //SaveLayout();
                //pdaq4controller.StopReadRegisterThread();
                pdaq4controller.ClosePDAQ4_CARD();
            }
            catch (Exception except)
            {
                helper.Error(string.Format("{0}", except.Message));
            }
        }

        //private void SaveLayout()
        //{
        //    var serializer = new Xceed.Wpf.AvalonDock.Layout.Serialization.XmlLayoutSerializer(dockingManager);
        //    using (var stream = new StreamWriter(LAYOUT_FILE_NAME))
        //        serializer.Serialize(stream);
        //}

        //private void RestoreLayout()
        //{
        //    try
        //    {
        //        var serializer = new Xceed.Wpf.AvalonDock.Layout.Serialization.XmlLayoutSerializer(dockingManager);
        //        using (var stream = new StreamReader(LAYOUT_FILE_NAME))
        //            serializer.Deserialize(stream);
        //        dockingManager.UpdateLayout();
        //    }
        //    catch (Exception e)
        //    {
        //        helper.Error(string.Format("{0}", e.Message));

        //    }

        //}
        private void OnDebugMode(object sender, RoutedEventArgs evt)
        {
            if (this.current_run_mode == RunMode.NORMAL_MODE)
            {
                PDAQ4DebugInfo debuginfo = pdaq4controller.DrvAgent.PDAQ4QueryDebugInfo();
                this.currentmode.Text = "Debug Mode";
                this.current_run_mode = RunMode.DEBUG_MODE;
                //this.DebugMode_Img.Source = new BitmapImage(new Uri("Img\\rw_mode.png", UriKind.Relative));
                //QUERY DEBUG INFO
            }
            else
            {
                this.currentmode.Text = "Normal Mode";
                this.current_run_mode = RunMode.NORMAL_MODE;
                //this.DebugMode_Img.Source = new BitmapImage(new Uri("Img\\readonly_mode.png", UriKind.Relative));
            }
        }

        private void OnInitialized(object sender, EventArgs e)
        {
            //RestoreLayout();
        }

        private void ReadInterval_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            this.pdaq4controller.ReadRegisterInterval = ((float)e.NewValue);
        }

        private void OnManualRefresh(object sender, RoutedEventArgs e)
        {
            //UInt32 finalOffset = sizeof(UInt32);

            //Console.WriteLine("UInt32 size = " + finalOffset);
            //return;
            if (pdaq4controller.IsPDAQ4Found == 1 && !pdaq4controller.AutoRefresh)
            {
                this.registerrwpanel.RefreshRegistersManually();
                this.registermappingpanel.RefreshRegistersManually();
            }
        }

        private void OnAutoRefreshChecked(object sender, RoutedEventArgs e)
        {
            //pdaq4controller.AutoRefresh = (bool)this.IsAutoRefresh.IsChecked;
        }

        private void OnStartDMA(object sender, RoutedEventArgs e)
        {
            try
            {
                if (!is_dma_started)
                {
                    settingpanel.StartAcq();
                    pdaq4controller.StartDMA();
                    this.StartDMA_Img.Source = new BitmapImage(new Uri("Img\\stop_imaging.png", UriKind.Relative));
                    is_dma_started = true;
                }
                else
                {   //The register update should be disabled during StopDMA
                    pdaq4controller.StopDMA();
                    settingpanel.StopAcq();
                    this.StartDMA_Img.Source = new BitmapImage(new Uri("Img\\start_imaging.png", UriKind.Relative));
                    is_dma_started = false;
                }
            }
            catch (Exception err)
            {
                settingpanel.StopAcq();
                helper.Error(string.Format("{0}", err.Message));
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            var offset = RegCon.RT_FRAMER_OFFSET;
            var list = pdaq4controller.DrvAgent.ReadRegisterByIC(1, offset, 6);
            //var msi_data = pdaq4controller.DrvAgent.ReadRegisterByIC(RegCon.RT_FRAMER_OFFSET + 0x14);

            foreach (var item in list)
            {
                int r = int.Parse(item.ToString());
            }

            //var version = pdaq4controller.DrvAgent.ReadRegisterByIC(RegCon.RT_FRAMER_OFFSET);
        }
    }
}