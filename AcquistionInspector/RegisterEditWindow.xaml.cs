﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace AcquistionInspector
{
   
    /// <summary>
    /// Interaction logic for RegisterEditWindow.xaml
    /// </summary>
    /// 
    public partial class RegisterEditWindow : Window
    {
        private List<Button> Byte0_Button_List = new List<Button>();
        private List<Button> Byte1_Button_List = new List<Button>();
        private List<Button> Byte2_Button_List = new List<Button>();
        private List<Button> Byte3_Button_List = new List<Button>();
        private UInt32 register_offset = 0;
        private PDAQ4Controller pdaq4controllder = PDAQ4Controller.GetInstance();
        private Helper helper = Helper.GetInstance();
        public RegisterEditWindow()
        {
            InitializeComponent();
            for (int i = 0; i < 8; i++)
            {
                Byte0_Button_List.Add((Button)GridRoot.FindName(string.Format("Bit{0:d2}", i)));
            }
            for (int i = 8; i < 16; i++)
            {
                Byte1_Button_List.Add((Button)GridRoot.FindName(string.Format("Bit{0:d2}", i)));
            }
            for (int i = 16; i < 24; i++)
            {
                Byte2_Button_List.Add((Button)GridRoot.FindName(string.Format("Bit{0:d2}", i)));
            }
            for (int i = 24; i < 32; i++)
            {
                Byte3_Button_List.Add((Button)GridRoot.FindName(string.Format("Bit{0:d2}", i)));
            }

        }

        private void Bit_Click(object sender, RoutedEventArgs e)
        {
            Button button = sender as Button;
            if (button.Content.Equals("0"))
                button.Content = "1";
            else
                button.Content = "0";
            string buttonname = button.Name;
            int bit_num = Convert.ToInt32(buttonname.Substring(3));
            Byte0.Text = string.Format("{0:X2}", ButtonListToByteValue(this.Byte0_Button_List));
            Byte1.Text = string.Format("{0:X2}", ButtonListToByteValue(this.Byte1_Button_List));
            Byte2.Text = string.Format("{0:X2}", ButtonListToByteValue(this.Byte2_Button_List));
            Byte3.Text = string.Format("{0:X2}", ButtonListToByteValue(this.Byte3_Button_List));


        }

        public void SetRegisterToWrite(uint orginalvalue, uint offset)
        {
            byte byte0 = (byte)(orginalvalue & 0xFF);
            byte byte1 = (byte)((orginalvalue >> 8) & 0xFF);
            byte byte2 = (byte)((orginalvalue >> 16) & 0xFF);
            byte byte3 = (byte)((orginalvalue >> 24) & 0xFF);
            Byte0.Text = String.Format("{0:X2}", byte0);
            this.SetByteValueToButtonList(Byte0_Button_List, byte0);
            Byte1.Text = String.Format("{0:X2}", (orginalvalue >> 8) & 0xFF);
            this.SetByteValueToButtonList(Byte1_Button_List, byte1);
            Byte2.Text = String.Format("{0:X2}", (orginalvalue >> 16) & 0xFF);
            this.SetByteValueToButtonList(Byte2_Button_List, byte2);
            Byte3.Text = String.Format("{0:X2}", (orginalvalue >> 24) & 0xFF);
            this.SetByteValueToButtonList(Byte3_Button_List, byte3);
            this.register_offset = offset;


        }

       
        private UInt32 GetRegisterValue()
        {

            byte byte0 = ButtonListToByteValue(this.Byte0_Button_List);
            byte byte1 = ButtonListToByteValue(this.Byte1_Button_List);
            byte byte2 = ButtonListToByteValue(this.Byte2_Button_List);
            byte byte3 = ButtonListToByteValue(this.Byte3_Button_List);

            UInt32 value = (UInt32)((byte3 << 24) + (byte2 << 16) + (byte1 << 8) + byte0);
            return value;

        }
        private void SetByteValueToButtonList(List<Button> buttonlist, Byte value)
        {
            int i = 0;
            foreach (Button button in buttonlist)
            {
                button.Content = string.Format("{0}", ((value >> i) & 0x01));
                i++;
            }
        }

        private byte ButtonListToByteValue(List<Button> buttonlist)
        {
            int i = 0;
            byte value = 0;
            foreach (Button button in buttonlist)
            {
                if (button.Content.Equals("1"))
                {
                    value += (byte)(1 << i);

                }
                i++;
            }

            return value;
        }

        private void Click_Button_Write(object sender, RoutedEventArgs ev)
        {
            try
            {
                UInt32 register_value_to_write = GetRegisterValue();
                pdaq4controllder.WritePDAQ4_CARD_Reg(register_offset, register_value_to_write);

               

            }
            catch(Exception e)
            {
                helper.Error(e.Message);
            }
            finally
            { this.Close(); 
            }
       
        }

        private void Click_Button_Cancel(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void Byte_TextChanged(object sender, TextChangedEventArgs e)
        {   
            
            TextBox textbox = (TextBox)sender;
            textbox.Text = textbox.Text.ToUpper();
            if (!ValidatedByteValue(textbox))
            {
                
                textbox.Background = Brushes.Red;
                
            }
            else
            {
                textbox.Background = Brushes.White;
                //textbox.Text = string.Format("{0:X2}", Convert.ToByte(textbox.Text, 16));
                if (textbox == Byte0)
                    SetByteValueToButtonList(Byte0_Button_List, Convert.ToByte(textbox.Text,16));
                else if (textbox == Byte1)
                    SetByteValueToButtonList(Byte1_Button_List, Convert.ToByte(textbox.Text,16));
                else if (textbox == Byte2)
                    SetByteValueToButtonList(Byte2_Button_List, Convert.ToByte(textbox.Text,16));
                else if (textbox == Byte3)
                    SetByteValueToButtonList(Byte3_Button_List, Convert.ToByte(textbox.Text,16));

            }
               
           

        }

        private bool ValidatedByteValue(TextBox textbox)
        {

            string value_str = (string)textbox.Text;
            
            try
            {
                Convert.ToByte(value_str, 16);
           
            }
            catch (Exception)
            {
                this.Button_Write.IsEnabled = false;
                return false;
            }
            this.Button_Write.IsEnabled = true;
            return true;
        }

        private void OnMouseWheel(object sender, MouseWheelEventArgs e)
        {

            int rotation = e.Delta / System.Windows.Forms.SystemInformation.MouseWheelScrollDelta;
            TextBox textbox = (TextBox)sender;
            if (ValidatedByteValue(textbox))
            {
                byte value = Convert.ToByte(textbox.Text, 16);
                value = (byte)(value + rotation);
                textbox.Text = string.Format("{0:X2}", value);
            }
       
        }

        private void Byte_LostFocus(object sender, RoutedEventArgs e)
        {
            TextBox textbox = (TextBox)sender;
            if (ValidatedByteValue(textbox))
                textbox.Text = string.Format("{0:X2}", Convert.ToByte(textbox.Text, 16));
        }



    }
}
