﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AcquistionInspector
{
    /// <summary>
    /// Interaction logic for LoggingPanel.xaml
    /// </summary>
    ///

    public partial class LoggingPanel : UserControl, IMyLog
    {
        public class Entry
        {
            public string Time { get; set; }
            public string Level{ get; set; }
            public string Message{ get; set; }
        }
        private Helper helper = Helper.GetInstance();
        public LoggingPanel()
        {
            InitializeComponent();
            helper.AddLogger(this);
           
        }



        public void Info(string msg)
        {
            Entry entery = new Entry();
            entery.Time = System.DateTime.Now.ToString();
            entery.Level = "INFO";
            entery.Message = msg;
            LogListView.Items.Add(entery);

        }

        public void Error(string msg)
        {
            Entry entery = new Entry();
            entery.Time = System.DateTime.Now.ToString();
            entery.Level = "ERROR";
            entery.Message = msg;
            LogListView.Items.Add(entery);
        }

        public void Warn(string msg)
        {
            Entry entery = new Entry();
            entery.Time = System.DateTime.Now.ToString();
            entery.Level = "WARN";
            entery.Message = msg;
            LogListView.Items.Add(entery);          
        }
    }

}
