﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AcquistionInspector
{
    interface IMyLog
    {
        void Info(string msg);
        void Error(string msg);
        void Warn(string msg);
    }
}