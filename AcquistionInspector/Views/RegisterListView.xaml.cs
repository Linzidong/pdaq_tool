﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AcquistionInspector.Views
{
    /// <summary>
    /// RegisterListView.xaml 的交互逻辑
    /// </summary>
    public partial class RegisterListView : UserControl
    {
        public RegisterListView()
        {
            InitializeComponent();
        }



        private void CancelButton_OnClick(object sender, RoutedEventArgs e)
        {
            if (WriteValueTb == null) return;
            WriteRegisterPopup.IsOpen = false;
        }

        private void Hex_Dec_Radio_OnChecked_Unchecked(object sender, RoutedEventArgs e)
        {
            if (WriteValueTb == null) return;
            WriteValueTb.Text = string.Empty;
        }
    }
}
