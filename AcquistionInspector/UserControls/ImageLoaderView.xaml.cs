﻿using AcquistionInspector.ViewModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AcquistionInspector.UserControls
{
    /// <summary>
    /// ImageLoaderView.xaml 的交互逻辑
    /// </summary>
    public partial class ImageLoaderView : UserControl
    {
        private bool _scan = true;

        private int __vectorperframe = 256;
        private int __samplepervector = 256;

        public ImageLoaderView()
        {
            InitializeComponent();

            Loaded += ImageLoaderView_Loaded;
        }

        private void ImageLoaderView_Loaded(object sender, RoutedEventArgs e)
        {
            Vector3DCollection myNormalCollection = new Vector3DCollection();
            Point3DCollection myPositionCollection = new Point3DCollection();
            PointCollection myTextureCoordinatesCollection = new PointCollection();
            Int32Collection myTriangleIndicesCollection = new Int32Collection();

            m_ringMesh.Normals = myNormalCollection;

            // Create a collection of vertex positions for the MeshGeometry3D.
            // There will be totally VectorPerFrame*2 vetexs
            if (_scan)
            {
                // The normal of the vetex is direct to the view.
                for (int i = 0; i < __vectorperframe * 2; i++)
                {
                    myNormalCollection.Add(new Vector3D(0, 0, 1));
                }
                for (int i = 0; i < __vectorperframe; i++)
                {
                    double di = (double)i;

                    double x = Math.Cos(di * Math.PI * 2.0 / (__vectorperframe - 1.0));
                    double y = -Math.Sin(di * Math.PI * 2.0 / (__vectorperframe - 1.0));

                    myPositionCollection.Add(new Point3D(x * 0.05, y * 0.05, 0));
                    myPositionCollection.Add(new Point3D(x, y, 0));

                    myTextureCoordinatesCollection.Add(new Point(0, di / (__vectorperframe - 1.0)));
                    myTextureCoordinatesCollection.Add(new Point(1, di / (__vectorperframe - 1.0)));
                }
                m_ringMesh.Positions = myPositionCollection;
                m_ringMesh.TextureCoordinates = myTextureCoordinatesCollection;

                for (int i = 0; i < (__vectorperframe - 1); i++)
                {
                    myTriangleIndicesCollection.Add(i * 2);
                    myTriangleIndicesCollection.Add(i * 2 + 2);
                    myTriangleIndicesCollection.Add(i * 2 + 1);
                    myTriangleIndicesCollection.Add(i * 2 + 1);
                    myTriangleIndicesCollection.Add(i * 2 + 2);
                    myTriangleIndicesCollection.Add(i * 2 + 3);
                }
            }
            else
            {
                for (int i = 0; i < 4; i++)
                {
                    myNormalCollection.Add(new Vector3D(0, 0, 1));
                }

                myPositionCollection.Add(new Point3D(-1, -1, 0));
                myPositionCollection.Add(new Point3D(-1, 1, 0));
                myPositionCollection.Add(new Point3D(1, -1, 0));
                myPositionCollection.Add(new Point3D(1, 1, 0));

                myTextureCoordinatesCollection.Add(new Point(0, 0));
                myTextureCoordinatesCollection.Add(new Point(0, 1));
                myTextureCoordinatesCollection.Add(new Point(1, 0));
                myTextureCoordinatesCollection.Add(new Point(1, 1));
                m_ringMesh.Positions = myPositionCollection;
                m_ringMesh.TextureCoordinates = myTextureCoordinatesCollection;

                myTriangleIndicesCollection.Add(0);
                myTriangleIndicesCollection.Add(2);
                myTriangleIndicesCollection.Add(1);
                myTriangleIndicesCollection.Add(1);
                myTriangleIndicesCollection.Add(2);
                myTriangleIndicesCollection.Add(3);
            }
            m_ringMesh.TriangleIndices = myTriangleIndicesCollection;

            // Apply the mesh to the geometry model.
            m_ringModel.Geometry = m_ringMesh;

            if (this.DataContext is ImageLoaderViewModel md)
            {
                DiffuseMaterial myMaterial = new DiffuseMaterial(new ImageBrush(md.FrameBitmap));
                m_ringModel.Material = myMaterial;
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            //var size = new Size(800, 800);
            //viewport.Measure(size);
            //viewport.Arrange(new Rect(new Point(0, 0), size));

            RenderTargetBitmap bmp = new RenderTargetBitmap((int)RenderSize.Width, (int)RenderSize.Height, 96, 96, PixelFormats.Default);

            bmp.Render(ViewBox);

            BmpBitmapEncoder bpe = new BmpBitmapEncoder();
            var folder = AppDomain.CurrentDomain.BaseDirectory + "SavedImage\\";
            if (!Directory.Exists(folder))
            {
                Directory.CreateDirectory(folder);
            }
            string path = folder + DateTime.Now.ToString("yyyyMMddfff") + ".bmp";
            bpe.Frames.Add(BitmapFrame.Create(bmp));

            using (Stream stream = File.Create(path))
            {
                bpe.Save(stream);
            }
        }

        private void Slider_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
        }
    }
}