﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using AcquistionInspector.Models;

namespace AcquistionInspector.UserControls
{
    /// <summary>
    /// RegisterListCtr.xaml 的交互逻辑
    /// </summary>
    public partial class RegisterListCtr : UserControl
    {
        public RegisterListCtr()
        {
            InitializeComponent();
        }



        public ObservableCollection<RegisterModel> RegisterCollection
        {
            get { return (ObservableCollection<RegisterModel>)GetValue(RegisterCollectionProperty); }
            set { SetValue(RegisterCollectionProperty, value); }
        }

        // Using a DependencyProperty as the backing store for MyProperty.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty RegisterCollectionProperty =
            DependencyProperty.Register("RegisterCollection", typeof(ObservableCollection<RegisterModel>), typeof(RegisterListCtr), new PropertyMetadata(new ObservableCollection<RegisterModel>()));

        private void GridViewColumnHeader_Action_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            if(e.NewSize.Width <= 90 || e.NewSize.Width >= 120)
            {
                e.Handled = true;
                return;
            }
        }
    }
}
