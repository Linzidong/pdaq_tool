
// Includes
#include <Windows.h>
#include <winioctl.h>
#include <initguid.h>
#include <malloc.h>
#include <setupapi.h>
#include <math.h>
#include <stdlib.h>
#include <crtdbg.h>
#include <assert.h>
#include <tchar.h>
#include "DriverAgent.h"
using namespace System;
using namespace System::Collections;
using namespace System::Threading;
using namespace System::Runtime::InteropServices;
using namespace System::Diagnostics;

#include "Fifo.h"
#include "../PDAQ4KMDFDriver/PDAQ4DriverPublic.h"
//.NET namespaces 
using namespace PDAQ4DriverAgent::PDAQ4Implementation;


CFifo::CFifo() 
{
  m_pFifo = IntPtr(nullptr);
  m_Event = nullptr;
}

// Destructor
CFifo::!CFifo() 
{
  Release();
}

// Init()
void CFifo::Init()
{
  if (m_Event == nullptr)
    m_Event = gcnew AutoResetEvent(false);
}

// Release()
VOID CFifo::Release()
{
  delete m_Event;
  m_Event = nullptr;
}

// Connect()
void CFifo:: Connect(IntPtr sharedfifo)
{
  // 'DrvSharedFifo*' fron IOCTL_CONNECT_FIFO
	m_pFifo = sharedfifo;
	
	
}

void CFifo::Disconnect()
{
  m_pFifo = IntPtr(nullptr);
  m_pFifo_descriptor = nullptr;
}
void CFifo::SetFifoInfo(FifoInfo ^ fifoinfo)
{
	m_pFifo_descriptor = fifoinfo;
}
// Read()
LONG CFifo::Read() //T* t)
{
	ULONG index = 0;

  void* pSrc = nullptr;
  DrvSharedFifo* pFifo = (DrvSharedFifo*)((void*)m_pFifo);
  //System::Diagnostics::Trace::WriteLine(String::Format("FIFO<{0:x}, {1:d}>, r={2:d}, w={3:d}", pFifo->nItemSize, pFifo->nItems, pFifo->nRead, pFifo->nWrite));
  if (pFifo->nWrite > pFifo->nRead)
  {
	  index = pFifo->nRead % pFifo->nItems;
	  //System::Diagnostics::Trace::WriteLine(String::Format("FIFO<{0:x}, {1:d}>, r={2:d}, w={3:d}", pFifo->nItemSize, pFifo->nItems, index, pFifo->nWrite));
	  //pSrc = Get  
	  //pFifo->nRead++;
  }
  else
  {
	  index = -1;
  }
  
  //System::Diagnostics::Trace::WriteLine(String::Format("FIFO<{0:x}, {1:d}>, r={2:d}, w={3:d}", pFifo->nItemSize, pFifo->nItems, pFifo->nRead, pFifo->nWrite));
  return index;
}

// Write the Data to the FIFO
LONG CFifo::Write() //T* t)
{
	ULONG index = 0;

	void* pSrc = nullptr;
	DrvSharedFifo* pFifo = (DrvSharedFifo*)((void*)m_pFifo);
	//System::Diagnostics::Trace::WriteLine(String::Format("FIFO<{0:x}, {1:d}>, r={2:d}, w={3:d}", pFifo->nItemSize, pFifo->nItems, pFifo->nRead, pFifo->nWrite));
	// At the begining, there are nItems element in the read dma
	if (pFifo->nRead > pFifo->nWrite - pFifo->nItems)
	{
		index = pFifo->nWrite % pFifo->nItems;
		//System::Diagnostics::Trace::WriteLine(String::Format("FIFO<{0:x}, {1:d}>, r={2:d}, w={3:d}", pFifo->nItemSize, pFifo->nItems, index, pFifo->nWrite));
		//pSrc = Get  
		//pFifo->nRead++;
	}
	else
	{
		index = -1;
	}

	//System::Diagnostics::Trace::WriteLine(String::Format("FIFO<{0:x}, {1:d}>, r={2:d}, w={3:d}", pFifo->nItemSize, pFifo->nItems, pFifo->nRead, pFifo->nWrite));
	return index;
}



void CFifo::Pack(IntPtr ptr, long index)
{
	m_mutex->WaitOne();
	DrvSharedFifo * sharedinfo = (DrvSharedFifo*)m_pFifo.ToPointer();
	FifoElement^ element = (FifoElement^)m_pFifo_descriptor->fifo_element_list[index];
	Byte *p =(Byte*)(void*) ptr;
	//array<UInt32>^ reg_array = gcnew array<UInt32>(0x81080);
	//pin_ptr<UInt32> p = &reg_array[0];
	for (int i = 0; i < element->dma_descriptor_list->Count; i++)
	{
		
		HardwareResource^ hwr = (HardwareResource^)element->dma_descriptor_list[i];
		//UInt32 *v = (UInt32 *)(void *)(hwr->VirtualPointer);
		
		memcpy(p, (void*)(hwr->VirtualPointer), hwr->Bytes);
		//CopyMemory(p, hwr->VirtualPointer.ToPointer(), hwr->Bytes);
		//System::Diagnostics::Trace::WriteLine(String::Format("{0:X}",hwr->Bytes));
		p += hwr->Bytes;
	}
	m_mutex->ReleaseMutex();
}


void CFifo::UnPack(IntPtr ptr,long size, long index)
{
	m_mutex->WaitOne();
	DrvSharedFifo * sharedinfo = (DrvSharedFifo*)m_pFifo.ToPointer();
	FifoElement^ element = (FifoElement^)m_pFifo_descriptor->fifo_element_list[index];
	Byte *p = (Byte*)(void*)ptr;
	//array<UInt32>^ reg_array = gcnew array<UInt32>(0x81080);
	//pin_ptr<UInt32> p = &reg_array[0];
	int blocksize = size;
	for (int i = 0; i < element->dma_descriptor_list->Count; i++)
	{

		HardwareResource^ hwr = (HardwareResource^)element->dma_descriptor_list[i];
		//UInt32 *v = (UInt32 *)(void *)(hwr->VirtualPointer);

		if (blocksize >= hwr->Bytes)
		{
			memcpy((void*)(hwr->VirtualPointer), p, hwr->Bytes);
			blocksize -= hwr->Bytes;
			p += hwr->Bytes;
		}
		else
		{
			memcpy((void*)(hwr->VirtualPointer), p, blocksize);
			blocksize = 0;
			break;
		}
		
		//CopyMemory(p, hwr->VirtualPointer.ToPointer(), hwr->Bytes);
		//System::Diagnostics::Trace::WriteLine(String::Format("{0:X}",hwr->Bytes));
		
	}
	m_mutex->ReleaseMutex();
}

void CFifo::IncReadIndex()
{
  DrvSharedFifo* pFifo = (DrvSharedFifo*)((void*)m_pFifo);
  pFifo->nRead += 1;
}

void CFifo::IncWriteIndex()
{
	DrvSharedFifo* pFifo = (DrvSharedFifo*)((void*)m_pFifo);
	pFifo->nWrite += 1;
}

// EventHandle()
IntPtr CFifo::GetNativeWaitHandle() 
{
  return m_Event->SafeWaitHandle->DangerousGetHandle();
}

long CFifo:: GetNumOverflow() 
{
  DrvSharedFifo* pFifo = (DrvSharedFifo*)((void*)m_pFifo);
  return (pFifo)? pFifo->nOverflow: -1;
}



