/*******************************************************************************************\
* File:
* Description:
*  Fifo class is used for high speed communicate with kernel mode via shared memory
* Copyright: Boston Scientific Corp. 2009
* History: 
*     1. LinX - Created, 05/2009
\*******************************************************************************************/
#pragma once

// Includes
using namespace System;
using namespace System::Threading;
////////////////////////////////////////////////////////////////////////////
// Note: we (the application) are either reader or writer, but not both
//  currently, this class is only used to read data that are written by kernel
//  - passing data to kernel is done by IOCTL, which is queued by IRP

namespace PDAQ4DriverAgent
{
	namespace PDAQ4Implementation
	{
		public ref class FifoElement
		{
		public:
			ArrayList^ dma_descriptor_list;
			FifoElement()
			{
				dma_descriptor_list = gcnew ArrayList();
			}
		};

		public ref class FifoInfo
		{
		public:
			UInt32 frame_type;
			ArrayList^ fifo_element_list;
		public:
			FifoInfo()
			{
				fifo_element_list = gcnew ArrayList();
			}
			void AddFifoElement(FifoElement^ element)
			{
				fifo_element_list->Add(element);
			}
			virtual property UInt32 FifoSize
			{
				UInt32 get(){ return fifo_element_list->Count; }

			}
			virtual property UInt32 FrameType
			{
				UInt32 get(){ return frame_type; }
				void set(UInt32 frametype){ frame_type = frametype; }
			}

		};


		public ref class CFifo
		{
		public:
			// Constructor
			CFifo();

			// Destructor
			~CFifo() { this->!CFifo(); }

			!CFifo();

			// Init()
			void Init();

			// Connect()
			void Connect(IntPtr sharedfifo);

			void SetFifoInfo(FifoInfo ^ fifoinfo);

			void Disconnect();
			// Read data from FIFO()
			LONG Read();
			// Write data to FIFO ()
			LONG Write();
			void Pack(IntPtr, long index);
			void UnPack(IntPtr,long size, long index);
			void IncWriteIndex();
			void IncReadIndex();

			// EventHandle()
			IntPtr GetNativeWaitHandle();

			long GetNumOverflow();

			property AutoResetEvent^ ReadyEvent
			{
				AutoResetEvent^ get() { return  m_Event; }
			}
		private:
			// Release()
			void Release();
			AutoResetEvent^			m_Event;
			IntPtr      		m_pFifo;  // returned by kernel
			FifoInfo^				m_pFifo_descriptor;
			static initonly Mutex^ m_mutex = gcnew Mutex;
			
		};

	}

}