﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using PDAQ4DriverAgent;
using PDAQ4DriverAgent.PDAQ4Implementation;

namespace Driver_Tool
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            DriverAgent agent = new DriverAgent();

            agent.OpenPDAQ4();

            AgentTb.Text = agent.GetHashCode().ToString();
        }
    }
}