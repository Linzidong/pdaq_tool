﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;
namespace PDAQ4DriverInterface
{
    /// <summary>
    /// This class is CatheterTable class.
    /// </summary>
    [XmlRootAttribute("CatheterTable", Namespace = "http://www.bsc.com",
            IsNullable = false)]
    public class CatheterTable
    {
        /// <summary>
        /// ILAB-1011.
        /// Sound speed in water, used in all caculations
        /// Uint can be: m/s, mm/ms, micron/us, or nanometer/ns
        /// </summary>
        public const float SoundSpeed = 1562.5f;

        /// <summary>
        /// ADC Frequency
        /// </summary>
        public const double ADCFreq = 200E6;

        /// <summary>
        /// Gets or sets the Name of Catheter Table.
        /// </summary>
        public string TableName { set; get; }

        /// <summary>
        /// Gets or sets the Table Version.
        /// </summary>
        public string TableVersion { set; get; }

        // Removed following review with LiW
        /// <summary>
        /// Gets or sets the Table HashCode.
        /// </summary>
        public string TableHashCode { set; get; }

        /// <summary>
        /// Gets or sets the List of  Preset Vessel Names.
        /// </summary>
        public PresetVessel[] VesselList { set; get; }

        /// <summary>
        /// Gets or sets the List of Frequently Used Annotations.
        /// </summary>
        public Annotation[] AnnotationList { set; get; }

        /// <summary>
        /// Dictionary of all catheters.
        /// Each entry contains CatheterID and an instance of Catheter object itself.
        /// </summary>
        public SerializableDictionary<uint, Catheter> CatheterList;

        /// <summary>
        /// Read/Write table from file
        /// </summary>
        public void WriteCathTable(string filename)
        {
            // Create an instance of the XmlSerializer class;
            // specify the type of object to serialize.
            XmlSerializer serializer = new XmlSerializer(typeof(CatheterTable));
            TextWriter writer = new StreamWriter(filename);

            // Serialize the cath table, and close the TextWriter.
            //serializer.Serialize(writer, _cathTable);
            serializer.Serialize(writer, this);
            writer.Close();
        }

        public static CatheterTable ReadCathTable(string filename)
        {
            return ReadCathTable(filename, true);
        }

        public static CatheterTable ReadCathTable(string filename, bool compareHashCode)
        {
            CatheterTable cathtable = new CatheterTable();

            // Create an instance of the XmlSerializer class;
            // specify the type of object to be deserialized.
            XmlSerializer serializer = new XmlSerializer(typeof(CatheterTable));

            // If the XML document has been altered with unknown 
            // nodes or attributes, handle them with the 
            // UnknownNode and UnknownAttribute events.
            serializer.UnknownNode += new XmlNodeEventHandler(serializer_UnknownNode);
            serializer.UnknownAttribute += new XmlAttributeEventHandler(serializer_UnknownAttribute);

            // Try to read
            try
            {
                // A FileStream is needed to read the XML document.
                FileStream fs = new FileStream(filename, FileMode.Open);

                // Use the Deserialize method to restore the object's state with
                // data from the XML document.
                cathtable = (CatheterTable)serializer.Deserialize(fs);
                fs.Close();
            }

            catch (InvalidOperationException ex)
            {
                throw new InvalidOperationException("Invalid Cath Profile: " + ex.Message);
            }

            catch (FileNotFoundException ex)
            {
                throw new FileNotFoundException("Cath Profile not found: " + ex.Message);
            }

            catch (Exception ex)
            {
                throw new Exception("Error reading Cath Profile: " + ex.Message);
            }

            /// ILAB-1011, generate acquisition delay in ImagingMode from catheter property
            foreach (KeyValuePair<uint, Catheter> r in cathtable.CatheterList)
            {
                r.Value.CalculateDelay();
            }

            // Compute gamma correction table
            foreach (KeyValuePair<uint, Catheter> r in cathtable.CatheterList)
            {
                foreach (ImagingMode im in r.Value.ImagingModes)
                {
                    im.Gamma = cathtable.LogLinearGammaCurve(im.TGC.HWGammaDBRange);
                }
            }

            // Compute and compare hash code
            if ((compareHashCode) && (cathtable.TableHashCode.CompareTo(cathtable.ComputeHashCode) != 0))
            {
                System.Diagnostics.Debug.WriteLine("Catheter table hash code is invalid");
                throw new Exception("Catheter table hash code is invalid", new System.IO.InvalidDataException());
            }

            return cathtable;
        }

        // Calculate 4096 byte gamma correction array
        // Maps 12 bit catheter output to 8 bit image
        public byte[] LogLinearGammaCurve(double gammaDB)
        {
            byte[] curve = new byte[4096];

            //dr_dB = 72;
            //G72 = min(255, round(20 * log10(max(1, A)) * 255 / dr_dB));
            for (int i = 0; i < 4096; i++)
            {
                double gv = Math.Min(255, Math.Round(20 * Math.Log10(Math.Max(1, i)) * 255 / gammaDB));
                curve[i] = (byte)gv;

                // Debug
                //System.Diagnostics.Trace.WriteLine(i + " " + curve[i]);
            }

            // Standard power law curve
            //for (int i = 0; i < 4096; i++)
            //{
            //    double v = Math.Pow((double)i / 4096, gamma);

            //    curve[i] = (byte)(v * 256);
            //}

            return curve;
        }

        // Hash code calculation function
        public string ComputeHashCode
        {
            get
            {
                int sum = 0;

                // Clear existing hash code so it does not interfere
                string hashcode = TableHashCode;
                TableHashCode = null;

                // Sum bytes in class
                byte[] barray = ToByteArray(this);

                foreach (byte b in barray)
                    sum += b;

                // Restore original
                TableHashCode = hashcode;

                // Convert to string
                return sum.ToString();
            }
        }

        private byte[] ToByteArray(object source)
        {
            // Create an instance of the XmlSerializer class;
            XmlSerializer serializer = new XmlSerializer(typeof(CatheterTable));

            using (var stream = new MemoryStream())
            {
                serializer.Serialize(stream, source);
                return stream.ToArray();
            }
        }

        private static void serializer_UnknownNode
        (object sender, XmlNodeEventArgs e)
        {
            System.Diagnostics.Trace.WriteLine(@"Unknown Node:" + e.Name + char.ToString('\t') + e.Text);
            throw new Exception("Catheter table unknown node", new System.IO.InvalidDataException());
        }

        private static void serializer_UnknownAttribute
        (object sender, XmlAttributeEventArgs e)
        {
            System.Xml.XmlAttribute attr = e.Attr;
            System.Diagnostics.Trace.WriteLine("Unknown attribute " +
                                        attr.Name + "='" + attr.Value + "'");
            throw new Exception("Catheter table unknown attribute", new System.IO.InvalidDataException());
        }
    }

    /// <summary>
    /// This class is PresetVessel class.
    /// </summary>
    public class PresetVessel
    {
        /// <summary>
        /// Gets or sets the Vessel ID.
        /// </summary>
        public uint VesselID { set; get; }

        /// <summary>
        /// Gets or sets the Vessel Type.
        /// </summary>
        public VesselType VesselType { set; get; }

        /// <summary>
        /// Gets or sets the Vessel Name.
        /// </summary>
        public string VesselName { set; get; }

    }

    /// <summary>
    /// The queue state.
    /// </summary>
    public enum VesselType
    {
        /// <summary>
        ///   ICE vessel type
        /// </summary>
        ICE,

        /// <summary>
        ///   Coronary vessel type
        /// </summary>
        Coronary,

        /// <summary>
        ///   Peripheral vessel type
        /// </summary>
        Peripheral,

        /// <summary>
        ///   Simulator vessel type
        /// </summary>
        Simulator
    }

    /// <summary>
    /// This class is Annotation class.
    /// </summary>
    public class Annotation
    {
        /// <summary>
        /// Gets or sets the Annotation ID.
        /// </summary>
        public byte AnnotationID { set; get; }

        /// <summary>
        /// Gets or sets the VesselType ICE, Coronary,  Peripheral.
        /// </summary>
        public VesselType VesselType { set; get; }

        /// <summary>
        /// Gets or sets the Annotation Text.
        /// </summary>
        public string AnnotationText { set; get; }
    }

    /// <summary>
    /// This class is Catheter class.
    /// </summary>
    public class Catheter
    {
        /// <summary>
        /// Gets or sets the Catheter ID.
        /// </summary>
        public uint CatheterID { set; get; }

        /// <summary>
        /// Gets or sets the Catheter Names.
        /// </summary>
        public string CatheterName { set; get; }

        /// <summary>
        /// Gets or sets the NumImagingModes.
        /// </summary>
        public int NumImagingModes { set; get; }

        /// <summary>
        /// Gets or sets the ImagingModes.
        /// </summary>
        public ImagingMode[] ImagingModes { set; get; }

        /// <summary>
        /// Gets or sets the Catheter Dimension.
        /// </summary>
        public uint CathSize { set; get; }

        /// <summary>
        /// Gets or sets the Offset from the Center Axis.
        /// </summary>
        public uint xDucerOffset { set; get; }

        /// <summary>
        /// Gets or sets the Tilted Angle along the Axis.
        /// </summary>
        public uint xDucerAngle { set; get; }

        /// <summary>
        /// Gets or sets the Sheath Thickness.
        /// </summary>
        public uint SheathThickness { set; get; }	// Reserved for later

        /// <summary>
        /// Gets or sets the Sound Velocity of the Sheath.
        /// </summary>
        public uint SheathSpeed { set; get; }	// Reserved for later

        /// <summary>
        /// Transition to Review after record, or stay imaging after record ?
        /// </summary>
        public bool ReviewAfterRecord { set; get; }

        /// ILAB-1011
        /// Calculate delay for each imaging mode from Catheters' mechanical properties
        public void CalculateDelay()
        {
            // Remove acquisition delay modifiers per ILAB-2198
            //double r = CathSize * 1000.0 / 60; // catheter radius in micron
            //double projection = Math.Cos(Math.PI * xDucerAngle / 180.0);

            //double tad = 2 * (r - xDucerOffset - SheathThickness) / (CatheterTable.SoundSpeed * projection) + 2 * SheathThickness / (SheathSpeed * projection);

            // now for every mode
            foreach (ImagingMode m in ImagingModes)
            {
                if (m.TRCControl != null)
                    m.AcquisitionDelay = Convert.ToUInt32(/*tad * 1000 +*/ m.TRCControl.SystemDelay[0]); //.TxRxTraces[0].SystemDelay); // in nano seconds
            }
        }

    }

    /// <summary>
    /// This class is ImagingMode class.
    /// </summary>
    public class ImagingMode
    {
        // From Xi
        // Path to FPGA firmware
        public string Firmware { set; get; }

        // Friendly name for mode
        public string ModeName { set; get; }

        // ID for Mode
        public uint ModeID { get; set; }

        /// <summary>
        /// Gets or sets the Default Depth Setting.
        /// </summary>
        public uint DefaultDepth { set; get; }

        /// <summary>
        /// Gets or sets the Available Depth Values. // Decimation / 256
        /// Used to calculate image diameter
        /// </summary>
        public uint[] DepthValue { set; get; }

        /// <summary>
        /// Gets or sets the Transmit and Receive Control.
        /// </summary>
        public TxRecControl TRCControl { set; get; }

        /// <summary>
        /// Gets or sets the number of HWTGCTables supported.
        /// </summary>
        public uint NumHWTGCTables { set; get; }

        /// <summary>
        /// This is delay from start of TX to start of acquisition, in nanosecond
        /// It shall be calculated from fields in Catheter:  size, sheath, speed etc
        /// (So does it need to be in here ?)
        /// </summary>
        [XmlIgnore]
        public uint AcquisitionDelay { get; set; }

        /// <summary>
        /// Gets or sets the HWTGC table.
        /// Size 4.
        /// </summary>
        public HWTGC[] HWTGCTable { set; get; }

        /// <summary>
        /// Gets or sets the number of HWDSPPipeLines supported.
        /// </summary>
        public uint NumHWDSPPipeLines { set; get; }

        /// <summary>
        /// Gets or sets the HW DSP Pipeline.
        /// Size 4.
        /// </summary>
        public DSPPipeline[] HWDSPPipeLine { set; get; }

        /// <summary>
        /// Gets or sets the number of ImagingChannels supported.
        /// </summary>
        public uint NumImagingChannels { set; get; }

        /// <summary>
        /// Gets or sets the Imaging Channel.
        /// Size 4.
        /// </summary>
        public ImageChannel[] ImagingChannel { set; get; }

        /// <summary>
        /// Gets or sets the NumStageFilter1.
        /// </summary>
        public int NumStageFilter1 { set; get; }

        /// <summary>
        /// Gets or sets the list of available Stage 1 filters.
        /// Size 4.
        /// </summary>
        public StageFilter[] StageFilter1 { set; get; }

        /// <summary>
        /// Gets or sets the NumStageFilter2.
        /// </summary>
        public int NumStageFilter2 { set; get; }

        /// <summary>
        /// Gets or sets the list of available Stage 2 filters.
        /// Size 4.
        /// </summary>
        public StageFilter[] StageFilter2 { set; get; }

        /// <summary>
        /// Gets or sets the NumStageFilter3.
        /// </summary>
        public int NumStageFilter3 { set; get; }

        /// <summary>
        /// Gets or sets the list of available Stage 3 filters.
        /// Size 4.
        /// </summary>
        public StageFilter[] StageFilter3 { set; get; }

        /// <summary>
        /// Gets or sets the Vessel Type ICE/Coronary/Peripheral.
        /// </summary>
        public VesselType VesselType { set; get; }

        /// <summary>
        /// Gets or sets the Display Mechanical Index.
        /// </summary>
        public float MechanicalIndex { set; get; }

        /// <summary>
        /// Gets or sets the Display Thermal Index.
        /// </summary>
        public float ThermalIndex { set; get; }	// Reserved

        // Gamma table goes, replaced by HWGammaDBRange and calculated gamma table
        ///// <summary>
        ///// Gets or sets the Gamma log lookup table for 2 bytes envelop (~96 dB).
        ///// 4k for 12 bit resolution.
        ///// </summary>
        //[XmlElement(DataType = "hexBinary")]
        [XmlIgnore]
        public byte[] Gamma { set; get; }

        /// <summary>
        /// TGC Algorithm parameters
        /// </summary>
        public TGCValues TGC { set; get; }
    }

    /// <summary>
    /// This class is HWTGC class.
    /// </summary>
    public class HWTGC
    {
        /// <summary>
        /// Gets or sets the TGC ID.
        /// For multiple transmission
        /// </summary>
        public byte TGCID { set; get; }

        /// <summary>
        /// Gets or sets the TGC Delay.
        /// </summary>
        public uint TGCDelay { set; get; }

        /// <summary>
        /// Initial value of TGC that cannot be altered by TGC control
        /// (For image quality in the near field)
        ///  </summary>
        public uint PreTGCValue { set; get; }

        /// <summary>
        /// Gets or sets the TGC Value.
        /// Use same concept as smart tgc - use control points (array open size)
        /// </summary>
        [XmlElement(DataType = "hexBinary")]
        public byte[] TGCValue { set; get; }
    }

    /// <summary>
    /// This class contains the parameters for the TGC algorithm
    /// </summary>
    public class TGCValues
    {
        /// <summary>
        /// Minimum Gain
        /// Minimum gain presented to user in the interface.
        /// Don't want user to scrub image completely.
        /// Gain used after automatic setting stage (user override)
        /// </summary>
        public float GainUserMin { set; get; }


        /// <summary>
        /// Maximum Gain
        /// Maximum gain presented to user in the interface.
        /// </summary>
        public float GainUserMax { set; get; }

        /// <summary>
        /// Target Gain
        /// Nominal brightness we desire to present to the user. Used for automatic determination. 
        /// </summary>
        public float GainAutoTarget { set; get; }

        /// <summary>
        /// Gain Tolerance
        /// How much variation we can allow from the Target. Used for automatic.
        /// </summary>
        public float GainAutoTolerance { set; get; }

        /// <summary>
        /// Far-field gain:
        /// Controls how much far-field gain is applied. Three settings will be available, low, medium, and high.
        /// 
        /// Users interested in far field readings may want increased far-field gain.
        /// Users not interested in far field may want to suppress this.
        ///
        /// </summary
        /// 

        /// <summary>
        /// Low - Selects low value of gain used in far field image (Suppress far field)
        /// </summary>
        public float FarFieldLowGain { set; get; }

        /// <summary>
        /// Medium - Selects medium value of gain used in far field image (No enhancement)
        /// </summary>
        public float FarFieldMediumGain { set; get; }

        /// <summary>
        /// High - Selects high value of gain used in far field image (Enhance far field)
        /// </summary>
        public float FarFieldHighGain { set; get; }

        /// <summary>
        /// Near Field gain
        /// Some users will want to suppress near field image. (Use term near field ring-down suppresssion as UI term)
        /// </summary>

        /// <summary>
        /// Low - Selects low value of image suppression for near field. (Most likely, this means no suppression)
        /// </summary>
        public float NearFieldLowSuppression { set; get; }

        /// <summary>
        /// Medium - Selects medium value of image suppression for near field.
        /// </summary>
        public float NearFieldMediumSuppression { set; get; }

        /// <summary>
        /// High - Selects high value of image suppression for near field. (For users who can tolerate a dark near field)
        /// </summary>
        public float NearFieldHighSuppression { set; get; }

        /// <summary>
        /// SWGamma - Conversion of dB value to grey scale.
        /// Map output dB value to nearest dB value in table, and use that index in the image
        /// </summary>
        //[XmlElement(DataType = "float")]
        public float[] SWGamma { set; get; }

        /// <summary>
        /// HWGammaDBRange - Single float value used to compute the HW gamma LUT.
        /// </summary>
        public float HWGammaDBRange { set; get; }
    }

    /// <summary>
    /// This class is StageFilter class.
    /// </summary>
    public class StageFilter
    {
        /// <summary>
        /// Name for displaying on UI
        /// </summary>
        public string FilterName { set; get; }

        /// <summary>
        /// Gets or sets the type of Filter.
        /// </summary>
        public byte FilterType { set; get; }

        /// <summary>
        /// Gets or sets the Filter ID.
        /// </summary>
        public byte FilterID { set; get; }

        /// <summary>
        /// Gets or sets the Filter Length.
        /// </summary>
        public uint FilterLength { set; get; }

        /// <summary>
        /// Gets or sets the Scaling Factor.
        /// </summary>
        public float ScalingFactor { set; get; }

        /// <summary>
        /// Gets or sets the Filter Coefficients.
        /// Normalised to 1.0
        /// </summary>
        public float[] Coefficients { set; get; }
    }

    /// <summary>
    /// This class is Transmit Receive Control class.
    /// </summary>
    public class TxRecControl
    {
        ///
        /// Burst frequency in MHz
        /// 
        public uint BurstFrequency { set; get; }

        /// <summary>
        /// Number of pulses
        /// </summary>
        public uint NumberOfPulses { set; get; }

        /// <summary>
        /// Transmit peak-peak voltage, in Volts.
        /// </summary>
        public uint Voltage { get; set; }

        /// <summary>
        /// Gets or sets the Index of HW_TGC table.
        /// Size 8.
        /// </summary>
        [XmlElement(DataType = "hexBinary")]
        public byte[] TGCIndex { set; get; }

        /// <summary>
        /// Gets or sets the System Delay in ns.
        /// Size 8.
        /// For multiple transmission modes, right now only one
        /// 
        /// </summary>
        public uint[] SystemDelay { set; get; }

        /// <summary>
        /// Gets or sets the Transmission (Pulse) Interval in ms.
        /// For multiple transmission modes, right now only one
        /// Size 8.
        /// </summary>
        public uint[] TransmissionInterval { set; get; }

        /// <summary>
        /// Gets or sets the Summation Coefficient.
        /// For multiple transmission modes, right now only one
        /// Size 8.
        /// </summary>
        public int[] SumCoefficient { set; get; }

        /// <summary>
        /// Gets or sets the Motor Speed. RPM
        /// </summary>
        public uint MotorSpeed { set; get; }

        /// <summary>
        /// Gets or sets Number of Vectors per Frame.
        /// </summary>
        public uint VectorsPerFrame { set; get; }

        /// <summary>
        /// Gets or sets the Tx Summation.
        /// Size 4.
        /// </summary>
        public uint[] TxSummation { set; get; }

        /// <summary>
        /// Gets or sets the Summation Output.
        /// </summary>
        public uint SummationOutput { set; get; }
    }

    /// <summary>
    /// This class is DSPPipeline class.
    /// </summary>
    public class DSPPipeline
    {
        /// <summary>
        /// Gets or sets the Demodulation Frequency.
        /// </summary>
        public uint DeModFrequency { set; get; }

        /// <summary>
        /// Gets or sets the Samples per Vector.
        /// Output to channel.
        /// </summary>
        public uint SamplesPerVector { set; get; }

        /// <summary>
        /// Gets or sets the Pipeline input table for mixer output mapping.
        /// (Which input to use)
        /// </summary>
        public int PIT { set; get; }

        /// <summary>
        /// Gets or sets ID for Stage 1 filter.
        /// </summary>
        public int Filter1ID { set; get; }

        /// <summary>
        /// Gets or sets ID for Stage 2 filter.
        /// </summary>
        public int Filter2ID { set; get; }

        /// <summary>
        /// Gets or sets ID for Stage 3 filter.
        /// </summary>
        public int Filter3ID { set; get; }
    }

    /// <summary>
    /// This class is DisplaySchema class.
    /// </summary>
    public class DisplaySchema
    {
        /// <summary>
        /// Gets or sets the Overlay Mode.
        /// </summary>
        public int OverlayMode { set; get; }

        /// <summary>
        /// Gets or sets the Percentage Transparent.
        /// </summary>
        public int PercentageTransparent { set; get; }

        /// <summary>
        /// Gets or sets the Color Map.
        /// Size 3.
        /// </summary>
        [XmlElement(DataType = "hexBinary")]
        public byte[][] ColorMap { set; get; }
    }

    /// <summary>
    /// This class is ImageChannel class.
    /// </summary>
    public class ImageChannel
    {
        /// <summary>
        /// Gets or sets the ImageChannelDictionary.
        /// Includes DDPVolume, SmartTGC control points, NURD correction paramters
        /// </summary>
        public SerializableDictionary<string, string> ImageChannelDictionary { set; get; }

        /// <summary>
        /// Gets or sets the Display Schema Index to define overlay.
        /// </summary>
        public int DisplayIndex { set; get; }

        /// <summary>
        /// Gets or sets the FrameRate, channel frame rate.
        /// </summary>
        public int FrameRate { set; get; }

    }

    // These are actually part of CatheterInfo, so will just use that class
    //// Catheter mask , microns/sample, and mask microns 
    //public class DepthDataValues
    //{
    //    public float Depth;
    //    public float CatheterMask;
    //    public float MicronsPerSample;
    //    public float MaskMicrons;
    //}

    /// <summary>
    /// The serializable dictionary.
    /// </summary>
    /// <typeparam name="TKey">
    /// </typeparam>
    /// <typeparam name="TVal">
    /// </typeparam>
    [Serializable]
    public class SerializableDictionary<TKey, TVal> : Dictionary<TKey, TVal>, IXmlSerializable, ISerializable
    {
        #region Constants and Fields

        /// <summary>
        /// The dictionary node name.
        /// </summary>
        private const string DictionaryNodeName = "Dictionary";

        /// <summary>
        /// The item node name.
        /// </summary>
        private const string ItemNodeName = "Item";

        /// <summary>
        /// The key node name.
        /// </summary>
        private const string KeyNodeName = "Key";

        /// <summary>
        /// The value node name.
        /// </summary>
        private const string ValueNodeName = "Value";

        /// <summary>
        /// The dictionary of all xml serializers.
        /// </summary>
        private static Dictionary<Type, XmlSerializer> _allSerializers = new Dictionary<Type, XmlSerializer>();

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="SerializableDictionary{TKey,TVal}"/> class.
        /// </summary>
        public SerializableDictionary()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SerializableDictionary{TKey,TVal}"/> class.
        /// </summary>
        /// <param name="dictionary">
        /// The dictionary.
        /// </param>
        public SerializableDictionary(IDictionary<TKey, TVal> dictionary)
            : base(dictionary)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SerializableDictionary{TKey,TVal}"/> class.
        /// </summary>
        /// <param name="comparer">
        /// The comparer.
        /// </param>
        public SerializableDictionary(IEqualityComparer<TKey> comparer)
            : base(comparer)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SerializableDictionary{TKey,TVal}"/> class.
        /// </summary>
        /// <param name="capacity">
        /// The capacity.
        /// </param>
        public SerializableDictionary(int capacity)
            : base(capacity)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SerializableDictionary{TKey,TVal}"/> class.
        /// </summary>
        /// <param name="dictionary">
        /// The dictionary.
        /// </param>
        /// <param name="comparer">
        /// The comparer.
        /// </param>
        public SerializableDictionary(IDictionary<TKey, TVal> dictionary, IEqualityComparer<TKey> comparer)
            : base(dictionary, comparer)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SerializableDictionary{TKey,TVal}"/> class.
        /// </summary>
        /// <param name="capacity">
        /// The capacity.
        /// </param>
        /// <param name="comparer">
        /// The comparer.
        /// </param>
        public SerializableDictionary(int capacity, IEqualityComparer<TKey> comparer)
            : base(capacity, comparer)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SerializableDictionary{TKey,TVal}"/> class.
        /// </summary>
        /// <param name="info">
        /// The info.
        /// </param>
        /// <param name="context">
        /// The context.
        /// </param>
        protected SerializableDictionary(SerializationInfo info, StreamingContext context)
        {
            int itemCount = info.GetInt32("ItemCount");
            for (int i = 0; i < itemCount; i++)
            {
                KeyValuePair<TKey, TVal> kvp =
                    (KeyValuePair<TKey, TVal>)
                    info.GetValue(string.Format("Item{0}", i), typeof(KeyValuePair<TKey, TVal>));
                Add(kvp.Key, kvp.Value);
            }
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets the value serializer.
        /// </summary>
        protected XmlSerializer ValueSerializer
        {
            get
            {
                lock (_allSerializers)
                {
                    if (!_allSerializers.ContainsKey(typeof(TVal)))
                    {
                        _allSerializers[typeof(TVal)] = new XmlSerializer(typeof(TVal));
                    }

                    return _allSerializers[typeof(TVal)];
                }
            }
        }

        /// <summary>
        /// Gets the key serializer.
        /// </summary>
        private XmlSerializer KeySerializer
        {
            get
            {
                lock (_allSerializers)
                {
                    if (!_allSerializers.ContainsKey(typeof(TKey)))
                    {
                        _allSerializers[typeof(TKey)] = new XmlSerializer(typeof(TKey));
                    }

                    return _allSerializers[typeof(TKey)];
                }
            }
        }

        #endregion

        #region Explicit Interface Methods

        /// <summary>
        /// The get object data.
        /// </summary>
        /// <param name="info">
        /// The info.
        /// </param>
        /// <param name="context">
        /// The context.
        /// </param>
        void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("ItemCount", Count);
            int itemIdx = 0;
            foreach (KeyValuePair<TKey, TVal> kvp in this)
            {
                info.AddValue(string.Format("Item{0}", itemIdx), kvp, typeof(KeyValuePair<TKey, TVal>));
                itemIdx++;
            }
        }

        /// <summary>
        /// The get schema.
        /// </summary>
        /// <returns>
        /// The <see cref="XmlSchema"/>.
        /// </returns>
        XmlSchema IXmlSerializable.GetSchema()
        {
            return null;
        }

        /// <summary>
        /// The read xml.
        /// </summary>
        /// <param name="reader">
        /// The reader.
        /// </param>
        /// <exception cref="XmlException">
        /// </exception>
        void IXmlSerializable.ReadXml(XmlReader reader)
        {
            if (reader.IsEmptyElement)
            {
                return;
            }

            // Move past container
            if (!reader.Read())
            {
                throw new XmlException(@"Error in Deserialization of Dictionary");
            }

            // reader.ReadStartElement(DictionaryNodeName);
            while (reader.NodeType != XmlNodeType.EndElement)
            {
                reader.ReadStartElement(ItemNodeName);
                reader.ReadStartElement(KeyNodeName);
                TKey key = (TKey)KeySerializer.Deserialize(reader);
                reader.ReadEndElement();
                reader.ReadStartElement(ValueNodeName);
                TVal value = (TVal)ValueSerializer.Deserialize(reader);
                reader.ReadEndElement();
                reader.ReadEndElement();
                Add(key, value);
                reader.MoveToContent();
            }

            // reader.ReadEndElement();
            reader.ReadEndElement(); // Read End Element to close Read of containing node
        }

        /// <summary>
        /// The write xml.
        /// </summary>
        /// <param name="writer">
        /// The writer.
        /// </param>
        void IXmlSerializable.WriteXml(XmlWriter writer)
        {
            // writer.WriteStartElement(DictionaryNodeName);
            foreach (KeyValuePair<TKey, TVal> kvp in this)
            {
                writer.WriteStartElement(ItemNodeName);
                writer.WriteStartElement(KeyNodeName);
                KeySerializer.Serialize(writer, kvp.Key);
                writer.WriteEndElement();
                writer.WriteStartElement(ValueNodeName);
                ValueSerializer.Serialize(writer, kvp.Value);
                writer.WriteEndElement();
                writer.WriteEndElement();
            }

            // writer.WriteEndElement();
        }

        #endregion
    }
}

